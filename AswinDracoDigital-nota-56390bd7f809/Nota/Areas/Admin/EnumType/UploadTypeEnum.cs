﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.EnumType
{
    public enum UploadTypeEnum : Int16
    {
        Notes = 1,
        Music = 2,
        Lyrics = 3,
    }
}
