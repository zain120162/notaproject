﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.EnumType
{
    public enum PriceTypeEnum : Int16
    {
        Free = 1,
        Premium = 2
    }
}
