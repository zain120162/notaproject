﻿using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class ManageUserController : BaseController
    {
        #region Member Declaration
        private readonly IManageUser _user;
        #endregion

        #region Constractor
        public ManageUserController(IManageUser user)
        {
            _user = user;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Public User Management";
            var viewModel = new UserModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "User" }
            };
            return View(viewModel);
        }
        #endregion

        #region Detail
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Detail(string id)
        {
            ViewData["PageTitle"] = "Public User Detail";
            //ViewData["Manage User Detail"] = "Manage User Detail";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "ManageUser");
                }
                else
                {
                    var userID = int.Parse(id);
                    UserModel objUserModel = _user.GetUserDetailByUserID(userID);
                    if (objUserModel == null)
                    {
                        return RedirectToAction("Index", "ManageUser", new { Msg = "drop" });
                    }
                    else
                    {
                        return View(objUserModel);
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index", "ManageUser", new { Msg = "error" });
            }
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<UserModel>> _AjaxBinding()
        {
            var userList = _user.BindUserDetail().ToList();
            return Json(userList);
        }
        #endregion

        #region Edit
        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(UserModel objUserModel)
        {
            _user.EditUser(objUserModel);
            return RedirectToAction("Index", "ManageUser", new { Msg = "updated" });
        }
        #endregion
    }
}