﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Helpers;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Interface;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class EventParticipantController : Controller
    {
        #region Member Declaration
        private readonly IEventParticipant _eventparticipant;
        private readonly IEvents _event;
        private readonly ITemplate _template;
        private readonly IManageUser _publicuser;
        private readonly ICommonSetting _commonsetting;
        private readonly IEmailSend _emailsend;
        #endregion

        #region Constractor
        public EventParticipantController(IEventParticipant eventparticipant, IEvents events, ITemplate template, IManageUser publicuser, ICommonSetting commonsetting, IEmailSend emailsend)
        {
            _eventparticipant = eventparticipant;
            _event = events;
            _template = template;
            _publicuser = publicuser;
            _commonsetting = commonsetting;
            _emailsend = emailsend;
        }
        #endregion
        #region Index
        public IActionResult Index()
        {
            return View();
        }
        #endregion
        #region Add Member
        [HttpPost]
        public IActionResult Add(EventParticipantModel eventparticepentmodel)
        {
            EventModel eventmodel = _event.GetEventsDetailByEventsId(eventparticepentmodel.EventsID);
            //eventmodel.MaxParticipants
            if (eventmodel.RegParticipants == eventmodel.MaxParticipants)
            {
                var response = new { Result = "Seats are Full" };
                return Json(response);
            }
            else
            {
                eventmodel.RegParticipants++;
                _event.EditEvents(eventmodel);
                eventparticepentmodel.DateApplied = DateTime.Now;
                bool result = _eventparticipant.Add(eventparticepentmodel);
                if (result)
                {
                    var templates = _template.GetEmailTemplateContent("Event Member Add Request");
                    var objUserModel = _publicuser.GetUserDetailByUserID(eventparticepentmodel.PublicUserID);
                    string emailBody = templates.Body;
                    emailBody = PopulateEmailBody(objUserModel, emailBody, eventmodel.Title);
                    _emailsend.SendModuleEmail(objUserModel.Email, templates.Subject, emailBody);
                    var response = new { Result = "Success" };
                    return Json(response);
                }
                else
                {
                    var response = new { Result = "Failed" };
                    return Json(response);
                }
            }
            //  return View();
        }
        #endregion
        #region Email body Html In Forgot Password        
        private string PopulateEmailBody(UserModel result, string emailBody, string eventtitle)
        {
            var siteUrl = _commonsetting.GetCommonSetting().SiteURL;
            emailBody = emailBody.Replace("#FirstName#", Convert.ToString(result.FirstName));
            emailBody = emailBody.Replace("#LastName#", Convert.ToString(result.LastName));
            emailBody = emailBody.Replace("#EventName#", Convert.ToString(eventtitle));

            return emailBody;
        }
        #endregion
        #region Delete Member
        [HttpPost]
        public IActionResult Delete(EventParticipantModel eventparticepentmodel)
        {
            EventModel eventmodel = _event.GetEventsDetailByEventsId(eventparticepentmodel.EventsID);
            eventmodel.RegParticipants--;
            _event.EditEvents(eventmodel);
            bool result = _eventparticipant.Delete(eventparticepentmodel);

            if (result)
            {
                var response = new { Result = "Success" };
                return Json(response);
            }
            else
            {
                var response = new { Result = "Failed" };
                return Json(response);
            }
        }
        #endregion
    }
}