﻿using System;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Interface;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ForgotPasswordController : Controller
    {
        #region Member Declaration
        private readonly IEmailSend _emailSend;
        private readonly IForgotPassword _forgotPassword;
        private readonly ITemplate _template;
        private readonly ICommonSetting _commonSetting;
        #endregion

        #region Constractor
        public ForgotPasswordController(ICommonSetting commonSetting, IEmailSend emailSend, IForgotPassword forgotPassword, ITemplate template)
        {
            _forgotPassword = forgotPassword;
            _template = template;
            _emailSend = emailSend;
            _commonSetting = commonSetting;
        }
        #endregion

        #region Index
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(ForgotPasswordModel objForgotPasswordModel)
        {
            if (ModelState.IsValid)
            {
                objForgotPasswordModel = _forgotPassword.CheckForContact(objForgotPasswordModel.Email);
                if (objForgotPasswordModel != null)
                { 
                    var templates = _template.GetEmailTemplateContent("Forgot Password");

                    string emailBody = templates.Body;
                    emailBody = PopulateEmailBody(objForgotPasswordModel, emailBody);

                    _emailSend.SendModuleEmail(objForgotPasswordModel.Email, templates.Subject, emailBody);
                    return RedirectToAction("Index", "Login", new { Msg = "Forgot" });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.NotExistsUser));
                    return View(objForgotPasswordModel);
                }
            }
            return View(objForgotPasswordModel);
        }
        #endregion

        #region Forgot Password Confirmation
        [HttpGet]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }
        #endregion

        #region Email body Html In Forgot Password        
        private string PopulateEmailBody(ForgotPasswordModel result, string emailBody)
        {
            var siteUrl = _commonSetting.GetCommonSetting().SiteURL;
            emailBody = emailBody.Replace("#FirstName#", Convert.ToString(result.FirstName));
            emailBody = emailBody.Replace("#LastName#", Convert.ToString(result.LastName));
            emailBody = emailBody.Replace("#click here#", "<a href=" + string.Format(siteUrl) + "ResetPassword?UID=" + GlobalCode.UrlEncrypt(DateTime.Now.ToString(GlobalCode.dateTimeFormat) + "###" + result.UserID.ToString()) + ">click here</a>");
            
            return emailBody;
        }
        #endregion
    }
}