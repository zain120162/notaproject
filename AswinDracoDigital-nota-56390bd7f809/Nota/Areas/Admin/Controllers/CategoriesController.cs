﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nota.Areas.Admin.Controllers
{   
    [Area("Admin")]
    [ValidateUserLogin]
    public class CategoriesController : BaseController
    {
        #region Member Declaration
        private readonly ICategories _categories;
        private readonly ApplicationDbContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region Constractor
        [Obsolete]
        public CategoriesController(ICategories categories,ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _categories = categories;
            _context = context;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Category Management";
            var viewModel = new CategoriesModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Categories" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create()
        {
            ViewData["PageTitle"] = "Add Category";
            CategoriesModel categoriesModel = new CategoriesModel();
            return View(categoriesModel);
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create(CategoriesModel categoriesModel, AttachmentModel attachmentModel)
        {
                if (_categories.IsCategoriesExists(categoriesModel.CategoriesID, categoriesModel.Name.Trim()))
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Categories"));
                    return View();
                }
                if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
                {
                    var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                    if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName))))
                    {
                        ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                        return View(categoriesModel);
                    }

                    if (attachments.Any(x => ((x.Length / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize))
                    {
                        ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                        return View(categoriesModel);
                    }
                }
                _categories.AddCategories(categoriesModel, attachmentModel);
                return RedirectToAction("Index", "Categories", new { Msg = "added" });
        }
        #endregion

        #region Edit
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(string id)
        {
            ViewData["PageTitle"] = "Edit Category";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Categories");
                }
                else
                {
                    var categoriesID = int.Parse(id);
                    CategoriesModel categoriesModel = _categories.GetCategoriesDetailByCategoriesId(categoriesID);
                    if (categoriesModel == null)
                    {
                        return RedirectToAction("Index", "Categories", new { Msg = "drop" });
                    }
                    return View(categoriesModel);
                }
            }
            catch
            {
                return RedirectToAction("Index", "Categories", new { Msg = "error" });
            }
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(CategoriesModel categoriesModel, string categoriesId,AttachmentModel attachmentModel)
        {
            var categoriesDetail = _categories.GetCategoriesDetailByCategoriesId(categoriesModel.CategoriesID);
            if (categoriesDetail == null)
            {
                return RedirectToAction("Index", "Categories", new { Msg = "drop" });
            }
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName))))
                {
                    ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                    return View(categoriesModel);
                }

                if (attachments.Any(x => ((x.Length / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize))
                {
                    ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                    return View(categoriesModel);
                }
            }
            _categories.EditCategories(categoriesModel,attachmentModel);
            return RedirectToAction("Index", "Categories", new { Msg = "updated" });
        }
        #endregion

        #region Download Attechment
        [Obsolete]
        public ActionResult DownloadAttachment(long attachmentID)
        {
            try
            {
                var attachmentFilePath = GetAttahchmentFilePath(_context, attachmentID);
                if (string.IsNullOrEmpty(attachmentFilePath))
                {
                    return RedirectToAction("Index", "Categories", new { Msg = "filenotfound" });
                }

                var fileName = Path.GetFileName(attachmentFilePath);
                FileInfo file = new FileInfo(attachmentFilePath);
                if (file.Exists)
                {
                    byte[] fileBytes = System.IO.File.ReadAllBytes(attachmentFilePath);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return RedirectToAction("Index", "Categories", new { Msg = "filenotfound" });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Categories", new { Msg = "error" });
            }
        }

        [Obsolete]
        public string GetAttahchmentFilePath(ApplicationDbContext context, long attachmentID)
        {
            var attachment = context.tblCategories.Where(x => x.CategoriesID == attachmentID).FirstOrDefault();
            var LeadKey = attachment.CategoriesID;
            if (attachment != null)
            {
                string folderName = "ApplicationDocuments/Images";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string filePath = Path.Combine(webRootPath, folderName, attachment.Thumbnail);

                return filePath;
            }
            return string.Empty;
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<CategoriesModel>> _AjaxBinding()
        {
            var categoriesList = _categories.BindCategoriesDetail().ToList();
            return Json(categoriesList);
        }
        #endregion

        #region IsActive Ajax Binding
        public IActionResult _IsActive(int id)
        {
            var Verified = _categories.IsActive(id);
            if (Verified == true)
            {
                TempData["Msg"] = "active";
            }
            else
            {
                TempData["Msg"] = "inActive";
            }
            return Json(Verified);
        }
        #endregion

        #region Delete
        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Delete)]
        public IActionResult Delete(string[] categoriesId)
        {
            try
            {
                if (categoriesId.Length > 0)
                {
                    _categories.DeleteCategories(categoriesId);
                    return RedirectToAction("Index", "Categories", new { Msg = "deleted" });
                }
                else
                {
                    return RedirectToAction("Index", "Categories", new { Msg = "NoSelect" });
                }
            }
            catch (Exception _exception)
            {
                if (_exception.InnerException != null && (_exception.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((_exception.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference)))
                {
                    return RedirectToAction("Index", "Categories", new { Msg = "inuse" });
                }
                return RedirectToAction("Index", "Categories", new { Msg = "error" });
            }
        }
        #endregion

        #region Detail
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Detail(string id)
        {
            ViewData["PageTitle"] = "Category Detail";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Categories");
                }
                else
                {
                    var categoriesID = int.Parse(id);
                    CategoriesModel categoriesModel = _categories.GetCategoriesDetailByCategoriesId(categoriesID);
                    if (categoriesModel == null)
                    {
                        return RedirectToAction("Index", "Categories", new { Msg = "drop" });
                    }
                    else
                    {
                        return View(categoriesModel);
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index", "Categories", new { Msg = "error" });
            }
        }
        #endregion
    }
}