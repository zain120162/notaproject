﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Areas.Admin.Helpers;
using Nota.Areas.Admin.Interface;
using Nota.Context;
using Nota.Common;
using Nota.Areas.Admin.Models;
using Nota.Models;
using Nota.Controllers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class RolePrivilegesController : BaseController
    {
        #region Member Declaration
        private readonly IRoles _role;
        private readonly IRolePrivileges _rolePrivileges;
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public RolePrivilegesController(IRoles role, ApplicationDbContext context, IRolePrivileges rolePrivileges)
        {
            _role = role;
            _rolePrivileges = rolePrivileges;
            _context = context;
        }
        #endregion

        #region Index
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Role Privileges Management";
            RolePrivilegesModel objRolePrivilegesModel = new RolePrivilegesModel();
            objRolePrivilegesModel.Roles = new SelectList(_role.GetActiveAndNonAdminRoleList(), "RoleID", "Role");
            objRolePrivilegesModel.ModuleName = new CommonMessagesModel { ModuleName = "Role Privileges" };
            return View(objRolePrivilegesModel);
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index(int? RoleID, string btnSubmit, RolePrivilegesModel objRolePrivilegesModel)
        {
            ViewData["PageTitle"] = "Role Privileges Management";
            var roleList = _role.GetActiveAndNonAdminRoleList();
            objRolePrivilegesModel.ModuleName = new CommonMessagesModel { ModuleName = "Role Privileges" };
            objRolePrivilegesModel.Roles = new SelectList(_role.GetActiveAndNonAdminRoleList(), "RoleID", "Role");

            if (Convert.ToInt32(RoleID) > 0 && btnSubmit == null)
            {
                ViewData["RolePrivileges"] = _rolePrivileges.ExecuteStoredProcedure("RolePrev", Convert.ToInt32(RoleID));
                return View(objRolePrivilegesModel);
            }
            else if (RoleID > 0)
            {
                _rolePrivileges.DeleteRolePrivilegesByRoleID(Convert.ToInt32(RoleID));

                string strHdMenuItemID = Request.Form["hdMenuItemID"];
                string[] strHdMenuItemIDArray = strHdMenuItemID.Split(',');

                string strChkView = Request.Form["chkView"];
                string[] strChkViewArray = null;
                if (strChkView != null)
                    strChkViewArray = strChkView.Split(',');

                string strChkAdd = Request.Form["chkAdd"];
                string[] strChkAddArray = null;
                if (strChkAdd != null)
                    strChkAddArray = strChkAdd.Split(',');

                string strChkEdit = Request.Form["chkEdit"];
                string[] strChkEditArray = null;
                if (strChkEdit != null)
                    strChkEditArray = strChkEdit.Split(',');

                string strChkDelete = Request.Form["chkDelete"];
                string[] strChkDeleteArray = null;
                if (strChkDelete != null)
                    strChkDeleteArray = strChkDelete.Split(',');

                string strChkDetail = Request.Form["chkDetail"];
                string[] strChkDetailArray = null;
                if (strChkDetail != null)
                    strChkDetailArray = strChkDetail.Split(',');

                int MenuItemID = 0;

                foreach (var item in strHdMenuItemIDArray)
                {
                    MenuItemID = Convert.ToInt32(item);
                    bool isView = false;
                    bool isAdd = false;
                    bool isEdit = false;
                    bool isDelete = false;
                    bool isDetail = false;

                    if (strChkViewArray != null && strChkViewArray.Contains("v" + MenuItemID))
                        isView = true;
                    if (strChkAddArray != null && strChkAddArray.Contains("a" + MenuItemID))
                        isAdd = true;
                    if (strChkEditArray != null && strChkEditArray.Contains("e" + MenuItemID))
                        isEdit = true;
                    if (strChkDeleteArray != null && strChkDeleteArray.Contains("d" + MenuItemID))
                        isDelete = true;
                    if (strChkDetailArray != null && strChkDetailArray.Contains("de" + MenuItemID))
                        isDetail = true;

                    if (isView || isAdd || isEdit || isDelete || isDetail)
                    {
                        tblRolePrivileges objTblRolePrivileges = new tblRolePrivileges();
                        objTblRolePrivileges.RoleID = RoleID;
                        objTblRolePrivileges.MenuItemID = Convert.ToInt32(MenuItemID);
                        objTblRolePrivileges.View = isView;
                        objTblRolePrivileges.Add = isAdd;
                        objTblRolePrivileges.Edit = isEdit;
                        objTblRolePrivileges.Delete = isDelete;
                        objTblRolePrivileges.Detail = isDetail;
                        objTblRolePrivileges.IsActive = true;
                        _context.tblRolePrivileges.Add(objTblRolePrivileges);
                        _context.SaveChanges();
                    }
                }

                int[] MenuItemIDs = (from m in _context.tblMenuItem where m.ParentID == 0 && m.IsActive == true select m.MenuItemID).ToArray();
                foreach (int menuID in MenuItemIDs)
                {
                    int[] SubMenuIDArray = _context.tblMenuItem.Where(e => e.ParentID == menuID && e.IsActive == true).Select(e => e.MenuItemID).ToArray();
                    bool MainStatus = false;
                    foreach (int subMenuID in SubMenuIDArray)
                    {
                        tblRolePrivileges objTblRolePrivilegesMain = _context.tblRolePrivileges.Where(e => e.MenuItemID == subMenuID && e.RoleID == RoleID).FirstOrDefault();
                        if (objTblRolePrivilegesMain != null)
                        {
                            if (objTblRolePrivilegesMain.View == true || objTblRolePrivilegesMain.Add == true || objTblRolePrivilegesMain.Edit == true || objTblRolePrivilegesMain.Delete == true || objTblRolePrivilegesMain.Detail == true)
                            {
                                MainStatus = true;
                                break;
                            }
                        }
                    }
                    if (MainStatus)
                    {
                        tblRolePrivileges objTblRolePrivilegesMainItem = new tblRolePrivileges();
                        objTblRolePrivilegesMainItem.View = true;
                        objTblRolePrivilegesMainItem.Add = true;
                        objTblRolePrivilegesMainItem.Edit = true;
                        objTblRolePrivilegesMainItem.Delete = true;
                        objTblRolePrivilegesMainItem.Detail = true;
                        objTblRolePrivilegesMainItem.MenuItemID = menuID;
                        objTblRolePrivilegesMainItem.RoleID = RoleID;
                        objTblRolePrivilegesMainItem.IsActive = true;
                        _context.tblRolePrivileges.Add(objTblRolePrivilegesMainItem);
                        _context.SaveChanges();
                    }
                }
                ViewData["MenuItem"] = null;
                return RedirectToAction("Index", "RolePrivileges", new { Msg = "added" });
            }
            else
            {
                ViewData["MenuItem"] = null;
                return RedirectToAction("Index", "RolePrivileges");
            }
        }
        #endregion

        #region Update Rights of Parent Menu
        private void UpdateRightsOfParentMenu(int RoleID, int MenuItemID)
        {
            ViewData["PageTitle"] = "Role Privileges Management";
            int[] SubMenuIDArray = _context.tblMenuItem.Where(e => e.ParentID == MenuItemID && e.IsActive == true).Select(e => e.MenuItemID).ToArray();
            bool MainStatus = false;
            foreach (int subMenuID in SubMenuIDArray)
            {
                int[] subToSubMenuIDList = _context.tblMenuItem.Where(e => e.ParentID == subMenuID && e.IsActive == true).Select(e => e.MenuItemID).ToArray();
                bool status = false;
                foreach (int subToSubMenuID in subToSubMenuIDList)
                {
                    tblRolePrivileges objTblRolePrivileges = _context.tblRolePrivileges.Where(e => e.MenuItemID == subToSubMenuID && e.RoleID == RoleID).FirstOrDefault();
                    if (objTblRolePrivileges != null)
                    {
                        if (objTblRolePrivileges.View == true || objTblRolePrivileges.Add == true || objTblRolePrivileges.Edit == true || objTblRolePrivileges.Delete == true || objTblRolePrivileges.Detail == true)
                        {
                            status = true;
                            break;
                        }
                    }
                }
                if (status)
                {
                    tblRolePrivileges objTblRolePrivilegesSub = new tblRolePrivileges();
                    objTblRolePrivilegesSub.View = true;
                    objTblRolePrivilegesSub.Add = true;
                    objTblRolePrivilegesSub.Edit = true;
                    objTblRolePrivilegesSub.Delete = true;
                    objTblRolePrivilegesSub.Detail = true;
                    objTblRolePrivilegesSub.MenuItemID = subMenuID;
                    objTblRolePrivilegesSub.RoleID = RoleID;
                    objTblRolePrivilegesSub.IsActive = true;
                    _context.tblRolePrivileges.Add(objTblRolePrivilegesSub);
                    _context.SaveChanges();
                }
                tblRolePrivileges objTblRolePrivilegesMain = _context.tblRolePrivileges.Where(e => e.MenuItemID == subMenuID && e.RoleID == RoleID).FirstOrDefault();
                if (objTblRolePrivilegesMain != null)
                {
                    if (objTblRolePrivilegesMain.View == true || objTblRolePrivilegesMain.Add == true || objTblRolePrivilegesMain.Edit == true || objTblRolePrivilegesMain.Delete == true || objTblRolePrivilegesMain.Detail == true)
                    {
                        MainStatus = true;
                        break;
                    }
                }
            }
            if (MainStatus)
            {
                tblRolePrivileges objTblRolePrivilegesMainItem = new tblRolePrivileges();
                objTblRolePrivilegesMainItem.View = true;
                objTblRolePrivilegesMainItem.Add = true;
                objTblRolePrivilegesMainItem.Edit = true;
                objTblRolePrivilegesMainItem.Delete = true;
                objTblRolePrivilegesMainItem.Detail = true;
                objTblRolePrivilegesMainItem.MenuItemID = MenuItemID;
                objTblRolePrivilegesMainItem.RoleID = RoleID;
                objTblRolePrivilegesMainItem.IsActive = true;
                _context.tblRolePrivileges.Add(objTblRolePrivilegesMainItem);
                _context.SaveChanges();
            }
        }
        #endregion

        #region Json Method To Get Role List For DropDown
        public JsonResult GetRoleList(int RoleID)
        {
            if (RoleID > 0)
            {
                return Json(_rolePrivileges.GetUserSelectList(RoleID));
            }
            else
            {
                return Json(string.Empty);
            }
        }
        #endregion
    }
}