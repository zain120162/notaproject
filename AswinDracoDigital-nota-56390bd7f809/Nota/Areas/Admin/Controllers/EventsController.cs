﻿using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using System.Linq;
using System;
using System.Collections.Generic;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class EventsController : BaseController
    {
        #region Member Declaration
        private readonly ApplicationDbContext _context;
        private readonly IEvents _events;
        private readonly IManageUser _user;
        private readonly IEventSubmission _eventsubmission;
        #endregion

        #region Constractor
        [Obsolete]
        public EventsController(ApplicationDbContext context, IEvents events, IManageUser user, IEventSubmission eventsubmission)
        {
            _context = context;
            _events = events;
            _user = user;
            _eventsubmission = eventsubmission;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Event Management";
            var viewModel = new EventModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Events" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create()
        {
            ViewData["PageTitle"] = "Add Event";
            EventModel eventModel = new EventModel();
            return View(eventModel);
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create(EventModel eventModel)
        {
            if (_events.IsEventExists(eventModel.EventsID, eventModel.Title.Trim()))
            {
                ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Events"));
                return View();
            }
            else
            {
                _events.AddEvents(eventModel);
                return RedirectToAction("Index", "Events", new { Msg = "added" });
            }
        }
        #endregion

        #region Edit
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(string id)
        {
            ViewData["PageTitle"] = "Edit Event";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Events");
                }
                else
                {
                    var eventsID = int.Parse(id);
                    EventModel eventModel = _events.GetEventsDetailByEventsId(eventsID);
                    if (eventModel == null)
                    {
                        return RedirectToAction("Index", "Events", new { Msg = "drop" });
                    }
                    return View(eventModel);
                }
            }
            catch
            {
                return RedirectToAction("Index", "Events", new { Msg = "error" });
            }
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(EventModel eventModel, string eventsId)
        {
            var eventsDetail = _events.GetEventsDetailByEventsId(eventModel.EventsID);
            if (eventModel == null)
            {
                return RedirectToAction("Index", "Events", new { Msg = "drop" });
            }
            else
            {
                _events.EditEvents(eventModel);
                return RedirectToAction("Index", "Events", new { Msg = "updated" });
            }
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<EventModel>> _AjaxBinding()
        {
            var eventsList = _events.BindEventsDetail().ToList();
            return Json(eventsList);
        }
        #endregion

        #region Delete
        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Delete)]
        public IActionResult Delete(string[] eventsId)
        {
            try
            {
                if (eventsId.Length > 0)
                {
                    _events.DeleteEvents(eventsId);
                    return RedirectToAction("Index", "Events", new { Msg = "deleted" });
                }
                else
                {
                    return RedirectToAction("Index", "Events", new { Msg = "NoSelect" });
                }
            }
            catch (Exception _exception)
            {
                if (_exception.InnerException != null && (_exception.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((_exception.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference)))
                {
                    return RedirectToAction("Index", "Events", new { Msg = "inuse" });
                }
                return RedirectToAction("Index", "Events", new { Msg = "error" });
            }
        }
        #endregion

        #region Detail
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Detail(string id)
        {
            ViewData["PageTitle"] = "Event Detail";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Events");
                }
                else
                {
                    var eventsID = int.Parse(id);
                    EventModel eventModel = _events.GetEventsDetailByEventsId(eventsID);
                    if (eventModel == null)
                    {
                        return RedirectToAction("Index", "Events", new { Msg = "drop" });
                    }
                    else
                    {
                        return View(eventModel);
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index", "Events", new { Msg = "error" });
            }
        }
        #endregion
        #region Members
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Members(string id)
        {
            ViewData["PageTitle"] = "Event Members";
            ViewData["EventID"] = id;
            return View();
            //try
            //{
            //    if (string.IsNullOrEmpty(id))
            //    {
            //        return RedirectToAction("Index", "Events");
            //    }
            //    else
            //    {
            //        var eventsID = int.Parse(id);
            //        List<EventMemberModel> eventmembermodellist = _events.GetEventMembers(eventsID);
            //        if (eventmembermodellist == null)
            //        {
            //            return RedirectToAction("Index", "Events", new { Msg = "drop" });
            //        }
            //        else
            //        {
            //            return View(eventmembermodellist);
            //        }
            //    }
            //}
            //catch
            //{
            //    return RedirectToAction("Index", "Events", new { Msg = "error" });
            //}
        }
        #endregion
        #region Get Members
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult GetMembers(string id)
        {

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Events");
                }
                else
                {
                    var eventsID = int.Parse(id);
                    List<EventMemberModel> eventmembermodellist = _events.GetEventMembers(eventsID);
                    if (eventmembermodellist == null)
                    {
                        return Json(eventmembermodellist);
                    }
                    else
                    {
                        return Json(eventmembermodellist);
                    }
                }
            }
            catch
            {
                return Json(null);
            }
        }
        #endregion
        #region IsActive Ajax Binding
        public IActionResult _IsActive(int id)
        {
            var Verified = _events.IsActive(id);
            if (Verified == true)
            {
                TempData["Msg"] = "active";
            }
            else
            {
                TempData["Msg"] = "inActive";
            }
            return Json(Verified);
        }
        #endregion
        #region Ajax Binding
        public ActionResult<IList<UserModel>> GetUsers()
        {
            var userList = _user.BindUserDetail().ToList();
            return Json(new { data = userList });
        }
        #endregion

        #region View Submissions 
        public ActionResult ViewSubmissions(int id, int eventid)
        {
            List<EventSubmissionModel> eventsubmodel = _eventsubmission.GetAll(id, eventid);
            return View(eventsubmodel);
        }
        #endregion

        #region Download Submission
        public FileResult Download(string file)
        {

            string contentType = string.Empty;
            if (file.Contains(".txt"))
            {
                contentType = "application/txt";
            }
            else if (file.Contains(".docx"))
            {
                contentType = "application/docx";
            }
            else
            {
                contentType = "application/docx";
            }
            return File(file, contentType, file);
        }
        #endregion
    }
}