﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Common;
using Nota.Areas.Admin.Helpers;
using Nota.Models;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    //[Area(nameof(Admin))]
    //[Route("Admin/[controller]/[action]")]
    public class LoginController : Controller
    {
        #region Member Declaration        
        private readonly IUser _user;
        private readonly IRolePrivileges _rolePrivileges;
        #endregion

        #region Constractor      
        public LoginController(IUser user, IRolePrivileges rolePrivileges)
        {
            _user = user;
            _rolePrivileges = rolePrivileges;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            if (CurrentAdminSession.User != null && CurrentAdminSession.UserID > 0)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Index(int? id)
         {
            if (CurrentAdminSession.User != null && CurrentAdminSession.UserID > 0)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            if (!string.IsNullOrEmpty(Request.Form["Email"]) && !string.IsNullOrEmpty(Request.Form["Password"]))
            {
                string strUserName = Convert.ToString(Request.Form["Email"]);
                string strUserPassword = Convert.ToString(Request.Form["Password"]);
                var userDetail = _user.GetActiveUserByEmail(strUserName);
                if (userDetail != null)
                {
                    if (strUserPassword == GlobalCode.Encryption.Decrypt(userDetail.Password))
                    {
                        CurrentAdminUser User = new CurrentAdminUser();
                        User.UserID = userDetail.UserID;
                        User.RoleID = (int)userDetail.RoleID;
                        User.FirstName = userDetail.FirstName;
                        User.LastName = userDetail.LastName;
                        User.Email = userDetail.Email;
                        User.IsAdminRole = userDetail.IsAdminUser;
                        CurrentAdminSession.User = User;
                        HttpContext.Session.SetInt32("UserID", userDetail.UserID);
                        HttpContext.Session.SetInt32("RoleID", (int)userDetail.RoleID);
                        IList<vwRolePrivileges> menuItems = null;
                        IList<vwRolePrivileges> mainItems = null;
                        int roleID = Convert.ToInt32(userDetail.RoleID);
                        var ParentList = _rolePrivileges.GetParentIDsByRole(roleID, userDetail.IsAdminUser);
                        mainItems = _rolePrivileges.GetMainMenuList(roleID, ParentList, userDetail.IsAdminUser);
                        menuItems = _rolePrivileges.GetMenuList(roleID, userDetail.IsAdminUser);
                        HttpContext.Session.SetObject("MainItems", mainItems);
                        HttpContext.Session.SetObject("MenuItems", menuItems);

                        if (TempData["ReturnPath"] != null)
                        {
                            return Redirect(Url.Content(TempData["ReturnPath"].ToString()));
                        }
                        return RedirectToAction("Index", "Dashboard",new { area = "Admin"});
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, Messages.WrongEmailPassword);
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, Messages.WrongEmailPassword);
                    return View();
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, Messages.RequiredField);
                return View();
            }
        }
        #endregion

        #region Logout
        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            HttpContext.Session.Remove("UserID");
            return RedirectToAction("Index", "Login", new { Msg = "Logout" });
        }
        #endregion

        #region Error
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
    }
}