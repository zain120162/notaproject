﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class ResetPasswordController : BaseController
    {
        #region Member Declaration
        private readonly IUser _user;
        private readonly IResetPassword _resetPassword;
        #endregion

        #region Constractor
        public ResetPasswordController(IUser user, IResetPassword resetPassword)
        {
            _user = user;
            _resetPassword = resetPassword;
        }
        #endregion

        #region Index      
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public ActionResult Index()
        {
            if (HttpContext.Request.Query["UID"].ToString() != null)
            {
                string userDetail = HttpContext.Request.Query["UID"].ToString();
                int userID;
                if (int.TryParse(GlobalCode.UrlDecrypt(userDetail).Split(new string[] { "###" }, StringSplitOptions.RemoveEmptyEntries).Last(), out userID))
                {
                    var objValidateUser = _user.GetUserByID(userID);
                    if (objValidateUser == null)
                    {
                        ModelState.AddModelError(string.Empty, Messages.NotExistsUser);
                        return View(new ResetPasswordModel { Email = string.Empty });
                    }
                    var objValidateResetPassword = new ResetPasswordModel
                    {
                        UserID = userID,
                        Email = objValidateUser.Email,
                        OldPassword = GlobalCode.Encryption.Decrypt(objValidateUser.Password),
                    };
                    return View(objValidateResetPassword);
                }
                ModelState.AddModelError(string.Empty, string.Format(Messages.InvalidLink, string.Empty));
                return View(new ResetPasswordModel { Email = string.Empty });
            }
            else
            {
                ModelState.AddModelError(string.Empty, string.Format(Messages.InvalidLink, string.Empty));
                return View(new ResetPasswordModel { Email = string.Empty });
            }
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public ActionResult Index(ResetPasswordModel objValidateResetPassword)
        {
            if (ModelState.IsValid)
            {
                if (objValidateResetPassword.OldPassword == objValidateResetPassword.Password)
                {
                    ModelState.AddModelError(string.Empty, Messages.NewPasswordCanNotBeSame);
                    return View(objValidateResetPassword);
                }
                UserModel objValidateUser = _user.GetUserByID(objValidateResetPassword.UserID);
                if (objValidateUser == null)
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.InvalidLink, "Email"));
                    return View(objValidateResetPassword);
                }
                objValidateResetPassword.Password = GlobalCode.Encryption.Encrypt(objValidateResetPassword.Password);
                _resetPassword.EditUserPassword(objValidateResetPassword.Password, objValidateResetPassword.UserID);
                return RedirectToAction("Index", "Login", new { Msg = "Reset" });
            }
            else
            {
                return View(objValidateResetPassword);
            }
        }
        #endregion
    }
}