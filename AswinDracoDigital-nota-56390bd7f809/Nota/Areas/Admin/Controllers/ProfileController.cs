﻿using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class ProfileController : BaseController
    {
        #region Member Declaration
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUser _user;
        private readonly IResetPassword _resetPassword;
        #endregion

        #region Constractor
        public ProfileController(IUser user, IResetPassword resetPassword, IHttpContextAccessor httpContextAccessor)
        {
            _user = user;
            _resetPassword = resetPassword;
            _httpContextAccessor = httpContextAccessor;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Change Password
        public IActionResult ChangePassword()
        {
            ChangePasswordModel objChangePasswordModel = new ChangePasswordModel();
            objChangePasswordModel.UserID = CurrentAdminSession.User.UserID;
            objChangePasswordModel.Email = CurrentAdminSession.User.Email;
            objChangePasswordModel.ModuleName = new CommonMessagesModel { ModuleName = "Change password" };
            ViewData["PageTitle"] = "Change Password";
            return PartialView(objChangePasswordModel);
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordModel objChangePasswordModel)
        {
            objChangePasswordModel.UserID = CurrentAdminSession.User.UserID;
            objChangePasswordModel.Email = CurrentAdminSession.User.Email;

            if (ModelState.IsValid)
            {
                if (objChangePasswordModel.OldPassword == objChangePasswordModel.NewPassword)
                {
                    ModelState.AddModelError("", string.Format(Messages.NewPasswordCanNotBeSame));
                    return View(objChangePasswordModel);
                }

                if (objChangePasswordModel.NewPassword != objChangePasswordModel.ConfirmPassword)
                {
                    ModelState.AddModelError("", string.Format(Messages.PasswordDoNotMatch));
                    return View(objChangePasswordModel);
                }
                var userDetail = _user.GetUserByID(objChangePasswordModel.UserID);
                if (userDetail != null)
                {
                    string currentPassword = GlobalCode.Encryption.Decrypt(userDetail.Password);

                    if (currentPassword != objChangePasswordModel.OldPassword)
                    {
                        ModelState.AddModelError("", string.Format(Messages.OldPasswordDoNotMatch));
                        return View(objChangePasswordModel);
                    }
                }
                objChangePasswordModel.NewPassword = GlobalCode.Encryption.Encrypt(objChangePasswordModel.NewPassword);

                _resetPassword.EditUserPassword(objChangePasswordModel.NewPassword, objChangePasswordModel.UserID);
                HttpContext.Session.Clear();
                HttpContext.Session.Remove("UserID");
                return RedirectToAction("Index", "DashBoard", new { Msg = "changed" });
            }
            else
            {
                var error = string.Join(", ", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                return View(new { success = false, Message = error });
            }
        }
        #endregion
    }
}