﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class UserController : BaseController
    {
        #region Member Declaration
        private readonly IUser _user;
        #endregion

        #region Constractor
        public UserController(IUser user)
        {
            _user = user;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "User Management";
            var viewModel = new UserModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "User" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        [HttpGet]
        public IActionResult Create()
        {
            ViewData["PageTitle"] = "Add User";
            UserModel objUserModel = new UserModel();
            BindRoleDropDown(objUserModel);
            objUserModel.IsActive = true;
            return View(objUserModel);
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create(UserModel objUserModel)
        {
            //Validate User exists or not
            if (_user.IsUserExists(objUserModel.UserID, objUserModel.Email.Trim()))
            {
                ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Email"));
                return View();
            }
            else
            {
                _user.AddUser(objUserModel);
                BindRoleDropDown(objUserModel);
                return RedirectToAction("Index", "Login", new { Msg = "added" });
            }
        }
        #endregion

        #region Edit
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(string id)
        {
            ViewData["PageTitle"] = "Edit User";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    var userID = int.Parse(id);
                    UserModel objUserModel = _user.GetUserDetailByUserID(userID);
                    if (objUserModel == null)
                    {
                        return RedirectToAction("Index", "User", new { Msg = "drop" });
                    }
                    //if (objUserViewModel.Email == GlobalCode.AdminEmailAddress)
                    //{
                    //    return RedirectToAction("Index", "User", new { Msg = "notauthorized" });
                    //}
                    BindRoleDropDown(objUserModel);
                    objUserModel.Password = objUserModel.Password;
                    return View(objUserModel);
                }
            }
            catch
            {
                return RedirectToAction("Index", "User", new { Msg = "error" });
            }
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(UserModel objUserModel, string userId)
        {
            var userDetail = _user.GetUserDetailByUserID(objUserModel.UserID);
            if (userDetail == null)
            {
                return RedirectToAction("Index", "User", new { Msg = "drop" });
            }
            if (userDetail.Email == GlobalCode.AdminEmailAddress)
            {
                return RedirectToAction("Index", "User", new { Msg = "notauthorized" });
            }

            bool emailDuplicateStatus = _user.IsUserExistsByEmail("Email", objUserModel.Email.Trim(), objUserModel.UserID);
            if (emailDuplicateStatus)
            {
                ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Email "));
                BindRoleDropDown(objUserModel);
                return View(objUserModel);
            }
            _user.EditUser(objUserModel);
            return RedirectToAction("Index", "User", new { Msg = "updated" });

        }
        #endregion

        #region Detail
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Detail(string id)
        {
            ViewData["PageTitle"] = "User Detail";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    var userID = int.Parse(id);
                    UserModel objUserModel = _user.GetUserDetailByUserID(userID);
                    if (objUserModel == null)
                    {
                        return RedirectToAction("Index", "User", new { Msg = "drop" });
                    }
                    else
                    {
                        return View(objUserModel);
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index", "User", new { Msg = "error" });
            }
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<UserModel>> _AjaxBinding()
        {
            var userList = _user.BindUserDetail().ToList();
            return Json(userList);
        }
        #endregion

        #region Delete
        [ValidateUserPermission(GlobalCode.Actions.Delete)]
        [HttpPost]
        public IActionResult Delete(string[] userId)
        {
            try
            {
                if (userId.Length > 0)
                {
                    _user.DeleteUser(userId);
                    return RedirectToAction("Index", "User", new { Msg = "deleted" });
                }
                else
                {
                    return RedirectToAction("Index", "User", new { Msg = "NoSelect" });
                }
            }
            catch (Exception _exception)
            {
                if (_exception.InnerException != null && (_exception.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((_exception.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference)))
                {
                    return RedirectToAction("Index", "User", new { Msg = "inuse" });
                }
                return RedirectToAction("Index", "User", new { Msg = "error" });
            }
        }
        #endregion

        #region Bind Role Drop Down
        private UserModel BindRoleDropDown(UserModel objUserModel)
        {
            objUserModel.SelectRole = new SelectList(_user.GetRoleList(0, string.Empty), "RoleID", "Role");
            return objUserModel;
        }
        #endregion

        #region IsActive Ajax Binding
        public IActionResult _IsActive(int id)
        {
            var Verified = _user.IsActive(id);
            if (Verified == true)
            {
                TempData["Msg"] = "active";
            }
            else
            {
                TempData["Msg"] = "inActive";
            }
            return Json(Verified);
        }
        #endregion
    }
}