﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Helpers;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Controllers;
using Nota.Common;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class ChatController : Controller
    {
        #region Member Declaration
        private readonly IUser _user;
        private readonly IManageUser _publicuser;
        private readonly IChat _chat;
        #endregion
        #region Constractor
        [Obsolete]
        public ChatController(IManageUser user, IChat chat)
        {
            _publicuser = user;
            _chat = chat;
            //      var msglist = _chat.GetMessages(1, 2);
        }
        #endregion
        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            var userList = _publicuser.BindUserDetail().Select(x => new UserModel { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName, UserID = x.UserID }).ToList();

            HttpContext.Session.SetString("userid", CurrentAdminSession.UserID.ToString());
            HttpContext.Session.SetString("username", CurrentAdminSession.FirstName.ToString());
            return View(userList);
        }
        #endregion
        #region test
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult test()
        {
            var userList = _publicuser.BindUserDetail().Select(x => new UserModel { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName, UserID = x.UserID }).ToList();

            HttpContext.Session.SetString("userid", CurrentAdminSession.UserID.ToString());
            HttpContext.Session.SetString("username", CurrentAdminSession.FirstName.ToString());
            return View(userList);
        }
        #endregion
        public PartialViewResult UserChat(string id)
        {
            if (id != null)
            {
                string[] param = id.Split('-');
                var msglist = _chat.GetMessages(int.Parse(param[0]), int.Parse(param[1]));
                UserModel usermdoel = _publicuser.GetUserDetailByUserID(int.Parse(param[1]));
                HttpContext.Session.SetString("ToUserName", usermdoel.FirstName + " " + usermdoel.LastName);
                //HttpContext.Session.SetString("ToUserName", "Messages");
                return PartialView(msglist);
            }
            else
            {
                return PartialView(null);
            }

        }

        #region Send Message
        [HttpPost]
        public JsonResult SendMsg(ChatModel chatmodel)
        {
            chatmodel.TimeStamp = DateTime.Now;
            chatmodel.SeenTag = false;
            bool result = _chat.AddMessage(chatmodel);
            if (result)
            {
                var response = new { Result = "Success" };
                return Json(response);
            }
            else
            {
                var response = new { Result = "Failed", ID = 0 };
                return Json(response);
            }
        }
        #endregion
    }
}