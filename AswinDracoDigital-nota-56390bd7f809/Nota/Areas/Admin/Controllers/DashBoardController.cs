﻿using Microsoft.AspNetCore.Mvc;
using Nota.Common;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    //[Route("Admin/[controller]/[action]")]
    public class DashBoardController : Controller
    {
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Admin Panel Dashboard";
            return View();
        }
    }
}