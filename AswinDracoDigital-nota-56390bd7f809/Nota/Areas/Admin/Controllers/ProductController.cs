﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using System.Linq;
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Controllers;
using Nota.Areas.Admin.Helpers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class ProductController : BaseController
    {
        #region Member Declaration
        private readonly IProduct _product;
        private readonly ApplicationDbContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region Constractor
        [Obsolete]
        public ProductController(IProduct product, ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _product = product;
            _context = context;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Product Management";
            var viewModel = new ProductModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Product" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create()
        {
            ViewData["PageTitle"] = "Create Product";
            ProductModel productModel = new ProductModel();
            BindAllDropDown(productModel);
            return View(productModel);
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create(ProductModel productModel, AttachmentModel attachmentModel)
        {
            if (_product.IsProductExists(productModel.ProductID, productModel.Title.Trim()))
            {
                ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Product"));
                BindAllDropDown(productModel);
                return View();
            }
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName))))
                {
                    ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                    BindAllDropDown(productModel);
                    return View(productModel);
                }

                if (attachments.Any(x => ((x.Length / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize))
                {
                    ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                    return View(productModel);
                }
            }
            _product.AddProduct(productModel, attachmentModel);
            return RedirectToAction("Index", "Product", new { Msg = "added" });
        }
        #endregion

        #region Edit
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(string id)
        {
            ViewData["PageTitle"] = "Edit Product";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var productID = int.Parse(id);
                    ProductModel productModel = _product.GetProductDetailByProductId(productID);
                    if (productModel == null)
                    {
                        return RedirectToAction("Index", "Product", new { Msg = "drop" });
                    }
                    BindAllDropDown(productModel);
                    return View(productModel);
                }
            }
            catch
            {
                return RedirectToAction("Index", "Product", new { Msg = "error" });
            }
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(ProductModel productModel, string productId, AttachmentModel attachmentModel)
        {
            var productDetail = _product.GetProductDetailByProductId(productModel.ProductID);
            if (productDetail == null)
            {
                return RedirectToAction("Index", "Product", new { Msg = "drop" });
            }
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName))))
                {
                    ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                    return View(productModel);
                }

                if (attachments.Any(x => ((x.Length / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize))
                {
                    ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                    return View(productModel);
                }
            }
            _product.EditProduct(productModel, attachmentModel);
            return RedirectToAction("Index", "Product", new { Msg = "updated" });
        }
        #endregion

        #region Download Attechment
        [Obsolete]
        public ActionResult DownloadAttachment(long attachmentID)
        {
            try
            {
                var attachmentFilePath = GetAttahchmentFilePath(_context, attachmentID);
                if (string.IsNullOrEmpty(attachmentFilePath))
                {
                    return RedirectToAction("Index", "Product", new { Msg = "filenotfound" });
                }

                var fileName = Path.GetFileName(attachmentFilePath);
                FileInfo file = new FileInfo(attachmentFilePath);
                if (file.Exists)
                {
                    byte[] fileBytes = System.IO.File.ReadAllBytes(attachmentFilePath);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return RedirectToAction("Index", "Product", new { Msg = "filenotfound" });
                }
            }
            catch
            {
                return RedirectToAction("Index", "Product", new { Msg = "error" });
            }
        }

        [Obsolete]
        public string GetAttahchmentFilePath(ApplicationDbContext context, long attachmentID)
        {
            var attachment = context.tblProducts.Where(x => x.ProductID == attachmentID).FirstOrDefault();
            var LeadKey = attachment.ProductID;
            if (attachment != null)
            {
                string folderName = "ApplicationDocuments/Images";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string filePath = Path.Combine(webRootPath, folderName, attachment.CoverImage);

                return filePath;
            }
            return string.Empty;
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<ProductModel>> _AjaxBinding()
        {
            var productList = _product.BindProductDetail().ToList();
            return Json(productList);
        }
        #endregion

        #region Verified Ajax Binding
        public IActionResult _Verified(int id)
        {
            var Verified = _product.VerifiedProduct(id);
            TempData["Msg"] = "verified";
            return Json(Verified);
        }
        #endregion

        #region IsActive Ajax Binding
        public IActionResult _IsActive(int id)
        {
            var Verified = _product.IsActive(id);
            return Json(Verified);
        }
        #endregion

        #region Delete
        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Delete)]
        public IActionResult Delete(string[] productId)
        {
            try
            {
                if (productId.Length > 0)
                {
                    _product.DeleteProduct(productId);
                    return RedirectToAction("Index", "Product", new { Msg = "deleted" });
                }
                else
                {
                    return RedirectToAction("Index", "Product", new { Msg = "NoSelect" });
                }
            }
            catch (Exception _exception)
            {
                if (_exception.InnerException != null && (_exception.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((_exception.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference)))
                {
                    return RedirectToAction("Index", "Product", new { Msg = "inuse" });
                }
                return RedirectToAction("Index", "Product", new { Msg = "error" });
            }
        }
        #endregion

        #region Detail
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Detail(string id)
        {
            ViewData["PageTitle"] = "Product Detail";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    var productID = int.Parse(id);
                    ProductModel productModel = new ProductModel();
                    BindAllDropDown(productModel);
                    productModel = _product.GetProductDetailByProductId(productID);
                    if (productModel == null)
                    {
                        return RedirectToAction("Index", "Product", new { Msg = "drop" });
                    }
                    else
                    {
                        BindAllDropDown(productModel);
                        return View(productModel);
                    }
                }
            }
            catch
            {
                return RedirectToAction("Index", "Product", new { Msg = "error" });
            }
        }
        #endregion

        #region Bind All Drop Down
        private ProductModel BindAllDropDown(ProductModel productModel)
        {
            productModel.SelectCategories = new SelectList(_product.GetCategoriesList(0, string.Empty), "CategoriesID", "Name");
            return productModel;
        }
        #endregion
    }
}