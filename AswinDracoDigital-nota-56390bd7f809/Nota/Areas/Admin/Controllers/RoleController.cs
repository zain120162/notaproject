﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Helpers;
using Nota.Areas.Admin.Interface;
using Nota.Common;
using Nota.Areas.Admin.Models;
using Nota.Controllers;

namespace Nota.Areas.Admin.Controllers
{
    [Area("Admin")]
    [ValidateUserLogin]
    public class RoleController : BaseController
    {
        #region Member Declaration
        private readonly IRoles _role;
        #endregion

        #region Constractor
        public RoleController(IRoles role)
        {
            _role = role;
        }
        #endregion

        #region Index
        [ValidateUserPermission(GlobalCode.Actions.Index)]
        public IActionResult Index()
        {
            ViewData["PageTitle"] = "Role Management";
            var viewModel = new RoleModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Role" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create()
        {
            ViewData["PageTitle"] = "Add Role";
            RoleModel objRoleModel = new RoleModel();
            objRoleModel.IsActive = true;
            return View(objRoleModel);
        }

        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Create)]
        public IActionResult Create(RoleModel objRoleModel)
        {
            if (ModelState.IsValid)
            {
                if (_role.IsRoleExists(0, objRoleModel.Role.Trim()))
                {
                    ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Role "));
                    return View(objRoleModel);
                }
                _role.SaveChanges(objRoleModel);
                return RedirectToAction("Index", "Role", new { Msg = "added" });
            }
            else
            {
                return View(objRoleModel);
            }
        }
        #endregion

        #region Edit
        [HttpGet]
        [ValidateUserPermission(GlobalCode.Actions.Edit)]
        public IActionResult Edit(string id)
        {
            ViewData["PageTitle"] = "Edit Role";
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Role");
                }
                else
                {
                    var roleID = int.Parse(id);
                    RoleModel objRoleModel = _role.GetRoleDetailByRoleID(roleID);
                    if (objRoleModel == null)
                    {
                        return RedirectToAction("Index", "Role", new { Msg = "drop" });
                    }
                    if (objRoleModel.IsAdminRole)
                    {
                        return RedirectToAction("Index", "Role", new { Msg = "notauthorized" });
                    }
                    return View(objRoleModel);
                }
            }
            catch
            {
                return RedirectToAction("Index", "Role", new { Msg = "error" });
            }
        }

        [HttpPost]
        public IActionResult Edit(RoleModel objRoleModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Validate Role exists or not
                    if (_role.IsRoleExists(objRoleModel.RoleID, objRoleModel.Role.Trim()))
                    {
                        ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Role "));
                        return View();
                    }
                    else
                    {
                        _role.UpdateRole(objRoleModel);
                        return RedirectToAction("Index", "Role", new { Msg = "updated" });
                    }
                }
                else
                {
                    return View(objRoleModel);
                }
            }
            catch
            {
                return RedirectToAction("Index", "Role", new { Msg = "error" });
            }
        }
        #endregion

        #region Detail
        [ValidateUserPermission(GlobalCode.Actions.Detail)]
        public IActionResult Detail(string id)
        {
            ViewData["PageTitle"] = "Role Detail";
            try
            {
                var roleID = int.Parse(id);
                RoleModel objRoleModel = _role.GetRoleDetailByRoleID(roleID);
                if (objRoleModel == null)
                {
                    return RedirectToAction("Index", "Role", new { Msg = "drop" });
                }
                return View(objRoleModel);
            }
            catch
            {
                return RedirectToAction("Index", "Role", new { Msg = "error" });
            }
        }
        #endregion

        #region Delete
        [HttpPost]
        [ValidateUserPermission(GlobalCode.Actions.Delete)]
        public IActionResult Delete(string[] roleID)
        {
            try
            {
                if (roleID.Length > 0)
                {
                    _role.DeleteRole(roleID);
                    return RedirectToAction("Index", "Role", new { Msg = "deleted" });
                }
                else
                {
                    return RedirectToAction("Index", "Role", new { Msg = "NoSelect" });
                }
            }
            catch (Exception _exception)
            {
                if (_exception.InnerException != null && (_exception.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((_exception.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference)))
                {
                    return RedirectToAction("Index", "Role", new { Msg = "inuse" });
                }
                return RedirectToAction("Index", "Role", new { Msg = "error" });
            }
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<RoleModel>> _AjaxBinding()
        {
            var roleList = _role.BindRoleDetail().ToList();
            return Json(roleList);
            //IQueryable<RoleViewModel> objRoleViewModel;
            //objRoleViewModel = _role.BindRoleDetail();
            //if (objRoleViewModel != null)
            //{
            //    return Json(new { data = objRoleViewModel });
            //}
            //else
            //{
            //    return Json(new { data = objRoleViewModel });
            //}
        }
        #endregion

        #region Validate Duplicate Role        
        [HttpPost]
        public JsonResult ValidateDuplicateRole(int? RoleID, string Role)
        {
            if (_role.IsRoleExists(Convert.ToInt32(RoleID), Role.Trim()))
            {
                return Json(new { @status = "0" });
            }
            else
            {
                return Json(new { @status = "1" });
            }
        }
        #endregion

        //#region IsActive Ajax Binding
        //public IActionResult _IsActive(int id)
        //{
        //    var Verified = _role.IsActive(id);
        //    if (Verified == true)
        //    {
        //        TempData["Msg"] = "active";
        //    }
        //    else
        //    {
        //        TempData["Msg"] = "inActive";
        //    }
        //    return Json(Verified);
        //}
        //#endregion
    }
}