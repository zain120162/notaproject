﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Context;
using Nota.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class RolePrivilegesRepository : IRolePrivileges
    {
        #region Member Declaration
        private readonly ApplicationDbContext _context;
        public IConfiguration Configuration { get; }
        #endregion

        #region Constractor
        public RolePrivilegesRepository(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }
        #endregion

        #region Delete Role Privileges By Role ID
        public void DeleteRolePrivilegesByRoleID(int RoleID)
        {
            _context.tblRolePrivileges.RemoveRange(_context.tblRolePrivileges.Where(c => c.RoleID == RoleID));
            _context.SaveChanges();
        }
        #endregion

        #region Get User List
        public List<DropDownBindModel> GetUserSelectList(int? RoleID)
        {
            List<DropDownBindModel> objUserList = (from p in _context.tblUser
                                                       where p.IsActive == true
                                                       && p.RoleID == RoleID
                                                       select new DropDownBindModel
                                                       {
                                                           value = p.UserID,
                                                           name = p.FirstName + " " + p.LastName
                                                       }).ToList();
            return objUserList;
        }
        #endregion

        #region Execute Store Procedure
        public DataTable ExecuteStoredProcedure(string strSpName, int RoleID)
        {
            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];
            MySqlConnection con = null;
            DataTable dt = null;
            using (con = new MySqlConnection(connectionString))
            {
                con.Open();
                MySqlCommand sqlcmd = new MySqlCommand();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Connection = con;
                sqlcmd.CommandText = strSpName;
                MySqlDataAdapter sqladp = new MySqlDataAdapter(sqlcmd);
                sqlcmd.Parameters.Add(new MySqlParameter("@p_RoleID", RoleID));
                sqlcmd.Prepare();
                dt = new DataTable();
                sqladp.Fill(dt);
            }
            return dt;
        }
        #endregion

        #region Get ParentIDs By Role
        public List<int?> GetParentIDsByRole(int roleID, bool isAdminUser)
        {
            List<int?> parentList = (from p in _context.vwRolePrivileges
                                     where p.RoleID == roleID && p.View == true && p.ParentID > 0
                                     && p.IsActive == 1
                                     select p.ParentID).ToList();
            return parentList;
        }
        #endregion

        #region Get Main Menu List      
        public IList<vwRolePrivileges> GetMainMenuList(int RoleID, List<int?> ParentList, bool isAdminUser)
        {

            List<vwRolePrivileges> objMainMenuFinal = new List<vwRolePrivileges>();
            List<vwRolePrivileges> objMainMenu = new List<vwRolePrivileges>();
            List<vwRolePrivileges> objEmpMenu = new List<vwRolePrivileges>();
            objMainMenu = _context.vwRolePrivileges.Where(p => p.ParentID == 0 &&
                        (isAdminUser ? p.IsAdminUser == isAdminUser : p.RoleID == RoleID)).OrderBy(p => p.SortOrder).ToList();

            objMainMenuFinal = objMainMenu.Union(objEmpMenu).Distinct().ToList();
            objMainMenu = objMainMenu.Intersect(objEmpMenu).ToList();
            objMainMenu = objMainMenu.Except(objEmpMenu).ToList();
            return objMainMenuFinal;
        }
        #endregion

        #region Get Menu List        
        public IList<vwRolePrivileges> GetMenuList(int RoleID, bool isAdminUser)
        {
            List<vwRolePrivileges> objMenuList = _context.vwRolePrivileges.Where(p => p.View == true && p.ParentID != 0 && p.IsActive == 1 &&
                                                (isAdminUser ? p.IsAdminUser == isAdminUser : p.RoleID == RoleID))
                                                .OrderBy(p => p.OrderID).ThenBy(p => p.ParentID).ThenBy(p => p.SortOrder).ThenBy(p => p.MenuItem).ToList();
            if (!isAdminUser)
            {
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    List<vwRolePrivileges> objEmployeeMenuList = db.vwRolePrivileges.Where(p => p.RoleID == RoleID && p.ParentID != 0
                                                    && p.IsActive == 1).OrderBy(p => p.OrderID).ThenBy(p => p.ParentID).ThenBy(p => p.SortOrder).ThenBy(p => p.MenuItem).ToList();
                    foreach (var item in objEmployeeMenuList)
                    {
                        var menu = objMenuList.Where(p => p.MenuItemID == item.MenuItemID).FirstOrDefault();
                        if (menu != null)
                        {
                            if (item.View.HasValue && item.View.Value)
                                menu.View = item.View;
                            if (item.Add.HasValue && item.Add.Value)
                                menu.Add = item.Add;
                            if (item.Edit.HasValue && item.Edit.Value)
                                menu.Edit = item.Edit;
                            if (item.Detail.HasValue && item.Detail.Value)
                                menu.Detail = item.Detail;
                            if (item.Delete.HasValue && item.Delete.Value)
                                menu.Delete = item.Delete;
                        }
                        else
                        {
                            objMenuList.Add(item);
                        }
                    }
                }
            }
            return objMenuList.OrderBy(p => p.SortOrder).ToList();

        }
        #endregion
    }
}