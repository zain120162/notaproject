﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Repository
{
    public class EventSubmissionRepository : IEventSubmission
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion

        #region Constractor
        public EventSubmissionRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        public List<EventSubmissionModel> GetAll(int Userid, int eventid)
        {
            var result = (from u in _context.tblEventSubmission
                          where u.UserID == Userid && u.EventID == eventid
                          select new EventSubmissionModel
                          {
                              EventID = u.EventID,
                              UserID = u.UserID,
                              ContentType = u.ContentType,
                              EventsubID = u.EventsubID,
                              isActive = u.isActive,
                              Name = u.Name,
                              Path = u.Path,
                          }).ToList();
            return result;
        }
    }
}
