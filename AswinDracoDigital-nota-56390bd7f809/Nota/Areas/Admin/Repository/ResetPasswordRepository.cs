﻿using Nota.Areas.Admin.Interface;
using Nota.Context;
using Nota.Models;

namespace Nota.Areas.Admin.Repository
{
    public class ResetPasswordRepository : IResetPassword
    {
        #region Member Declaration
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public ResetPasswordRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Edid User Password       
        public void EditUserPassword(string password, int userID)
        {
            tblUser _tblUser = _context.tblUser.Find(userID);
            _tblUser.Password = password;
            _context.SaveChanges();
        }
        #endregion
    }
}