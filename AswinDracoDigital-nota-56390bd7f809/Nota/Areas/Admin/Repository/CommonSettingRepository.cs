﻿using Nota.Areas.Admin.Interface;
using Nota.Context;
using Nota.Models;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class CommonSettingRepository : ICommonSetting
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public CommonSettingRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Common Setting
        public tblCommonSetting GetCommonSetting()
        {
            return _context.tblCommonSetting.FirstOrDefault();
        }
        #endregion
    }
}