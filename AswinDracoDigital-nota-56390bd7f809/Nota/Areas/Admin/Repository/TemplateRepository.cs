﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Context;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class TemplateRepository : ITemplate
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public TemplateRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Get Email Template Content
        public TemplateModel GetEmailTemplateContent(string templateName)
        {
            TemplateModel obj = new TemplateModel();
            var Templates = (from p in _context.tblTemplate
                             where p.TemplateName.ToUpper() == templateName.ToUpper()
                             select p).FirstOrDefault();

            obj.TemplateID = Templates.TemplateID;
            obj.TemplateName = Templates.TemplateName;
            obj.Subject = Templates.Subject;
            obj.Body = Templates.Body;
            return obj;
        }
        #endregion
    }
}