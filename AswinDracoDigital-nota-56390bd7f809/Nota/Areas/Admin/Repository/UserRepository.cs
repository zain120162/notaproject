﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Areas.Admin.Helpers;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class UserRepository : IUser
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion

        #region Constractor
        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Is User Exists
        public bool IsUserExists(int userID, string emailID)
        {
            bool result = (from r in _context.tblUser
                           where (userID == 0 || r.UserID != userID)
                           && (string.Equals(r.Email.Trim(), emailID, StringComparison.OrdinalIgnoreCase))
                           select r.UserID).Any();
            return result;
        }
        #endregion

        #region Get Role List
        public List<UserModel> GetRoleList(int roleID, string roleName)
        {
            List<UserModel> RoleList = (from c in _context.tblRole
                                            where (string.IsNullOrEmpty(roleName) || c.Role == roleName)
                                            && (roleID == 0 || c.RoleID != roleID)
                                            && c.IsActive == true
                                            orderby c.Role
                                            select new UserModel
                                            {
                                                RoleID = c.RoleID,
                                                Role = c.Role,
                                            }).ToList();
            return RoleList;
        }
        #endregion

        #region Bind User Detail
        public IQueryable<UserModel> BindUserDetail()
        {
            var UserDetail = from u in _context.tblUser
                                 //where u.Email != GlobalCode.AdminEmailAddress
                             orderby u.UserID
                             select new UserModel
                             {
                                  UserID = u.UserID,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  Email = u.Email,
                                  Password = u.Password,
                                  RoleID = u.RoleID,
                                  Role = u.tblRole.Role,
                                  IsActive = u.IsActive,
                                  IsAdminUser = u.IsAdminUser,
                                  IsAdminRole = u.tblRole.IsActive ?? false,
                                  IsCurrentAdminUser = CurrentAdminSession.UserID == u.UserID ? true : false,
                                  Active = u.IsActive == true ? GlobalCode.activeText : GlobalCode.inActiveText,
                              };
            return UserDetail;
        }
        #endregion

        #region Add User
        public void AddUser(UserModel objUserModel)
        {
            tblUser objTblUser = new tblUser();
            objTblUser.FirstName = objUserModel.FirstName.Trim();
            objTblUser.LastName = objUserModel.LastName.Trim();
            objTblUser.Email = objUserModel.Email;
            objTblUser.Phone = objUserModel.Phone;
            objTblUser.RoleID = objUserModel.RoleID;
            objTblUser.Password = GlobalCode.Encryption.Encrypt(objUserModel.Password);
            objTblUser.IsAdminUser = objUserModel.IsAdminUser;
            objTblUser.IsActive = objUserModel.IsActive;
            _context.tblUser.Add(objTblUser);
            _context.SaveChanges();
        }
        #endregion

        #region Edit User
        public void EditUser(UserModel objUserModel)
        {
            tblUser objTblUser = _context.tblUser.Where(u => u.UserID == objUserModel.UserID).FirstOrDefault();
            if (objTblUser != null)
            {
                objTblUser.FirstName = objUserModel.FirstName.Trim();
                objTblUser.LastName = objUserModel.LastName.Trim();
                objTblUser.Phone = objUserModel.Phone;
                objTblUser.Email = objUserModel.Email;
                objTblUser.RoleID = objUserModel.RoleID;
                objTblUser.Password = GlobalCode.Encryption.Decrypt(objUserModel.Password);
                objTblUser.IsAdminUser = objUserModel.IsAdminUser;
                objTblUser.IsActive = objUserModel.IsActive;
                _context.SaveChanges();
            }
        }
        #endregion

        #region Get User Detail By User ID
        public UserModel GetUserDetailByUserID(int userID)
        {
            UserModel objUserViewModel = (from u in _context.tblUser
                                              join r in _context.tblRole on u.RoleID equals r.RoleID
                                              where u.UserID == userID
                                              select new UserModel
                                               {
                                                  UserID = u.UserID,
                                                  Email = u.Email,
                                                  Password = GlobalCode.Encryption.Decrypt(u.Password),
                                                  FirstName = u.FirstName,
                                                  LastName = u.LastName,
                                                  Phone = u.Phone,
                                                  RoleID = r.RoleID,
                                                  Role = r.Role,
                                                  AdminUser = u.IsAdminUser ? "Yes" : "No",
                                                  IsActive = u.IsActive,
                                                  Active = u.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                  IsAdminUser = u.IsAdminUser,
                                              }).FirstOrDefault();
            return objUserViewModel;
        }
        #endregion

        #region Is User Exists By Email
        public bool IsUserExistsByEmail(string fieldList, string valueList, int strAddOrEditID)
        {
            bool IsExists = false;
            string strUserName = string.Empty;
            if (!string.IsNullOrEmpty(fieldList))
            {
                if (strAddOrEditID == -1 && strAddOrEditID != 0)
                {
                    strUserName = UserRepository.GetUserCount(string.Empty, valueList.Trim(), 0) == 0 ? null : "1";
                }
                else if (strAddOrEditID != 0)
                {
                    strUserName = UserRepository.GetUserCount(string.Empty, valueList.Trim(), strAddOrEditID) == 0 ? null : "1";
                }

                if (!string.IsNullOrEmpty(strUserName))
                {
                    IsExists = true;
                }
            }
            return IsExists;
        }
        #endregion

        #region Get User Count
        public static int GetUserCount(string userName, string email, int userID)
        {
            return (from p in _context.tblUser
                    where (string.IsNullOrEmpty(email) || p.Email == email) && (userID == 0 || p.UserID != userID)
                    select p.UserID).Count();
        }
        #endregion

        #region Is User Email Exists
        public bool IsUserEmailExists(int UserID, string Email)
        {
            bool result = (from p in _context.tblUser
                           where (UserID == 0 || p.UserID != UserID) && (string.Equals(p.Email.Trim(), Email, StringComparison.OrdinalIgnoreCase))
                           select p.UserID).Any();
            return result;
        }
        #endregion

        #region Delete User
        public void DeleteUser(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblUser.RemoveRange(_context.tblUser.Where(r => IdsToDelete.Contains(r.UserID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Get Active User By Email
        public UserModel GetActiveUserByEmail(string email)
        {
            var userDetail = (from u in _context.tblUser
                              where u.Email == email && u.IsActive
                              select new UserModel
                              {
                                  UserID = u.UserID,
                                  RoleID = u.RoleID,
                                  Password = u.Password,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  Phone = u.Phone,
                                  Email = u.Email,
                                  IsActive = u.IsActive,
                                  Active = u.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                  IsAdminUser = u.IsAdminUser,
                              }).FirstOrDefault();
            return userDetail;
        }
        #endregion

        #region Get User By ID
        public UserModel GetUserByID(int userID)
        {
            UserModel objUserViewModel = (from u in _context.tblUser
                                              where u.UserID == userID
                                              select new UserModel
                                              {
                                                  UserID = u.UserID,
                                                  Email = u.Email,
                                                  Password = u.Password,
                                                  FirstName = u.FirstName,
                                                  LastName = u.LastName,
                                                  Phone = u.Phone,
                                                  Active = u.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                  AdminUser = u.IsAdminUser ? "Yes" : "No",
                                              }).FirstOrDefault();
            return objUserViewModel;

        }
        #endregion

        #region IsActive
        public bool IsActive(int id)
        {
            var data = _context.tblUser.Where(l => l.UserID == id).FirstOrDefault();
            if (data.IsActive == false)
            {
                bool status = data.IsActive = true;
                _context.SaveChanges();
                return status;
            }
            else
            {
                bool status = data.IsActive = false;
                _context.SaveChanges();
                return status;
            }
        }
        #endregion
    }
}