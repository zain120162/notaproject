﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class RoleRepository : IRoles
    {
        #region Member Declaration
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public RoleRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Add Role
        public void SaveChanges(RoleModel objRoleModel)
        {
            tblRole objTblRole = new tblRole();
            objTblRole.Role = objRoleModel.Role.Trim();
            objTblRole.Description = !string.IsNullOrEmpty(objRoleModel.Description) ? objRoleModel.Description.Trim() : objRoleModel.Description;
            objTblRole.IsActive = objRoleModel.IsActive;
            objTblRole.IsAdminRole = objRoleModel.IsAdminRole;
            _context.tblRole.Add(objTblRole);
            _context.SaveChanges();
        }
        #endregion

        #region Update Role
        public void UpdateRole(RoleModel objRoleModel)
        {
            var objTblRole = _context.tblRole.Where(r => r.RoleID == objRoleModel.RoleID).FirstOrDefault();
            if (objTblRole != null)
            {
                objTblRole.Role = objRoleModel.Role.Trim();
                objTblRole.Description = objRoleModel.Description;
                objTblRole.IsActive = objRoleModel.IsActive;
                _context.SaveChanges();
            }
        }
        #endregion

        #region Bind Role Detail
        public IQueryable<RoleModel> BindRoleDetail()
        {
            var RoleDetail = (from r in _context.tblRole
                              orderby r.Role
                              select new RoleModel
                              {
                                  RoleID = r.RoleID,
                                  Role = r.Role,
                                  Description = r.Description,
                                  Active = r.IsActive == true ? GlobalCode.activeText : GlobalCode.inActiveText,
                                  AdminRole = r.IsAdminRole == true ? GlobalCode.YesText : GlobalCode.NoText,
                              });
            return RoleDetail;
        }
        #endregion

        #region Get Role Detail By Role ID
        public RoleModel GetRoleDetailByRoleID(int roleID)
        {
            RoleModel objRoleViewModel = (from r in _context.tblRole
                                              where r.RoleID == roleID
                                              select new RoleModel
                                              {
                                                  RoleID = r.RoleID,
                                                  Role = r.Role,
                                                  Description = r.Description,
                                                  IsActive = (r.IsActive.HasValue ? r.IsActive.Value : false),
                                                  Active = (r.IsActive == true ? GlobalCode.activeText : GlobalCode.inActiveText),
                                              }).FirstOrDefault();
            return objRoleViewModel;
        }
        #endregion

        #region Delete Role
        public void DeleteRole(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblRole.RemoveRange(_context.tblRole.Where(r => IdsToDelete.Contains(r.RoleID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Is Role Exists
        public bool IsRoleExists(int roleID, string roleName)
        {
            bool result = (from r in _context.tblRole
                           where (roleID == 0 || r.RoleID != roleID)
                           && (string.Equals(r.Role.Trim(), roleName, StringComparison.OrdinalIgnoreCase))
                           select r.RoleID).Any();
            return result;
        }
        #endregion

        #region Get Active And Non Admin Role List
        public List<tblRole> GetActiveAndNonAdminRoleList()
        {
            return _context.tblRole.Where(c => c.IsActive == true && !c.IsAdminRole).OrderBy(c => c.Role).ToList();
        }
        #endregion

        //#region IsActive
        //public bool IsActive(int id)
        //{
        //    var data = _context.tblRole.Where(l => l.RoleID== id).FirstOrDefault();
        //    if (data.IsActive == false)
        //    {
        //        bool status = data.IsActive = true;
        //        _context.SaveChanges();
        //        return status;
        //    }
        //    else
        //    {
        //        bool status = data.IsActive = false;
        //        _context.SaveChanges();
        //        return status;
        //    }
        //}
        //#endregion
    }
}