﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Repository
{
    public class ManageUserRepository : IManageUser
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion

        #region Constractor
        public ManageUserRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Get User Detail By User ID
        public UserModel GetUserDetailByUserID(int userID)
        {
            UserModel objUserViewModel = (from u in _context.tblPublicUser
                                          where u.PublicUserID == userID
                                          select new UserModel
                                          {
                                              UserID = u.PublicUserID,
                                              Email = u.Email,
                                              Password = GlobalCode.Encryption.Decrypt(u.Password),
                                              FirstName = u.FirstName,
                                              LastName = u.LastName,
                                              Phone = u.Phone,
                                          }).FirstOrDefault();
            return objUserViewModel;
        }
        #endregion

        #region Bind User Detail
        public IQueryable<UserModel> BindUserDetail()
        {
            var UserDetail = from u in _context.tblPublicUser
                                 //where u.Email != GlobalCode.AdminEmailAddress
                             orderby u.PublicUserID
                             select new UserModel
                             {
                                 UserID = u.PublicUserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Email = u.Email,
                                 Password = u.Password,
                             };
            return UserDetail;
        }
        #endregion
        #region Edit User
        public void EditUser(UserModel objPublicUserModel)
        {
            tblPublicUser objTblPublicUser = _context.tblPublicUser.Where(u => u.PublicUserID == objPublicUserModel.UserID).FirstOrDefault();
            if (objTblPublicUser != null)
            {
                objTblPublicUser.FirstName = objPublicUserModel.FirstName.Trim();
                objTblPublicUser.LastName = objPublicUserModel.LastName.Trim();
                objTblPublicUser.Phone = objPublicUserModel.Phone;
                objTblPublicUser.Email = objPublicUserModel.Email;
                _context.SaveChanges();
            }
        }
        #endregion

    }
}
