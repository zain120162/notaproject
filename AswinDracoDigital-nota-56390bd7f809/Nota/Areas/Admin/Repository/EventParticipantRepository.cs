﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Repository
{
    public class EventParticipantRepository : IEventParticipant
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion
        #region Constractor
        public EventParticipantRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        #region Add Participant
        public bool Add(EventParticipantModel eventparticipantmodel)
        {
            Nota.Models.tblEventParticipants objTbleventParticipant = new Nota.Models.tblEventParticipants();
            objTbleventParticipant.EventsID = eventparticipantmodel.EventsID;
            objTbleventParticipant.PublicUserID = eventparticipantmodel.PublicUserID;
            objTbleventParticipant.Status = eventparticipantmodel.Status;
            objTbleventParticipant.DateApplied = eventparticipantmodel.DateApplied;
            _context.tblEventParticipants.Add(objTbleventParticipant);
            return _context.SaveChanges() == 1 ? true : false;
        }
        #endregion
        #region Delete Participant
        public bool Delete(EventParticipantModel eventparticepentmodel)
        {

            _context.tblEventParticipants.Remove(_context.tblEventParticipants.Where(x => x.PublicUserID == eventparticepentmodel.PublicUserID && x.EventsID == eventparticepentmodel.EventsID).FirstOrDefault());
            return _context.SaveChanges() == 1 ? true : false;
        }
        #endregion
    }
}
