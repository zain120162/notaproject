﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Nota.Areas.Admin.EnumType;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Areas.Admin.Helpers;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class ProductRepository : IProduct
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region Constractor
        [Obsolete]
        public ProductRepository(ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
        #endregion

        #region Get Categories List
        public List<ProductModel> GetCategoriesList(int CategoriesID, string name)
        {
            List<ProductModel> CategoriesList = (from c in _context.tblCategories
                                               where (string.IsNullOrEmpty(name) || c.Name == name)
                                               && (CategoriesID == 0 || c.CategoriesID != CategoriesID)
                                               orderby c.Name
                                               select new ProductModel
                                               {
                                                   CategoriesID = c.CategoriesID,
                                                   Name = c.Name,
                                               }).ToList();
            return CategoriesList;
        }
        #endregion

        #region Is Product Exists
        public bool IsProductExists(int productID, string title)
        {
            bool result = (from l in _context.tblProducts
                           where (productID == 0 || l.ProductID != productID)
                           && (string.Equals(l.Title.Trim(), title, StringComparison.OrdinalIgnoreCase))
                           select l.ProductID).Any();
            return result;
        }
        #endregion

        #region Bind Product Detail
        public IQueryable<ProductModel> BindProductDetail()
        {
            var ProductDetail = (from a in _context.tblProducts
                                    orderby a.ProductID
                                    select new ProductModel
                                    {
                                        ProductID = a.ProductID,
                                        Title = a.Title,
                                        CategoriesID = a.CategoriesID,
                                        Description = a.Description,
                                        CoverImage = a.CoverImage,
                                        //UploadType = a.UploadType,
                                        //PriceType = a.PriceType,
                                        Amount = a.Amount,
                                        IsApproved = a.IsApproved,
                                        DiscountPercentage = a.DiscountPercentage,
                                        IsActive = a.IsActive,
                                        Active = a.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                        Approved = a.IsApproved ? GlobalCode.Approved : GlobalCode.Pending,
                                    });
            return ProductDetail;
        }
        #endregion

        #region Add Product
        [Obsolete]
        public void AddProduct(ProductModel productModel, AttachmentModel attachmentModel)
        {
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                SaveUploadedFiles(productModel, "Images", attachmentModel.Attachments);
            }
        }
        #endregion

        [Obsolete]
        public bool SaveUploadedFiles(ProductModel productModel, string folderName, IEnumerable<IFormFile> files)
        {
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string basePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(basePath))
                    System.IO.Directory.CreateDirectory(basePath);

                foreach (IFormFile attachment in files)
                {
                    string GUID = Guid.NewGuid().ToString();
                    string fileName = attachment.FileName;
                    string fileExtension = Path.GetExtension(fileName);
                    fileName = GUID + fileExtension;
                    string destinationPath = Path.Combine(basePath, fileName);
                    var stream = new FileStream(destinationPath, FileMode.Create);
                    attachment.CopyToAsync(stream);

                    var tblproduct = new tblProduct();
                    tblproduct.CoverImage = fileName;
                    tblproduct.Title = productModel.Title;
                    tblproduct.CategoriesID = productModel.CategoriesID;
                    tblproduct.IsActive = productModel.IsActive;
                    tblproduct.Description = productModel.Description;
                    tblproduct.Amount = productModel.Amount;
                    tblproduct.DiscountPercentage = productModel.DiscountPercentage;
                    tblproduct.UploadType = productModel.UploadType;
                    tblproduct.PriceType = productModel.PriceType;
                    tblproduct.IsApproved = productModel.IsApproved;
                    tblproduct.CreatedBy = CurrentAdminSession.UserID;
                    tblproduct.CreatedDate = DateTime.Now;
                    _context.tblProducts.Add(tblproduct);
                    _context.SaveChanges();
                }
            }
            return isFileUploaded;
        }

        #region Edit Product
        [Obsolete]
        public void EditProduct(ProductModel productModel, AttachmentModel attachmentModel)
        {
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                SaveEditedFiles(productModel, "Images", attachmentModel.Attachments);
            }
            else
            {
                var tblproduct = _context.tblProducts.Where(l => l.ProductID == productModel.ProductID).FirstOrDefault();
                if (tblproduct != null)
                {
                    tblproduct.Title = productModel.Title;
                    tblproduct.CategoriesID = productModel.CategoriesID;
                    tblproduct.IsActive = productModel.IsActive;
                    tblproduct.Description = productModel.Description;
                    tblproduct.Amount = productModel.Amount;
                    tblproduct.DiscountPercentage = productModel.DiscountPercentage;
                    tblproduct.UploadType = productModel.UploadType;
                    tblproduct.PriceType = productModel.PriceType;
                    tblproduct.IsApproved = productModel.IsApproved;
                    tblproduct.ModifiedBy = CurrentAdminSession.UserID;
                    tblproduct.ModifiedDate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
        }
        #endregion

        [Obsolete]
        public bool SaveEditedFiles(ProductModel productModel, string folderName, IEnumerable<IFormFile> files)
        {
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string basePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(basePath))
                    System.IO.Directory.CreateDirectory(basePath);

                foreach (IFormFile attachment in files)
                {
                    string GUID = Guid.NewGuid().ToString();
                    string fileName = attachment.FileName;
                    string fileExtension = Path.GetExtension(fileName);
                    fileName = GUID + fileExtension;
                    string destinationPath = Path.Combine(basePath, fileName);
                    var stream = new FileStream(destinationPath, FileMode.Create);
                    attachment.CopyToAsync(stream);

                    var tblproduct = _context.tblProducts.Where(l => l.ProductID == productModel.ProductID).FirstOrDefault();
                    if (tblproduct != null)
                    {
                        tblproduct.CoverImage = fileName;
                        tblproduct.Title = productModel.Title;
                        tblproduct.CategoriesID = productModel.CategoriesID;
                        tblproduct.IsActive = productModel.IsActive;
                        tblproduct.Description = productModel.Description;
                        tblproduct.Amount = productModel.Amount;
                        tblproduct.DiscountPercentage = productModel.DiscountPercentage;
                        tblproduct.UploadType = productModel.UploadType;
                        tblproduct.PriceType = productModel.PriceType;
                        tblproduct.ModifiedBy = CurrentAdminSession.UserID;
                        tblproduct.ModifiedDate = DateTime.Now;
                        _context.SaveChanges();
                    }
                }
            }
            return isFileUploaded;
        }

        #region Delete Product
        public void DeleteProduct(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblProducts.RemoveRange(_context.tblProducts.Where(r => IdsToDelete.Contains(r.ProductID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Get Product Count
        public static int GetProductCount(string title, string Employee, int productId)
        {
            return (from v in _context.tblProducts
                    where (string.IsNullOrEmpty(title) || v.Title == title)
                    && (productId == 0 || v.ProductID != productId)
                    select v.ProductID).Count();
        }
        #endregion
        
        #region Get Product Detail By Product ID
        public ProductModel GetProductDetailByProductId(int productID)
        {
            ProductModel productModel = (from a in _context.tblProducts
                                               where a.ProductID== productID
                                               select new ProductModel
                                               {
                                                   ProductID = a.ProductID,
                                                   Title = a.Title,
                                                   Description = a.Description,
                                                   CoverImage = a.CoverImage,
                                                   CategoriesID = a.CategoriesID,
                                                   UploadType = Convert.ToInt16(a.UploadType),
                                                   PriceType = Convert.ToInt16(a.PriceType),
                                                   Amount = a.Amount,
                                                   DiscountPercentage = a.DiscountPercentage,
                                                   IsActive = a.IsActive,
                                                   Active = a.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                   IsApproved = a.IsApproved,
                                                   Approved = a.IsApproved ? GlobalCode.Approved :GlobalCode.Pending,
                                               }).FirstOrDefault();
            return productModel;
        }
        #endregion

        #region IsVerified
        public bool VerifiedProduct(int id)
        {
            var data = _context.tblProducts.Where(l => l.ProductID == id).FirstOrDefault();
            data.IsApproved = true;
            _context.SaveChanges();

            return true;
        }
        #endregion

        #region IsActive
        public bool IsActive(int id)
        {
            var data = _context.tblProducts.Where(l => l.ProductID == id).FirstOrDefault();
            if (data.IsActive == false)
            {
             bool status = data.IsActive = true;
                _context.SaveChanges();
                return status;
            }
            else
            {
               bool status = data.IsActive = false;
                _context.SaveChanges();
                return status;
            }
        }
        #endregion
    }
}