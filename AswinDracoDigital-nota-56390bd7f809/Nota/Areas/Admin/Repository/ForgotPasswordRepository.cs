﻿using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Context;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class ForgotPasswordRepository : IForgotPassword
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public ForgotPasswordRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Check For Contact
        public ForgotPasswordModel CheckForContact(string email)
        {
            var validateForgotPassword = (from c in _context.tblUser
                                          where c.Email == email
                                          select new ForgotPasswordModel
                                          {
                                              UserID = c.UserID,
                                              FirstName = c.FirstName,
                                              LastName = c.LastName,
                                              Email = c.Email
                                          }).FirstOrDefault();

            return validateForgotPassword;
        }
        #endregion
    }
}