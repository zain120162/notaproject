﻿using Nota.Areas.Admin.Helpers;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Models
{
    public class ChatRepository : IChat
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion
        #region Constractor
        public ChatRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        #region Add Message
        public bool AddMessage(ChatModel chatmodel)
        {
            Nota.Models.tblChat objTblChat = new Nota.Models.tblChat();
            objTblChat.Message = chatmodel.Message;
            objTblChat.TouserID = chatmodel.TouserID;
            objTblChat.UserID = chatmodel.UserID;
            objTblChat.FromuserID = chatmodel.FromuserID;
            objTblChat.SeenTag = chatmodel.SeenTag;
            objTblChat.TimeStamp = chatmodel.TimeStamp;
            _context.tblChat.Add(objTblChat);
            return _context.SaveChanges() == 1 ? true : false;
        }
        #endregion
        #region Delete Message
        public bool DeleteMessage(int ID)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Get Messages
        public List<ChatModel> GetMessages(int userdID, int touserID)
        {
            var UserDetail = from u in _context.tblChat
                                 //where u.Email != GlobalCode.AdminEmailAddress
                             orderby u.MessageID
                             where (u.UserID == userdID || u.UserID == touserID) && (u.TouserID == touserID || u.TouserID == userdID)
                             select new ChatModel
                             {
                                 MessageID = u.MessageID,
                                 UserID = u.UserID,
                                 TouserID = u.TouserID,
                                 FromuserID = u.FromuserID,
                                 Message = u.Message,
                                 TimeStamp = u.TimeStamp,
                                 SeenTag = u.SeenTag
                             };
            return UserDetail.ToList();
        }
        #endregion
    }
}
