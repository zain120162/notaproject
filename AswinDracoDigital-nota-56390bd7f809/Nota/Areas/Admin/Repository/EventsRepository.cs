﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Nota.Areas.Admin.EnumType;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Areas.Admin.Helpers;
using Nota.Models;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Nota.Areas.Admin.Repository
{
    public class EventsRepository : IEvents
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion

        #region Constractor
        public EventsRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region IsEventExists
        public bool IsEventExists(int eventsID, string title)
        {
            bool result = (from l in _context.tblEvents
                           where (eventsID == 0 || l.EventsID != eventsID)
                           && (string.Equals(l.Title.Trim(), title, StringComparison.OrdinalIgnoreCase))
                           select l.EventsID).Any();
            return result;
        }
        #endregion

        #region Bind Events Detail
        public IQueryable<EventModel> BindEventsDetail()
        {
            var EventsDetail = (from a in _context.tblEvents
                                orderby a.EventsID
                                select new EventModel
                                {
                                    EventsID = a.EventsID,
                                    Title = a.Title,
                                    Brief = a.Brief,
                                    Description = a.Description,
                                    StartDate = a.StartDate,
                                    EndDate = a.EndDate,
                                    IsActive = a.IsActive,
                                    PostedDate = a.PostedDate,
                                    MaxParticipants = a.MaxParticipants,
                                    WinnerID = a.WinnerID,
                                    RegParticipants = a.RegParticipants,
                                });
            return EventsDetail;
        }
        #endregion

        #region Add Events
        public void AddEvents(EventModel eventModel)
        {
            var tblevents = new tblEvents();
            tblevents.Title = eventModel.Title;
            tblevents.Description = eventModel.Description;
            tblevents.Brief = eventModel.Brief;
            tblevents.EndDate = eventModel.EndDate;
            tblevents.StartDate = eventModel.StartDate;
            tblevents.PostedDate = DateTime.Now;
            tblevents.MaxParticipants = eventModel.MaxParticipants;
            tblevents.CreatedBy = CurrentAdminSession.UserID;
            tblevents.CreatedDate = DateTime.Now;
            tblevents.RegParticipants = 0;
            _context.tblEvents.Add(tblevents);
            _context.SaveChanges();
        }
        #endregion       

        #region Edit Events
        public void EditEvents(EventModel eventModel)
        {
            var tblevents = _context.tblEvents.Where(l => l.EventsID == eventModel.EventsID).FirstOrDefault();
            if (tblevents != null)
            {
                tblevents.Title = eventModel.Title;
                tblevents.Description = eventModel.Description;
                tblevents.Brief = eventModel.Brief;
                tblevents.EndDate = eventModel.EndDate;
                tblevents.StartDate = eventModel.StartDate;
                tblevents.PostedDate = DateTime.Now;
                tblevents.MaxParticipants = eventModel.MaxParticipants;
                tblevents.RegParticipants = 0;
                tblevents.ModifiedBy = CurrentAdminSession.UserID;
                tblevents.ModifiedDate = DateTime.Now;
                _context.SaveChanges();
            }
        }
        #endregion

        #region Delete Events
        public void DeleteEvents(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblEvents.RemoveRange(_context.tblEvents.Where(r => IdsToDelete.Contains(r.EventsID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Get Events Detail By Events ID
        public EventModel GetEventsDetailByEventsId(int EventsID)
        {
            EventModel eventModel = (from a in _context.tblEvents
                                     where a.EventsID == EventsID
                                     select new EventModel
                                     {
                                         EventsID = a.EventsID,
                                         Title = a.Title,
                                         Brief = a.Brief,
                                         Description = a.Description,
                                         StartDate = a.StartDate,
                                         EndDate = a.EndDate,
                                         PostedDate = a.PostedDate,
                                         MaxParticipants = a.MaxParticipants,
                                         WinnerID = a.WinnerID,
                                         RegParticipants = a.RegParticipants,
                                     }).FirstOrDefault();
            return eventModel;
        }
        #endregion
        #region Get Members  By Events ID
        public List<EventMemberModel> GetEventMembers(int eventsID)
        {
            List<EventMemberModel> eventmembermodellist = (from a in _context.tblEventParticipants
                                                           join b in _context.tblPublicUser
                                                           on a.PublicUserID equals b.PublicUserID
                                                           where a.EventsID == eventsID
                                                           select new EventMemberModel
                                                           {
                                                               EventsID = a.EventsID,
                                                               FirstName = b.FirstName,
                                                               LastName = b.LastName,
                                                               Email = b.Email,
                                                               Phone = b.Phone,
                                                               Status = a.Status,
                                                               DateApplied = a.DateApplied,
                                                               PublicUserID = a.PublicUserID
                                                           }).ToList();
            return eventmembermodellist;
        }
        #endregion

        #region IsActive
        public bool IsActive(int id)
        {
            var data = _context.tblEvents.Where(l => l.EventsID == id).FirstOrDefault();
            if (data.IsActive == false)
            {
                bool status = data.IsActive = true;
                _context.SaveChanges();
                return status;
            }
            else
            {
                bool status = data.IsActive = false;
                _context.SaveChanges();
                return status;
            }
        }
        #endregion
    }
}