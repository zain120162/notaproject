﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Nota.Areas.Admin.Interface;
using Nota.Areas.Admin.Models;
using Nota.Common;
using Nota.Context;
using Nota.Areas.Admin.Helpers;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nota.Areas.Admin.Repository
{
    public class CategoriesRepository : ICategories
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;

        #endregion

        #region Constractor
        [Obsolete]
        public CategoriesRepository(ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }
        #endregion

        #region Is Categories Exists
        public bool IsCategoriesExists(int categoriesID, string categoriesName)
        {
            bool result = (from l in _context.tblCategories
                           where (categoriesID == 0 || l.CategoriesID != categoriesID)
                           && (string.Equals(l.Name.Trim(), categoriesName, StringComparison.OrdinalIgnoreCase))
                           select l.CategoriesID).Any();
            return result;
        }
        #endregion

        #region Bind Categories Detail
        public IQueryable<CategoriesModel> BindCategoriesDetail()
        {
            var CategoriesDetail = (from a in _context.tblCategories
                              orderby a.CategoriesID
                              select new CategoriesModel
                              {
                                  CategoriesID = a.CategoriesID,
                                  Name = a.Name,
                                  Thumbnail = a.Thumbnail, 
                                  IsActive = a.IsActive,
                                  Active = a.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                              });
            return CategoriesDetail;
        }
        #endregion

        #region Add Categories
        [Obsolete]
        public void AddCategories(CategoriesModel categoriesModel, AttachmentModel attachmentModel)
        {
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                SaveUploadedFiles(categoriesModel, "Images", attachmentModel.Attachments);
            }
        }
        #endregion

        [Obsolete]
        public bool SaveUploadedFiles(CategoriesModel categoriesModel, string folderName, IEnumerable<IFormFile> files)
        {
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string basePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(basePath))
                    System.IO.Directory.CreateDirectory(basePath);

                foreach (IFormFile attachment in files)
                {
                    string GUID = Guid.NewGuid().ToString();
                    string fileName = attachment.FileName;
                    string fileExtension = Path.GetExtension(fileName);
                    fileName = GUID + fileExtension;
                    string destinationPath = Path.Combine(basePath, fileName);
                    var stream = new FileStream(destinationPath, FileMode.Create);
                    attachment.CopyToAsync(stream);

                    var tblcategories = new tblCategories();
                    tblcategories.Thumbnail = fileName;
                    tblcategories.Name = categoriesModel.Name;
                    tblcategories.IsActive = categoriesModel.IsActive;
                    tblcategories.CreatedBy = CurrentAdminSession.UserID;
                    tblcategories.CreatedDate = DateTime.Now;
                    _context.tblCategories.Add(tblcategories);
                    _context.SaveChanges();
                }
            }
            return isFileUploaded;
        }

        #region Edit Categories
        [Obsolete]
        public void EditCategories(CategoriesModel categoriesModel,AttachmentModel attachmentModel)
        {
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                SaveEditedFiles(categoriesModel, "Images", attachmentModel.Attachments);
            }
            else
            {
                var tblCategories = _context.tblCategories.Where(l => l.CategoriesID == categoriesModel.CategoriesID).FirstOrDefault();
                if (tblCategories != null)
                {
                    tblCategories.Name = tblCategories.Name;
                    tblCategories.IsActive = tblCategories.IsActive;
                    tblCategories.ModifiedBy = CurrentAdminSession.UserID;
                    tblCategories.ModifiedDate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
        }
        #endregion

        [Obsolete]
        public bool SaveEditedFiles(CategoriesModel categoriesModel, string folderName, IEnumerable<IFormFile> files)
        {
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string basePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(basePath))
                    System.IO.Directory.CreateDirectory(basePath);

                foreach (IFormFile attachment in files)
                {
                    string GUID = Guid.NewGuid().ToString();
                    string fileName = attachment.FileName;
                    string fileExtension = Path.GetExtension(fileName);
                    fileName = GUID + fileExtension;
                    string destinationPath = Path.Combine(basePath, fileName);
                    var stream = new FileStream(destinationPath, FileMode.Create);
                    attachment.CopyToAsync(stream);

                    var tblCategories = _context.tblCategories.Where(l => l.CategoriesID == categoriesModel.CategoriesID).FirstOrDefault();
                    if (tblCategories != null)
                    {
                        tblCategories.Name = tblCategories.Name;
                        tblCategories.Thumbnail = fileName;
                        tblCategories.IsActive = tblCategories.IsActive;
                        tblCategories.ModifiedBy = CurrentAdminSession.UserID;
                        tblCategories.ModifiedDate = DateTime.Now;
                        _context.SaveChanges();
                    }
                }
            }
            return isFileUploaded;
        }

        #region Delete Categpries
        public void DeleteCategories(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblCategories.RemoveRange(_context.tblCategories.Where(r => IdsToDelete.Contains(r.CategoriesID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Get Categpries Count
        public static int GetCategpriesCount(string categpriesName, string Employee, int categpriesId)
        {
            return (from v in _context.tblCategories
                    where (string.IsNullOrEmpty(categpriesName) || v.Name == categpriesName)
                    && (categpriesId == 0 || v.CategoriesID != categpriesId)
                    select v.CategoriesID).Count();
        }
        #endregion

        #region Get Categories Detail By Categories ID
        public CategoriesModel GetCategoriesDetailByCategoriesId(int categoriesID)
        {
            CategoriesModel categoriesModel = (from c in _context.tblCategories
                                                 where c.CategoriesID == categoriesID
                                                 select new CategoriesModel
                                                 {
                                                     CategoriesID = c.CategoriesID,
                                                     Name = c.Name,
                                                     Thumbnail = c.Thumbnail,
                                                     IsActive = c.IsActive,
                                                     Active = c.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                 }).FirstOrDefault();
            return categoriesModel;
        }
        #endregion

        #region IsActive
        public bool IsActive(int id)
        {
            var data = _context.tblCategories.Where(l => l.CategoriesID == id).FirstOrDefault();
            if (data.IsActive == false)
            {
                bool status = data.IsActive = true;
                _context.SaveChanges();
                return status;
            }
            else
            {
                bool status = data.IsActive = false;
                _context.SaveChanges();
                return status;
            }
        }
        #endregion
    }
}
