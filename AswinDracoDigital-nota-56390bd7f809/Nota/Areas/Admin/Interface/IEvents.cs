﻿using Nota.Areas.Admin.Models;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Interface
{
    public interface IEvents
    {
        void EditEvents(EventModel eventModel);
        void DeleteEvents(string[] ids);
        void AddEvents(EventModel eventModel);
        IQueryable<EventModel> BindEventsDetail();
        bool IsEventExists(int eventsId, string title);
        EventModel GetEventsDetailByEventsId(int eventsID);
        List<EventMemberModel> GetEventMembers(int eventsID);
        bool IsActive(int id);
    }
}