﻿namespace Nota.Areas.Admin.Interface
{
    public interface IResetPassword
    {
        void EditUserPassword(string password, int userID);
    }
}