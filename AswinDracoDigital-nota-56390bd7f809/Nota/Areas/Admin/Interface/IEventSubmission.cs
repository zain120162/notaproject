﻿using Nota.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Interface
{
    public interface IEventSubmission
    {
        List<EventSubmissionModel> GetAll(int Userid, int eventid);
    }
}
