﻿using Nota.Areas.Admin.Models;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Interface
{
    public interface IProduct
    {
        void EditProduct(ProductModel productModel, AttachmentModel attachmentModel);
        void DeleteProduct(string[] ids);
        void AddProduct(ProductModel productModel, AttachmentModel attachmentModel);
        IQueryable<ProductModel> BindProductDetail();
        bool IsProductExists(int productId, string Title);
         List<ProductModel> GetCategoriesList(int CategoriesId, string name);
         ProductModel GetProductDetailByProductId(int productID);
         bool VerifiedProduct(int id);
         bool IsActive(int id);
    }
}