﻿using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Models;
using System.Linq;

namespace Nota.Areas.Admin.Interface
{
    public interface ICategories
    {
        void EditCategories(CategoriesModel categoriesModel,AttachmentModel attachmentModel);
        void DeleteCategories(string[] ids);
        void AddCategories(CategoriesModel categoriesModel, AttachmentModel attachmentModel);
        IQueryable<CategoriesModel> BindCategoriesDetail();
        bool IsCategoriesExists(int categoriesId, string categoriesName);
         CategoriesModel GetCategoriesDetailByCategoriesId(int categoriesID);
         bool IsActive(int id);
    }
}