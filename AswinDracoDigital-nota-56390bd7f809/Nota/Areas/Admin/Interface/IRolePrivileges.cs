﻿using Nota.Areas.Admin.Models;
using Nota.Models;
using System.Collections.Generic;
using System.Data;

namespace Nota.Areas.Admin.Interface
{
    public interface IRolePrivileges
    {
        void DeleteRolePrivilegesByRoleID(int RoleID);
        List<DropDownBindModel> GetUserSelectList(int? RoleID);
        DataTable ExecuteStoredProcedure(string strSpName, int RoleID);
        List<int?> GetParentIDsByRole(int roleID, bool isAdminUser);
        IList<vwRolePrivileges> GetMainMenuList(int RoleID, List<int?> ParentList,bool isAdminUser);
        IList<vwRolePrivileges> GetMenuList(int RoleID, bool isAdminUser);
    }
}