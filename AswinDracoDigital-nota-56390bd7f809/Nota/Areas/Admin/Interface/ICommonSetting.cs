﻿using Nota.Models;

namespace Nota.Areas.Admin.Interface
{
    public interface ICommonSetting
    {
        tblCommonSetting GetCommonSetting();
    }
}