﻿using Nota.Areas.Admin.Models;

namespace Nota.Areas.Admin.Interface
{
    public interface IForgotPassword
    {
        ForgotPasswordModel CheckForContact(string email);
    }
}