﻿using Nota.Areas.Admin.Models;

namespace Nota.Areas.Admin.Interface
{
    public interface ITemplate
    {
        TemplateModel GetEmailTemplateContent(string templateName);
    }
}