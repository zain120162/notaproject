﻿using Nota.Areas.Admin.Models;
using Nota.Models;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Interface
{
    public interface IRoles
    {
        //Task UpdateRoles(ApplicationUserViewModel appUser, ApplicationUserViewModel currentUserLogin);
        void SaveChanges(RoleModel objRoleModel);
        void UpdateRole(RoleModel objRoleModel);
        IQueryable<RoleModel> BindRoleDetail();
        RoleModel GetRoleDetailByRoleID(int roleID);
        void DeleteRole(string[] ids);
        bool IsRoleExists(int roleID, string roleName);
        List<tblRole> GetActiveAndNonAdminRoleList();
        //public bool IsActive(int id);
    }
}