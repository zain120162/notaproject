﻿using Nota.Areas.Admin.Models;
using System.Linq;

namespace Nota.Areas.Admin.Interface
{
    public interface IManageUser
    {

        UserModel GetUserDetailByUserID(int userID);
        IQueryable<UserModel> BindUserDetail();
        void EditUser(UserModel objUserModel);
    }
}