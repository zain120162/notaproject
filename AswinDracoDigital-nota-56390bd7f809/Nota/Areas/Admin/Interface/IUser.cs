﻿using Nota.Areas.Admin.Models;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.Admin.Interface
{
    public interface IUser
    {
        IQueryable<UserModel> BindUserDetail();
        List<UserModel> GetRoleList(int roleID, string roleName);
        bool IsUserExists(int userID, string emailID);
        bool IsUserEmailExists(int UserID, string Email);
        void AddUser(UserModel objUserModel);
        UserModel GetUserDetailByUserID(int userID);
        bool IsUserExistsByEmail(string fieldList, string valueList, int strAddOrEditID);
        void EditUser(UserModel objUserModel);
        void DeleteUser(string[] ids);
        UserModel GetActiveUserByEmail(string email);
        UserModel GetUserByID(int userID);
         bool IsActive(int id);

    }
}