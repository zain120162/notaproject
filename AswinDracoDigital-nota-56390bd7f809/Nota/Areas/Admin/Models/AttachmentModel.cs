﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Nota.Areas.Admin.Models
{
    public class AttachmentModel
    {
        public List<IFormFile> Attachments { get; set; }
        public List<IFormFile> Lyrics { get; set; }
        public List<IFormFile> Notes { get; set; }
        public List<IFormFile> Mp3 { get; set; } 
    }
}
