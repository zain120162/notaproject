﻿namespace Nota.Areas.Admin.Models
{
    public class DropDownBindModel
    {
        public long value { get; set; }
        public string name { get; set; }
    }
    public class Select2Result
    {
        public int id { get; set; }
        public string text { get; set; }
    }
}