﻿using Nota.Common;
using Nota.Models;
using System.ComponentModel.DataAnnotations;

namespace Nota.Areas.Admin.Models
{
    public class CategoriesModel : BaseModel
    {

        public int CategoriesID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Category name")]
        [StringLength(50, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [StringLength(50, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string Thumbnail { get; set; }

        public string Active { get; set; }

        public CommonMessagesModel ModuleName { get; set; }
    }
}