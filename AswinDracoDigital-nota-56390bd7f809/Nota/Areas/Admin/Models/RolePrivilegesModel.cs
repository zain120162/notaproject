﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Common;
using System.ComponentModel.DataAnnotations;

namespace Nota.Areas.Admin.Models
{
    public class RolePrivilegesModel
    {
        public int RolePrivilegeID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Role")]
        public int RoleID { get; set; }

        public SelectList Roles { get; set; }
        public SelectList MenuItemID { get; set; }
        public CommonMessagesModel ModuleName { get; set; }
    }
}
