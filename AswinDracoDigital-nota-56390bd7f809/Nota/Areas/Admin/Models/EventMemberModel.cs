﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Models
{
    public class EventMemberModel
    {
        public int UserID { get; set; }

        public int PublicUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Phone { get; set; }
        public int? RoleID { get; set; }
        public string Role { get; set; }
        public SelectList SelectRole { get; set; }
        public string Email { get; set; }

        public bool IsActive { get; set; }
        public bool IsCurrentAdminUser { get; set; }
        public string Active { get; set; }
        public bool IsAdminUser { get; set; }
        public bool IsAdminRole { get; set; }
        public string AdminUser { get; set; }



        public int EventParticipantsID { get; set; }

        public int EventsID { get; set; }

        public DateTime DateApplied { get; set; }

        public bool Status { get; set; }
    }
}
