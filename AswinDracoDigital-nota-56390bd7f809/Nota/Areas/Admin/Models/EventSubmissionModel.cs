﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Models
{
    public class EventSubmissionModel
    {
        public int EventsubID { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string Path { get; set; }
        public bool isActive { get; set; }
        public int EventID { get; set; }
        public int UserID { get; set; }
    }
}
