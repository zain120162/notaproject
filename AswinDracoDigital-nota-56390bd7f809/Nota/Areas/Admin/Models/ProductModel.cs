﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Nota.Areas.Admin.Models
{
    public class ProductModel : BaseModel
    {
        public int ProductID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Categories")]
        public int CategoriesID { get; set; }
        public SelectList SelectCategories { get; set; }
        public string Name { get; set; }

        public int UserID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Title")]
        [StringLength(50, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string Title { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Desciption")]
        [StringLength(250, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [StringLength(250, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string CoverImage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Upload Type")]
        public Int16 UploadType { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Upload Type")]
        public Int16 PriceType { get; set; }

        public bool IsApproved { get; set; }
        public string Approved { get; set; }
        public string FileName { get; set; }

        public string Active { get; set; }

        public decimal? DiscountPercentage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Total Amount")]
        public decimal Amount { get; set; }

        public CommonMessagesModel ModuleName { get; set; }

    }
}