﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Models
{
    public class ChatModel
    {
        public int MessageID { get; set; }
        public int UserID { get; set; }
        public int TouserID { get; set; }
        public int FromuserID { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }
        public bool SeenTag { get; set; }
    }
}
