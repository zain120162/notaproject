﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.Admin.Models
{
    public class EventParticipantModel
    {
        public int EventParticipantsID { get; set; }
        public int PublicUserID { get; set; }
        public int EventsID { get; set; }
        public DateTime DateApplied { get; set; }
        public bool Status { get; set; }
    }
}
