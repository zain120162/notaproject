﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nota.Areas.User.Repository
{
    public class UserResetPasswordRepository : IUserResetPassword
    {
        #region Member Declaration
        private readonly ApplicationDbContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region Constractor
        [Obsolete]
        public UserResetPasswordRepository(ApplicationDbContext context,IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }
        #endregion

        #region Edid User Password       
        public void EditUserPassword(string password, int userID)
        {
            tblPublicUser _tblPublicUser = _context.tblPublicUser.Find(userID);
            _tblPublicUser.Password = password;
            _context.SaveChanges();
        }
        #endregion

        #region Edit Detail
        [Obsolete]
        public void EditDetail(UserChangePasswordModel userChangePasswordModel, AttachmentModel attachmentModel)
        {
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                SaveEditedFiles(userChangePasswordModel, "Images", attachmentModel.Attachments);
            }
        }
        #endregion

        [Obsolete]
        public bool SaveEditedFiles(UserChangePasswordModel userChangePasswordModel, string folderName, IEnumerable<IFormFile> files)
        {
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string basePath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(basePath))
                    System.IO.Directory.CreateDirectory(basePath);

                foreach (IFormFile attachment in files)
                {
                    string GUID = Guid.NewGuid().ToString();
                    string fileName = attachment.FileName;
                    string fileExtension = Path.GetExtension(fileName);
                    fileName = GUID + fileExtension;
                    string destinationPath = Path.Combine(basePath, fileName);
                    var stream = new FileStream(destinationPath, FileMode.Create);
                    attachment.CopyToAsync(stream);

                    var tbluser = _context.tblPublicUser.Where(l => l.PublicUserID == userChangePasswordModel.PublicUserID).FirstOrDefault();
                    if (tbluser != null)
                    {
                        tbluser.ProfilePicture = fileName;
                        tbluser.FirstName = userChangePasswordModel.FirstName;
                        tbluser.LastName = userChangePasswordModel.LastName;
                        _context.SaveChanges();
                    }
                }
            }
            return isFileUploaded;
        }
    }
}