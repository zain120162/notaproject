﻿using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Repository
{
    public class ReviewRepository: IReview
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion
        public ReviewRepository(ApplicationDbContext context) {
            _context = context;
        }
        #region Add 
        public bool Add(ReviewModel reviewmodel)
        {
            var tblreview = new tblReview();
            tblreview.ProductID = reviewmodel.ProductID;
            tblreview.PublicUserID = reviewmodel.PublicUserID;
            tblreview.Review = reviewmodel.Review;
            tblreview.ReviewDate = reviewmodel.ReviewDate;
            tblreview.ReviewStar = reviewmodel.ReviewStar;
            _context.tblReview.Add(tblreview);
            int result = _context.SaveChanges();
            return result > 0 ? true : false;
        }
        #endregion
        #region Get Product Review 
        public List<ReviewModel> Get(int productid)
        {
            try
            {
                var result = _context.tblReview.Where(x => x.ProductID == productid).Select(x => new ReviewModel { ProductID = x.ProductID, ReviewID = x.ReviewID, ReviewDate = x.ReviewDate, Review = x.Review, ReviewStar = x.ReviewStar, PublicUserID = x.PublicUserID }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Check  user Review exists for product 
        public bool CheckUserReviewExist(int productid,int publicuserid)
        {
           
            var result = _context.tblReview.Where(x => x.ProductID == productid && x.PublicUserID== publicuserid).SingleOrDefault();
            return result != null?true:false ;
           
        }
        #endregion
    }
}
