﻿using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Repository
{
    public class PaymentDetailRepository: IPaymentDetail
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public PaymentDetailRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        #region Get 
        public PaymentDetailModel Get(int PaymentDetailID) {
            try
            {
                var result = _context.tblPaymentDetail.Where(x => x.PaymentDetailID== PaymentDetailID).Select(x => new PaymentDetailModel { PaymentDetailID = x.PaymentDetailID, Amount = x.Amount, Date = x.Date, PaymentStatus = x.PaymentStatus, PublicUserID = x.PublicUserID, ReferenceNO = x.ReferenceNO }).FirstOrDefault();
                return result;
            }
            catch (Exception ex) {
                return null;
            }
        }
        #endregion
        #region Get  All
        public List<PaymentDetailModel> GetAll()
        {
            try
            {
                var result = _context.tblPaymentDetail.OrderByDescending(x => x.PaymentDetailID).Select(x => new PaymentDetailModel { PaymentDetailID = x.PaymentDetailID, Amount = x.Amount, Date = x.Date, PaymentStatus = x.PaymentStatus, PublicUserID = x.PublicUserID, ReferenceNO = x.ReferenceNO }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Get  Top Record
        public PaymentDetailModel GetTopRecord()
        {
            try
            {
                var result = _context.tblPaymentDetail.OrderByDescending(x => x.PaymentDetailID).Select(x => new PaymentDetailModel { PaymentDetailID = x.PaymentDetailID, Amount = x.Amount, Date = x.Date, PaymentStatus = x.PaymentStatus, PublicUserID = x.PublicUserID, ReferenceNO = x.ReferenceNO }).First();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Add 
        public int Add(PaymentDetailModel paymentdetailmodel)
        {
            var tblpaymentdetail = new tblPaymentDetail();
            tblpaymentdetail.Amount = paymentdetailmodel.Amount;
            tblpaymentdetail.Date = paymentdetailmodel.Date;
            tblpaymentdetail.PaymentStatus = paymentdetailmodel.PaymentStatus;
            tblpaymentdetail.ReferenceNO = paymentdetailmodel.ReferenceNO;
            tblpaymentdetail.PublicUserID = paymentdetailmodel.PublicUserID;
            _context.tblPaymentDetail.Add(tblpaymentdetail);
            int result=_context.SaveChanges();
            return result > 0 ? tblpaymentdetail.PaymentDetailID : 0 ;
        }
        #endregion
    }
}
