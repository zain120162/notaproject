﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Common;
using Nota.Context;
using Nota.Areas.User.UserHelpers;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Innostudio;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nota.ViewModel;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Nota.Areas.User.Repository
{
    public class UserProductRepository : IUserProduct
    {
        #region Member Declaration
        private readonly ApplicationDbContext _context;
        [Obsolete]
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constractor
        [Obsolete]
        public UserProductRepository(ApplicationDbContext context, IWebHostEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _httpContextAccessor = httpContextAccessor;
        }
        #endregion

        #region Get Categories List
        public List<UserProductModel> GetCategoriesList(int CategoriesID, string name)
        {
            List<UserProductModel> CategoriesList = (from c in _context.tblCategories
                                                     where (string.IsNullOrEmpty(name) || c.Name == name)
                                                     && (CategoriesID == 0 || c.CategoriesID != CategoriesID)
                                                     orderby c.Name
                                                     select new UserProductModel
                                                     {
                                                         CategoriesID = c.CategoriesID,
                                                         Name = c.Name,
                                                     }).ToList();
            return CategoriesList;
        }
        #endregion

        #region Is Product Exists
        public bool IsProductExists(int productID, string title)
        {
            bool result = (from l in _context.tblProducts
                           where (productID == 0 || l.ProductID != productID)
                           && (string.Equals(l.Title.Trim(), title, StringComparison.OrdinalIgnoreCase))
                           select l.ProductID).Any();
            return result;
        }
        #endregion

        #region Bind Product Detail
        public IQueryable<UserProductModel> BindProductDetail(int publicUserID)
        {
            var ProductDetail = (from a in _context.tblProducts
                                 where (a.CreatedBy == publicUserID)
                                 orderby a.CreatedDate
                                 select new UserProductModel
                                 {
                                     ProductID = a.ProductID,
                                     Title = a.Title,
                                     CategoriesID = a.CategoriesID,
                                     Description = a.Description,
                                     CoverImage = a.CoverImage,
                                     //UploadType = a.UploadType,
                                     //PriceType = a.PriceType,
                                     Amount = a.Amount,
                                     DiscountPercentage = a.DiscountPercentage,
                                     Approved = a.IsApproved ? GlobalCode.Approved : GlobalCode.Pending,
                                     //IsActive = a.IsActive,
                                     Active = a.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                 });
            return ProductDetail;
        }
        #endregion

        #region Add Product
        [Obsolete]
        public async Task<bool> AddProduct(UserProductModel productModel, AttachmentModel attachmentModel)
        {
            bool isUploaded = false;
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0 )            {
                isUploaded = await SaveUploadedFiles(productModel, "Images", attachmentModel.Attachments);
            }
            return isUploaded;
        }
        #endregion

        [Obsolete]
        public async Task<bool> SaveUploadedFiles(UserProductModel productModel, string folderName, List<IFormFile> files)
        {
            int userID = CurrentPublicSession.PublicUserID;
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                string webRootPath = _hostingEnvironment.WebRootPath;
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string basePath = Path.Combine(webRootPath, folderName);

                FileUploader fileUploader = new FileUploader("Attachments",
                    files,
                    _httpContextAccessor,
                    _hostingEnvironment,
                    new Dictionary<string, dynamic>() {
                { "title", "auto" },
                { "uploadDir", basePath },
                { "files", new List<Dictionary<string, dynamic>>() }
                });
                IDictionary<string, dynamic> fileList = await fileUploader.Upload();

                try
                {
                    using (ApplicationDbContext _mycontext = new ApplicationDbContext())
                    {
                        tblProduct tblproduct = new tblProduct();
                        tblproduct.CoverImage = string.Empty;
                        tblproduct.Title = productModel.Title;
                        tblproduct.CategoriesID = productModel.CategoriesID;
                        tblproduct.Description = productModel.Description;
                        tblproduct.Amount = productModel.Amount;
                        tblproduct.DiscountPercentage = productModel.DiscountPercentage;
                        tblproduct.UploadType = productModel.UploadType;
                        tblproduct.PriceType = productModel.PriceType;
                        tblproduct.CreatedBy = userID;
                        tblproduct.CreatedDate = DateTime.Now;
                        await _mycontext.tblProducts.AddAsync(tblproduct);
                        await _mycontext.SaveChangesAsync();

                        foreach (dynamic kvp in fileList["files"])
                        {
                            tblProductFile tblProductFile = new tblProductFile();
                            tblProductFile.ImageName = kvp["name"];
                            tblProductFile.ProductID = tblproduct.ProductID;
                            tblProductFile.CreatedBy = userID;
                            tblProductFile.CreatedDate = DateTime.Now;
                            await _mycontext.tblProductFile.AddAsync(tblProductFile);
                        }
                        await _mycontext.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return isFileUploaded;
        }

        //[Obsolete]
        //public bool SaveUploadedFiles(UserProductModel productModel, string folderName, IEnumerable<IFormFile> files)
        //{
        //    bool isFileUploaded = false;
        //    if (files != null && files.Count() > 0)
        //    {
        //        folderName = "ApplicationDocuments\\" + folderName + "\\";
        //        string webRootPath = _hostingEnvironment.WebRootPath;
        //        string basePath = Path.Combine(webRootPath, folderName);

        //        if (!Directory.Exists(basePath))
        //            System.IO.Directory.CreateDirectory(basePath);

        //        foreach (IFormFile attachment in files)
        //        {
        //            string GUID = Guid.NewGuid().ToString();
        //            string fileName = attachment.FileName;
        //            string fileExtension = Path.GetExtension(fileName);
        //            fileName = GUID + fileExtension;
        //            string destinationPath = Path.Combine(basePath, fileName);
        //            var stream = new FileStream(destinationPath, FileMode.Create);
        //            attachment.CopyToAsync(stream);
        //            var tblproduct = new tblProduct();
        //            tblproduct.CoverImage = fileName;
        //            if (attachment == null)
        //            {
        //                _context.tblProducts.Add(tblproduct);
        //            }
        //        }
        //    }
        //    return isFileUploaded;
        //}

        #region Edit Product
        [Obsolete]
        public async Task<bool> EditProduct(UserProductModel productModel, AttachmentModel attachmentModel)
        {
            bool isUploaded = false;
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                isUploaded = await SaveEditedFiles(productModel, "Images", attachmentModel.Attachments);
            }
            else
            {
                var tblproduct = _context.tblProducts.Where(l => l.ProductID == productModel.ProductID).FirstOrDefault();
                if (tblproduct != null)
                {
                    tblproduct.Title = productModel.Title;
                    tblproduct.CategoriesID = productModel.CategoriesID;
                    tblproduct.Description = productModel.Description;
                    tblproduct.Amount = productModel.Amount;
                    tblproduct.DiscountPercentage = productModel.DiscountPercentage;
                    tblproduct.ModifiedBy = CurrentPublicSession.PublicUserID;
                    tblproduct.ModifiedDate = DateTime.Now;
                    _context.SaveChanges();
                }
            }
            return isUploaded;
        }
        #endregion

        [Obsolete]
        public async Task<bool> SaveEditedFiles(UserProductModel productModel, string folderName, List<IFormFile> files)
        {
            int userID = CurrentPublicSession.PublicUserID;
            bool isFileUploaded = false;
            if (files != null && files.Count() > 0)
            {
                string webRootPath = _hostingEnvironment.WebRootPath;
                folderName = "ApplicationDocuments\\" + folderName + "\\";
                string basePath = Path.Combine(webRootPath, folderName);

                FileUploader fileUploader = new FileUploader("Attachments",
                    files,
                    _httpContextAccessor,
                    _hostingEnvironment,
                    new Dictionary<string, dynamic>() {
                { "title", "auto" },
                { "uploadDir", basePath },
                { "files", new List<Dictionary<string, dynamic>>() }
                });
                IDictionary<string, dynamic> fileList = await fileUploader.Upload();

                try
                {
                    using (ApplicationDbContext _mycontext = new ApplicationDbContext())
                    {
                        var tblproduct = _mycontext.tblProducts.Where(l => l.ProductID == productModel.ProductID).FirstOrDefault();
                        if (tblproduct != null)
                        {
                            tblproduct.CoverImage = string.Empty;
                            tblproduct.Title = productModel.Title;
                            tblproduct.CategoriesID = productModel.CategoriesID;
                            tblproduct.Description = productModel.Description;
                            tblproduct.Amount = productModel.Amount;
                            tblproduct.DiscountPercentage = productModel.DiscountPercentage;
                            tblproduct.ModifiedBy = userID;
                            tblproduct.ModifiedDate = DateTime.Now;
                            await _mycontext.SaveChangesAsync();
                        }
                        foreach (dynamic kvp in fileList["files"])
                        {
                            tblProductFile tblProductFile = new tblProductFile();
                            tblProductFile.ImageName = kvp["name"];
                            tblProductFile.ProductID = productModel.ProductID;
                            tblProductFile.CreatedBy = userID;
                            tblProductFile.CreatedDate = DateTime.Now;
                            await _mycontext.tblProductFile.AddAsync(tblProductFile);
                        }
                        await _mycontext.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return isFileUploaded;
        }

        #region Delete Product
        public void DeleteProduct(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblProducts.RemoveRange(_context.tblProducts.Where(r => IdsToDelete.Contains(r.ProductID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Get Product Count
        public async Task<int> GetProductCount(string title, string Employee, int productId)
        {
            return await (from v in _context.tblProducts
                          where (string.IsNullOrEmpty(title) || v.Title == title)
                          && (productId == 0 || v.ProductID != productId)
                          select v.ProductID).CountAsync();
        }
        #endregion     

        #region Get Product Detail By Product ID
        public UserProductModel GetProductDetailByProductId(int productID)
        {
            UserProductModel productModel = (from a in _context.tblProducts
                                             join u in _context.tblProductFile on a.ProductID equals u.ProductID
                                             where a.ProductID == productID
                                             select new UserProductModel
                                             {
                                                 ProductID = a.ProductID,
                                                 Title = a.Title,
                                                 ImageName = u.ImageName,
                                                 Description = a.Description,
                                                 CoverImage = a.CoverImage,
                                                 CategoriesID = a.CategoriesID,
                                                 UploadType = Convert.ToInt64(a.UploadType),
                                                 PriceType = Convert.ToInt16(a.PriceType),
                                                 Amount = a.Amount,
                                                 DiscountPercentage = a.DiscountPercentage,
                                                 IsApproved = a.IsApproved,
                                                 Approved = a.IsApproved ? GlobalCode.Approved : GlobalCode.Pending,
                                                 Active = a.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                             }).FirstOrDefault();
            return productModel;
        }
        #endregion

        #region Get Product List
        public List<UploadedFiles> GetProductList(int productID)
        {
            var productList = (from p in _context.tblProductFile
                               where p.ProductID == productID
                               select new UploadedFiles
                               {
                                   name = p.ImageName,
                                   type = "image/jpeg",
                                   file = "/ApplicationDocuments/Images/" + p.ImageName,
                                   local = "/ApplicationDocuments/Images/" + p.ImageName,
                                   data = new Data
                                   {
                                       url = "/ApplicationDocuments/Images/" + p.ImageName,
                                       Renderforce = true,
                                       thumbnail = "/ApplicationDocuments/Images/" + p.ImageName
                                   }
                                   
                               }).ToList();

            return productList;
        }
        #endregion
        #region Get All Product Detail
        public List<UserProductModel> GetAll()
        {
            var ProductDetail = (from a in _context.tblProducts
                                 orderby a.CreatedDate
                                 select new UserProductModel
                                 {
                                     ProductID = a.ProductID,
                                     Title = a.Title,
                                     CategoriesID = a.CategoriesID,
                                     Description = a.Description,
                                     CoverImage = a.CoverImage,
                                     UploadType = a.UploadType,
                                     PriceType =short.Parse(a.PriceType.ToString()),
                                     Amount = a.Amount,
                                     DiscountPercentage = a.DiscountPercentage,
                                     Approved = a.IsApproved ? GlobalCode.Approved : GlobalCode.Pending,
                                     //IsActive = a.IsActive,
                                     Active = a.IsActive ? GlobalCode.activeText : GlobalCode.inActiveText,
                                 }).ToList();
            return ProductDetail;
        }
        #endregion
    }
}