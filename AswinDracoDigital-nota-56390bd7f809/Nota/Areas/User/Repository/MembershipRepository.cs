﻿using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Common;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Repository
{
    public class MembershipRepository : IMembership
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public MembershipRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        #region Get 
        public MembershipModel Get(int publicuserid)
        {
           var result= _context.tblMembership.Where(x => x.PublicUserID == publicuserid).Select(x=> new MembershipModel {PublicUserID=x.PublicUserID,ExpiryDate=x.ExpiryDate,MembershipID=x.MembershipID,MembershipName=x.MembershipName,PaymentDetailID=x.PaymentDetailID,StartDate=x.StartDate,DownloadAllowed=x.DownloadAllowed,UploadAllowed=x.UploadAllowed,ParticipantCreate=x.ParticipantCreate,ParticipantJoin=x.ParticipantJoin,}).SingleOrDefault();
            return result;
        }
        #endregion
        #region Add 
        public int Add(MembershipModel membershipmodel)
    {
            var tblmembership = new tblMembership();
            tblmembership.StartDate = membershipmodel.StartDate;
            tblmembership.ExpiryDate = membershipmodel.ExpiryDate;
            tblmembership.MembershipName = membershipmodel.MembershipName;
            tblmembership.PaymentDetailID = membershipmodel.PaymentDetailID;
            tblmembership.PublicUserID = membershipmodel.PublicUserID;
            tblmembership.ParticipantCreate = membershipmodel.ParticipantCreate;
            tblmembership.ParticipantJoin = membershipmodel.ParticipantJoin;
            tblmembership.DownloadAllowed = membershipmodel.DownloadAllowed;
            tblmembership.UploadAllowed = membershipmodel.UploadAllowed;
            _context.tblMembership.Add(tblmembership);
            int result = _context.SaveChanges();
            return result > 0 ? tblmembership.MembershipID : 0;
        }
        #endregion
        #region Edit 
        public int Edit(MembershipModel membershipmodel)
        {
            tblMembership membershipobj = _context.tblMembership.Where(u => u.PublicUserID == membershipmodel.PublicUserID).FirstOrDefault();
            if (membershipobj != null)
            {
                membershipobj.MembershipName = membershipmodel.MembershipName;
                membershipobj.StartDate = membershipmodel.StartDate;
                membershipobj.ExpiryDate = membershipmodel.ExpiryDate;
                membershipobj.PaymentDetailID = membershipmodel.PaymentDetailID;
                membershipobj.ParticipantCreate = membershipmodel.ParticipantCreate;
                membershipobj.ParticipantJoin = membershipmodel.ParticipantJoin;
                membershipobj.DownloadAllowed = membershipmodel.DownloadAllowed;
                membershipobj.UploadAllowed = membershipmodel.UploadAllowed;
                int result=_context.SaveChanges();
                return result > 0 ? membershipobj.MembershipID : 0;
            }
            return 0;
        }
        #endregion
    }
}