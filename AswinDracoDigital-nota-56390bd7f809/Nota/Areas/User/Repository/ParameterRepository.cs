﻿using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Repository
{
    public class ParameterRepository : IParameter
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public ParameterRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        #region Get Parameter
        public ParameterModel Get(string paraname)
        {
            var result = _context.tblParameter.Where(x => x.Name == paraname).Select(x => new ParameterModel { Name = x.Name, ParameterID = x.ParameterID, isActive = x.isActive, Value = x.Value }).SingleOrDefault();
            return result;
        }
        #endregion
    }
}
