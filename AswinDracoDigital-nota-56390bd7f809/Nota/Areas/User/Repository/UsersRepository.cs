﻿using Microsoft.AspNetCore.Hosting;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Common;
using Nota.Context;
using Nota.Models;
using System.Linq;
namespace Nota.Areas.User.Repository
{
    public class UsersRepository : IUsers
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        [System.Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region Constractor
        [System.Obsolete]
        public UsersRepository(IHostingEnvironment hostingEnvironment,ApplicationDbContext context)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }
        #endregion

        #region Get User By ID
        public PublicUserModel GetUserByID(int userID)
        {
            PublicUserModel publicUserModel  = (from u in _context.tblPublicUser
                                                    where u.PublicUserID == userID
                                                    select new PublicUserModel
                                                    {
                                                        PublicUserID = u.PublicUserID,
                                                        Email = u.Email,
                                                        Password = u.Password,
                                                        FirstName = u.FirstName,
                                                        DateJoined = u.DateJoined,
                                                        ProfilePicture = u.ProfilePicture,
                                                        Description = u.Description,
                                                        LastName = u.LastName,
                                                        Phone = u.Phone,
                                                        Active = u.Status ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                    }).FirstOrDefault();
            return publicUserModel;
        }
        #endregion

        #region Get User Detail By User ID
        public UserChangePasswordModel GetUserDetailByUserID(int userID)
        {
            UserChangePasswordModel objUserViewModel = (from u in _context.tblPublicUser
                                          where u.PublicUserID == userID
                                          select new UserChangePasswordModel
                                          {
                                              PublicUserID = u.PublicUserID,
                                              ProfilePicture = u.ProfilePicture,
                                              Email = u.Email,
                                              Password = GlobalCode.Encryption.Decrypt(u.Password),
                                              FirstName = u.FirstName,
                                              LastName = u.LastName,
                                              Phone = u.Phone,
                                          }).FirstOrDefault();
            return objUserViewModel;
        }
        #endregion

        #region Is User Exists By Email
        public bool IsUserExistsByEmail(string fieldList, string valueList, int strAddOrEditID)
        {
            bool IsExists = false;
            string strUserName = string.Empty;
            if (!string.IsNullOrEmpty(fieldList))
            {
                if (strAddOrEditID == -1 && strAddOrEditID != 0)
                {
                    strUserName = UsersRepository.GetUserCount(string.Empty, valueList.Trim(), 0) == 0 ? null : "1";
                }
                else if (strAddOrEditID != 0)
                {
                    strUserName = UsersRepository.GetUserCount(string.Empty, valueList.Trim(), strAddOrEditID) == 0 ? null : "1";
                }

                if (!string.IsNullOrEmpty(strUserName))
                {
                    IsExists = true;
                }
            }
            return IsExists;
        }
        #endregion

        #region Get User Count
        public static int GetUserCount(string userName, string email, int publicUserID)
        {
            return (from p in _context.tblPublicUser
                    where (string.IsNullOrEmpty(email) || p.Email == email) && (publicUserID == 0 || p.PublicUserID != publicUserID)
                    select p.PublicUserID).Count();
        }
        #endregion

        #region Get User Detail By User ID
        public PublicUserModel GetPublicUserDetailByUserID(int userID)
        {
            PublicUserModel objUserViewModel = (from u in _context.tblPublicUser
                                                        where u.PublicUserID == userID
                                                        select new PublicUserModel
                                                        {
                                                            FirstName=u.FirstName,
                                                            LastName=u.LastName,
                                                            MembershipID=u.MembershipID,
                                                            Description=u.Description,
                                                            DateJoined=u.DateJoined,
                                                            Email=u.Email,
                                                            Status=u.Status,
                                                            Phone=u.Phone,
                                                            IsEmailVerified=u.IsEmailVerified,
                                                            IsSocialLogin=u.IsSocialLogin,
                                                            Password=u.Password,
                                                            ProfilePicture=u.ProfilePicture,
                                                            PublicUserID=u.PublicUserID,
                                                            
                                                        }).FirstOrDefault();
            return objUserViewModel;
        }
        #endregion
        #region Edit User
        public bool EditUser(PublicUserModel objPublicUserModel)
        {
            tblPublicUser objTblPublicUser = _context.tblPublicUser.Where(u => u.PublicUserID == objPublicUserModel.PublicUserID).FirstOrDefault();
            if (objTblPublicUser != null)
            {
                objTblPublicUser.FirstName = objPublicUserModel.FirstName.Trim();
                objTblPublicUser.LastName = objPublicUserModel.LastName.Trim();
                objTblPublicUser.Phone = objPublicUserModel.Phone;
                objTblPublicUser.Email = objPublicUserModel.Email;
                objTblPublicUser.DateJoined = objPublicUserModel.DateJoined;
                objTblPublicUser.Description = objPublicUserModel.Description;
                objTblPublicUser.IsEmailVerified = objPublicUserModel.IsEmailVerified;
                objTblPublicUser.IsSocialLogin = objPublicUserModel.IsSocialLogin;
                objTblPublicUser.MembershipID = objPublicUserModel.MembershipID;
                objTblPublicUser.Password = objPublicUserModel.Password;
                objTblPublicUser.ProfilePicture = objPublicUserModel.ProfilePicture;
                objTblPublicUser.PublicUserID = objPublicUserModel.PublicUserID;
                objTblPublicUser.Status = objPublicUserModel.Status;
               int result= _context.SaveChanges();
                return result > 0 ? true:false;
            }
            return false;
        }
        #endregion
    }
}