﻿using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.User.Repository
{
    public class UserPurchaseRepository : IUserPurchase
    {
        #region Member Declaration
        private static ApplicationDbContext _context;

        #endregion

        #region Constractor
        [Obsolete]
        public UserPurchaseRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Bind Purchase Detail
        public IQueryable<UserPurchaseModel> BindPurchaseDetail(int publicUserID)
        {
            var PurchaseDetail = (from a in _context.tblPurchases
                                  join u in _context.tblPublicUser on a.PublicUserID equals u.PublicUserID
                                  join p in _context.tblProducts on a.ProductID equals p.ProductID
                                  join d in _context.tblPaymentDetail on a.PaymentDetailID equals d.PaymentDetailID
                                  where(u.PublicUserID == publicUserID)
                                  select new UserPurchaseModel
                                 {
                                     PurchaseID = a.PurchaseID,
                                     FirstName = u.FirstName,
                                     LastName = u.LastName,
                                     Title = p.Title,
                                     Description = p.Description,
                                     Amount = d.Amount,
                                     DiscountPercentage = p.DiscountPercentage,
                                     PaymentDate = d.Date,
                                     Date = a.Date,
                                 });
            return PurchaseDetail;
        }
        #endregion
        #region Add 
        public bool Add(UserPurchaseModel userpurchasemodel)
        {
            var tblpurchase = new tblPurchases();
            tblpurchase.Date = userpurchasemodel.Date;
            tblpurchase.ProductID = userpurchasemodel.ProductID;
            tblpurchase.PublicUserID= userpurchasemodel.PublicUserID;
            tblpurchase.PaymentDetailID = userpurchasemodel.PaymentDetailID;
            _context.tblPurchases.Add(tblpurchase);
            int result = _context.SaveChanges();
            return result > 0 ? true :false;
        }
        #endregion
    }
}