﻿using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Repository
{
    public class CartRepository:ICart
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion
        #region Constractor
        public CartRepository( ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion
        #region Add 
        public bool Add(CartModel cartmodel)
        {
            var tblcart = new tblCart();
            tblcart.Amount = cartmodel.Amount;
            tblcart.ProductID = cartmodel.ProductID;
            tblcart.ProductName = cartmodel.ProductName;
            tblcart.PublicUserID = cartmodel.PublicUserID;
            tblcart.Qty = cartmodel.Qty;
            _context.tblCart.Add(tblcart);
            int result = _context.SaveChanges();
            return result > 0 ? true : false;
        }
        #endregion
        #region Get 
        public CartModel Get(int CartID)
        {
            try
            {
                var result = _context.tblCart.Where(x => x.CartID == CartID).Select(x => new CartModel { CartID = x.CartID, Amount = x.Amount, ProductName = x.ProductName, ProductID = x.ProductID, PublicUserID = x.PublicUserID, Qty = x.Qty }).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Get  All
        public List<CartModel> GetAll(int publicuserid)
        {
            try
            {
                var result = _context.tblCart.Where(x=>x.PublicUserID== publicuserid).Select(x => new CartModel { CartID = x.CartID, Amount = x.Amount, ProductName = x.ProductName, ProductID = x.ProductID, PublicUserID = x.PublicUserID, Qty = x.Qty }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Delete Events
        public void Delete(int productid,int publicuserid)
        {
            _context.tblCart.Remove(_context.tblCart.Where(r => r.ProductID==productid && r.PublicUserID== publicuserid).FirstOrDefault());
            _context.SaveChanges();
        }
        #endregion
    }
}
