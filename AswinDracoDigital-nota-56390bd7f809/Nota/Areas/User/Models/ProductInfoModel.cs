﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Models
{
    public class ProductInfoModel
    {
        public UserProductModel userproductmodel = new UserProductModel();
        public List<ReviewModel> productreviewlist = new List<ReviewModel>();
        //    public PublicUserModel publicusermodel = new PublicUserModel();
        public string UserFullname = "";
        public bool reviewsubmitted = false;
    }
}
