﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Models
{
    public class CcAvenueViewModel
    {
        public string EncryptionRequest;
        public string AccessCode;
        public string CheckoutUrl;
        public CcAvenueViewModel(string v, string value1, string value2)
        {
            this.EncryptionRequest = v;
            this.AccessCode = value1; 
            this.CheckoutUrl = value2;
        }
    }
}
