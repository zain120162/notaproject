﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Models
{
    public class CartModel
    {
        public int CartID { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public decimal Amount { get; set; }

        public int Qty { get; set; }

        public int PublicUserID { get; set; }
    }
}
