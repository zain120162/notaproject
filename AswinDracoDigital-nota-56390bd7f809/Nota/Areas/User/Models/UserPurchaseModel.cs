﻿using Nota.Areas.Admin.Models;
using System;

namespace Nota.Areas.User.Models
{
    public class UserPurchaseModel
    {
        public int PurchaseID { get; set; }

        public int PublicUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int FileID { get; set; }
        public string FileName { get; set; }
        public string Filepath { get; set; }

        public int ProductID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Decimal? DiscountPercentage { get; set; }

        public int PaymentDetailID { get; set; }
        public Decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }

        public string CoverImage { get; set; }

        public DateTime Date { get; set; }

        public CommonMessagesModel ModuleName { get; set; }
    }
}