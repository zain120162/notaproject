﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Models
{
    public class CCAvenueResponseModel
    {
        public string merchant_param6 { set; get; }
        public string merchant_param5 { set; get; }
        public string merchant_param4 { set; get; }
        public string merchant_param3 { set; get; }
        public string billing_name { set; get; }
        public string merchant_param2 { set; get; }
        public string status_message { set; get; }
        public string merchant_param1 { set; get; }
        public string response_type { set; get; }
        public string billing_city { set; get; }
        public string amount { set; get; }
        public string order_status { set; get; }
        public string billing_country { set; get; }
        public string billing_address { set; get; }
        public string bank_qsi_no { set; get; }
        public string discount_value { set; get; }
        public string billing_zip { set; get; }
        public string delivery_country { set; get; }
        public string billing_tel { set; get; }
        public string failure_message { set; get; }
        public string order_id { set; get; }
        public string bank_ref_no { set; get; }
        public string delivery_address { set; get; }
        public string status_code { set; get; }
        public string billing_state { set; get; }
        public string payment_mode { set; get; }
        public string vault { set; get; }
        public string delivery_state { set; get; }
        public string card_holder_name { set; get; }
        public string offer_type { set; get; }
        public string delivery_name { set; get; }
        public string offer_code { set; get; }
        public string bank_receipt_no { set; get; }
        public string tracking_id { set; get; }
        public string delivery_city { set; get; }
        public string delivery_zip { set; get; }
        public string delivery_tel { set; get; }
        public string currency { set; get; }
        public string eci_value { set; get; }
        public string card_name { set; get; }
        public string billing_email { set; get; }
        public string mer_amount { set; get; }

    }

}
