﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Models
{
    public class MembershipModel
    {
        public int MembershipID { get; set; }

        public int PaymentDetailID { get; set; }

        public Int16 MembershipName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int DownloadAllowed { get; set; }

        public int UploadAllowed { get; set; }

        public bool ParticipantJoin { get; set; }

        public bool ParticipantCreate { get; set; }

        public int PublicUserID { get; set; }
    }
}
