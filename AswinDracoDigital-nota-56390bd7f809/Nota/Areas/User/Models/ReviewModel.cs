﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Models
{
    public class ReviewModel
    {
    
        public int ReviewID { get; set; }

        public string Review { get; set; }

        public DateTime ReviewDate { get; set; }

        public int ReviewStar { get; set; }

        public int PublicUserID { get; set; }

        public int ProductID { get; set; }
    }
}
