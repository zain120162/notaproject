﻿using Nota.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nota.Areas.User.Models
{
    public class PaymentDetailModel
    {
        public int PaymentDetailID { get; set; }

        public int PublicUserID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "ReferenceNO")]
        [StringLength(50, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string ReferenceNO { get; set; }

        public DateTime Date { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Amount")]
        public Decimal Amount { get; set; }

        public Int16 PaymentStatus { get; set; }
    }
}