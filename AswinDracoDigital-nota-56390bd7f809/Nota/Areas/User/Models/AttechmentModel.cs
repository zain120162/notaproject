﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Nota.Areas.User.Models
{
    public class AttechmentModel
    {
        public IEnumerable<IFormFile> Attachments { get; set; }
        public IEnumerable<IFormFile> Notes { get; set; }
        public IEnumerable<IFormFile> Mp3 { get; set; }
        public IEnumerable<IFormFile> Lyrics { get; set; }
    }
}