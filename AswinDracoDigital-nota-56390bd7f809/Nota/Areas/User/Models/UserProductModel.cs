﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Areas.Admin.Models;
using Nota.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nota.Areas.User.Models
{
    public class UserProductModel : BaseModel
    {
        public int UserProductID { get; set; }

        public int ProductID { get; set; }

        public int CategoriesID { get; set; }
        public SelectList SelectCategories { get; set; }
        public string Name { get; set; }

        public bool addedstatus { get; set; }

        public SelectList SelectListItems { get; set; }

        public int PublicUserID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Title")]
        [StringLength(50, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string Title { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Desciption")]
        [StringLength(250, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [StringLength(250, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        public string CoverImage { get; set; }

        public string ImageName { get; set; }

        public string FileName { get; set; }

        public Int64 UploadType { get; set; }

        public Int16 PriceType { get; set; }

        public bool IsApproved { get; set; }
        public string Approved { get; set; }
        public string Active { get; set; }

        public decimal? DiscountPercentage { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }

        public bool IsEmailVerified { get; set; }
        public string ProductDetail { get; set; }

        public CommonMessagesModel ModuleName { get; set; }
    }
}