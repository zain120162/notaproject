﻿using System;

namespace Nota.Areas.User.Models
{
    public class UserMembershipModel
    {
        public int MembershipID { get; set; }

        public int PaymentDetailID { get; set; }
        public Int64 ReferenceNO { get; set; }
        public DateTime Date { get; set; }
        public Decimal Amount { get; set; }
        public Int16 PaymentStatus { get; set; }

        public Int16 MembershipName { get; set; }

        public string Name { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int DownloadAllowed { get; set; }

        public int UploadAllowed { get; set; }

        public bool ParticipantJoin { get; set; }

        public bool ParticipantCreate { get; set; }
    }
}