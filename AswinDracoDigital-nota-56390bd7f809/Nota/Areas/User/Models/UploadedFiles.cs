﻿namespace Nota.Areas.User.Models
{
    public class UploadedFiles
    {
        public string name { get; set; }
        public string type { get; set; }
        public int size { get; set; }
        public string file { get; set; }
        public string local { get; set; }

        public Data data { get; set; }

    }

    public class Data
    { 
        public string url { get; set; }
        public string thumbnail { get; set; }
        public bool Renderforce { get; set; }
    }
}