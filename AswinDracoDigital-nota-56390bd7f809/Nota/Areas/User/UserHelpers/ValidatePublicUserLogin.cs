﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Nota.Common;

namespace Nota.Areas.User.UserHelpers
{
    public class ValidatePublicUserLogin : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (CurrentPublicSession.PublicUserID < 0)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.HttpContext.Response.Clear();
                    filterContext.HttpContext.Response.StatusCode = 401;
                    filterContext.HttpContext.Response.CompleteAsync();
                    filterContext.Result = new JsonResult(new { Data = "Unauthorize" });
                    return;
                }
                filterContext.HttpContext.Session.Clear();
                foreach (var cookieKey in filterContext.HttpContext.Request.Cookies.Keys)
                {
                    filterContext.HttpContext.Response.Cookies.Delete(cookieKey);
                }
                try
                {
                    var controllerName = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ControllerName;
                    var actionName = ((ControllerBase)filterContext.Controller).ControllerContext.ActionDescriptor.ActionName;
                    filterContext.Result = new RedirectResult("~/Home/Index" + controllerName + "/" + actionName);
                }
                catch (Exception)
                {
                    
                    filterContext.Result = new RedirectResult("~/Home/Index");
                    
                }
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
    public static class HttpRequestExtensions
    {
        private const string RequestedWithHeader = "X-Requested-With";
        private const string XmlHttpRequest = "XMLHttpRequest";
        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            if (request.Headers != null)
            {
                return request.Headers[RequestedWithHeader] == XmlHttpRequest;
            }
            return false;
        }
    }
}