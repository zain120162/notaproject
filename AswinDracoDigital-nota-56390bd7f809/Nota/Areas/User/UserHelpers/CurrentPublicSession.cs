﻿using Microsoft.AspNetCore.Http;
using System;

namespace Nota.Areas.User.UserHelpers
{
    public class CurrentPublicSession
    {
        private static HttpContextAccessor _HttpContextAccessor = new HttpContextAccessor();
        public static CurrentPublicUser PublicUser
        {
            get
            {
                CurrentPublicUser PublicUser = _HttpContextAccessor.HttpContext.Session.GetObject<CurrentPublicUser>("CurrentPublicUser");
                return PublicUser;
            }
            set
            {
                _HttpContextAccessor.HttpContext.Session.SetObject("CurrentPublicUser", value);
            }
        }

        public static int PublicUserID
        {
            get
            {
                if (PublicUser != null)
                    return PublicUser.PublicUserID;
                else
                    return -1;
            }
        }

        public static string FirstName
        {
            get
            {
                if (PublicUser != null)
                    return PublicUser.FirstName;
                else
                    return string.Empty;
            }
        }
        public static string LastName
        {
            get
            {
                if (PublicUser != null)
                    return PublicUser.LastName;
                else
                    return string.Empty;
            }
        }
        public static string UserName
        {
            get
            {
                if (PublicUser != null)
                    return PublicUser.UserName;
                else
                    return string.Empty;
            }
        }
        public static string Email
        {
            get
            {
                if (PublicUser != null)
                    return PublicUser.Email;
                else
                    return string.Empty;
            }
        }
       
    }
    [Serializable]
    public class CurrentPublicUser
    {
        public int PublicUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }

}