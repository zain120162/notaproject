﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CCA.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Areas.User.UserHelpers;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    [ValidatePublicUserLogin]
    public class CartController : Controller
    {
        #region Member Declaration
        private readonly ICart _cart;
        private readonly IUserProduct _userproduct;
        private readonly IParameter _param;
        private readonly IPaymentDetail _paymentdetail;
        private readonly IUserPurchase _userpurchase;
        private readonly IReview _review;
        private readonly IUsers _user;
        #endregion
        #region Constructor
        public CartController(ICart cart, IUserProduct userproduct, IParameter param, IPaymentDetail paymentdetail, IUserPurchase userpurchase, IReview review,IUsers user)
        {
            _cart = cart;
            _userproduct = userproduct;
            _param=param;
            _paymentdetail=paymentdetail;
            _userpurchase = userpurchase;
            _review= review;
            _user = user;
        }
        #endregion
        #region Index
        public IActionResult Index()
        {
            var result = _cart.GetAll(UserHelpers.CurrentPublicSession.PublicUserID);
            return View(result);
        }
        #endregion
        #region Add item to cart
        public IActionResult AddItem(int id)
        {
            List<CartModel> obj =HttpContext.Session.GetObject<List<CartModel>>("Cart");
            if (obj == null) {
                obj = new List<CartModel>();
            }
            var itemdetail=_userproduct.GetProductDetailByProductId(id);
            if (itemdetail != null)
            {
                CartModel cartmodel = new CartModel();
                cartmodel.PublicUserID = UserHelpers.CurrentPublicSession.PublicUserID;
                cartmodel.ProductID = id;
                cartmodel.ProductName = itemdetail.Title;
                cartmodel.Qty = 1;
                cartmodel.Amount = itemdetail.Amount;
                bool result=_cart.Add(cartmodel);
                obj.Add(cartmodel);
                HttpContext.Session.SetObject("Cart", obj);
                HttpContext.Session.SetString("cartcount", obj.Count.ToString());
                return RedirectToAction("Index","Library");
            }
            else {
                return View("Index");
            }
        }
        #endregion
        #region Remove item to cart
        public IActionResult RemoveItem(int id)
        {
            List<CartModel> obj = HttpContext.Session.GetObject<List<CartModel>>("Cart");
            if (obj == null)
            {
                obj = new List<CartModel>();
            }
            _cart.Delete(id, UserHelpers.CurrentPublicSession.PublicUserID);
            obj.Remove(obj.Where(x => x.ProductID == id).SingleOrDefault());
            HttpContext.Session.SetObject("Cart", obj);
            HttpContext.Session.SetString("cartcount", obj.Count.ToString());
            return View("Index",obj);

        }
        #endregion
        #region View Item Details
        public IActionResult ViewItem(int id)
        {
            var itemdetail = _userproduct.GetProductDetailByProductId(id);
            ProductInfoModel productinfomodel = new ProductInfoModel();
            productinfomodel.userproductmodel = itemdetail;
            productinfomodel.productreviewlist = _review.Get(id);
            productinfomodel.reviewsubmitted=_review.CheckUserReviewExist(id, UserHelpers.CurrentPublicSession.PublicUserID);
            productinfomodel.UserFullname = UserHelpers.CurrentPublicSession.FirstName + " " + UserHelpers.CurrentPublicSession.LastName;
            //productinfomodel.publicusermodel = _user.GetPublicUserDetailByUserID(UserHelpers.CurrentPublicSession.);
            return View(productinfomodel);
        }
        #endregion
        #region Checkout
        public IActionResult CheckOut()
        {
            var lastpaymentrecord = _paymentdetail.GetTopRecord();
            string[] values;
            string orderid = "";
            string firstoperand = "";
            if (lastpaymentrecord != null)
            {
                values = lastpaymentrecord.ReferenceNO.Split("_");
                firstoperand = (int.Parse(values[0]) + 1).ToString();
                orderid = firstoperand + "_" + DateTime.Now.Date.Day.ToString() + DateTime.Now.Date.Month.ToString() + DateTime.Now.Date.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
            }
            else
            {
                firstoperand = "1";
                orderid = firstoperand + "_" + DateTime.Now.Date.Day.ToString() + DateTime.Now.Date.Month.ToString() + DateTime.Now.Date.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
            }
            var guid = Guid.NewGuid();
            List<CartModel> obj = HttpContext.Session.GetObject<List<CartModel>>("Cart");
            firstoperand = guid.ToString()+ DateTime.Now.Date.Hour+"/"+DateTime.Now.Date.Minute+"/"+ DateTime.Now.Date.Millisecond;
            var queryParameter = new CCACrypto();
            string productids = "";
            foreach (var item in obj) {
                productids =  item.ProductID+"-"+productids;
            }
            return View("CcAvenue", new CcAvenueViewModel(queryParameter.Encrypt(BuildCcAvenueRequestParameters(orderid, productids, orderid, obj.Sum(x=>x.Amount).ToString(), _param.Get("TestMerchantid").Value), _param.Get("TestEncryptionkey").Value), _param.Get("TestAccesscode").Value, _param.Get("TestUrl").Value));
           //return View();
        }
        #endregion
        #region ccavenue request parameter
        private string BuildCcAvenueRequestParameters(string orderId,string productids, string tid, string amount, string merchentid)
        {

            var queryParameters = new Dictionary<string, string>
            {
            {"order_id", orderId},
            {"merchant_id", merchentid},
            {"amount", amount},
            {"merchant_param5", UserHelpers.CurrentPublicSession.PublicUserID.ToString()},
            {"merchant_param4", productids},
            {"currency","AED" },
            {"tid",tid },
            {"redirect_url","https://nota20200819034315.azurewebsites.net/User/Cart/payment_successful" },
            {"cancel_url","https://nota20200819034315.azurewebsites.net/User/Cart/payment_cancelled"},
            {"request_type","JSON" },
            {"response_type","JSON" },
            {"version","1.1" }
            //add other key value pairs here.
            }.Select(item => string.Format("{0}={1}", item.Key, item.Value));
            return string.Join("&", queryParameters);

        }
        #endregion
        #region payment successfull
        [HttpPost]
        public IActionResult payment_successful(string encResp)
        {
            /*Do not change the encResp.If you change it you will not get any response.
            Decode the encResp*/
            var decryption = new CCACrypto();
            var decryptedParameters = decryption.Decrypt(encResp,
            _param.Get("TestEncryptionkey").Value);
            ViewData["decryptedParameters"] = decryptedParameters;
            //  dynamic json = JsonConvert.DeserializeObject(decryptedParameters);
            var myDetails = JsonConvert.DeserializeObject<CCAvenueResponseModel>(decryptedParameters);
            PaymentDetailModel paymentdetailmodel = new PaymentDetailModel();
            if (myDetails.order_status == "Success")
            {
                ViewData["Msg"] = "Thank you payment has been made";
                paymentdetailmodel.PaymentStatus = 1;
                paymentdetailmodel.ReferenceNO = myDetails.order_id;
                paymentdetailmodel.Amount = decimal.Parse(myDetails.amount);
                paymentdetailmodel.PublicUserID = int.Parse(myDetails.merchant_param5);
                paymentdetailmodel.Date = DateTime.Now;
                int result = _paymentdetail.Add(paymentdetailmodel);
                string[] productids = myDetails.merchant_param4.Split('-');
                for (int i = 0; i < productids.Length; i++)
                {
                    if (result > 0)
                    {
                        UserPurchaseModel userpurchasemodel = new UserPurchaseModel();
                        userpurchasemodel.PaymentDetailID = result;
                        userpurchasemodel.Date = DateTime.Now;
                        userpurchasemodel.PublicUserID = int.Parse(myDetails.merchant_param5);
                        userpurchasemodel.ProductID = int.Parse(productids[i]);
                        _userpurchase.Add(userpurchasemodel);
                    }
                    else
                    {
                        // ViewData["Msg"] = "payment processed .system error occured. contact Support team to resolve this issue";
                    }
                }
            }
            return View();
        }
        #endregion
        #region payment Cancelled
        [HttpPost]
        public IActionResult payment_cancelled(string encResp)
        {
            /*Do not change the encResp.If you change it you will not get any response.
            Decode the encResp*/
            var decryption = new CCACrypto();
            var decryptedParameters = decryption.Decrypt(encResp,
            _param.Get("TestEncryptionkey").Value);
            ViewData["decryptedParameters"] = decryptedParameters;
            //  dynamic json = JsonConvert.DeserializeObject(decryptedParameters);
            var myDetails = JsonConvert.DeserializeObject<CCAvenueResponseModel>(decryptedParameters);
            PaymentDetailModel paymentdetailmodel = new PaymentDetailModel();
            ViewData["Msg"] = "Payment was cancelled";
            paymentdetailmodel.PaymentStatus = 1;
            paymentdetailmodel.ReferenceNO = myDetails.order_id;
            paymentdetailmodel.Amount = decimal.Parse(myDetails.amount);
            paymentdetailmodel.PublicUserID = UserHelpers.CurrentPublicSession.PublicUserID;
            _paymentdetail.Add(paymentdetailmodel);

            return View();
        }
        #endregion
        #region payment successfull
        [HttpPost]
        public IActionResult SubmitReview(ReviewModel reviewmodel)
        {
            reviewmodel.ReviewDate = DateTime.Now;
            reviewmodel.PublicUserID = UserHelpers.CurrentPublicSession.PublicUserID;
            bool reviewexist=_review.CheckUserReviewExist(reviewmodel.ProductID, UserHelpers.CurrentPublicSession.PublicUserID);
            if (!reviewexist)
            {
                _review.Add(reviewmodel);
            }
            else { 
            
            }
            return View("Index","Library");
        }
        #endregion
    }
}