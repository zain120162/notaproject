﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Areas.User.UserHelpers;
using System.Collections.Generic;
using System.Linq;
using Nota.Common;
using System.IO;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class MyUploadController : BaseController
    {
        #region Member Declaration
        private readonly IUserProduct _userproduct;
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public MyUploadController(IUserProduct userproduct, ApplicationDbContext context)
        {
            _userproduct = userproduct;
            _context = context;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            var viewModel = new UserProductModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Product" }
            };
            return View(viewModel);
        }
        #endregion

        #region Detail
        [HttpGet]
        public IActionResult Detail(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "MyUpload");
                }
                else
                {
                    var userproductID = int.Parse(id);
                    UserProductModel userproductModel = new UserProductModel();
                    BindAllDropDown(userproductModel);
                    userproductModel = _userproduct.GetProductDetailByProductId(userproductID);
                    if (userproductModel == null)
                    {
                        TempData["Msg"] = "drop";
                        return RedirectToAction("Index", "MyUpload");
                    }
                    else
                    {
                        BindAllDropDown(userproductModel);
                        return View(userproductModel);
                    }
                }
            }
            catch
            {
                TempData["Msg"] = "error";
                return RedirectToAction("Index", "MyUpload");
            }
        }
        #endregion

        #region Edit
        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "MyUpload");
                }
                else
                {
                    var productID = int.Parse(id);
                    UserProductModel productModel = _userproduct.GetProductDetailByProductId(productID);
                    if (productModel.ImageName != null)
                    {
                        productModel.ProductDetail = JsonConvert.SerializeObject(_userproduct.GetProductList(productID), Formatting.Indented);
                    }
                    if (productModel == null)
                    {
                        TempData["Msg"] = "drop";
                        return RedirectToAction("Index", "MyUpload");
                    }
                    BindAllDropDown(productModel);
                    return View(productModel);
                }
            }
            catch
            {
                TempData["Msg"] = "error";
                return RedirectToAction("Index", "MyUpload");
            }
        }


        [HttpPost]
        public IActionResult Edit(UserProductModel userProductModel, AttachmentModel attachmentModel)
        {
            var productDetail = _userproduct.GetProductDetailByProductId(userProductModel.ProductID);
            if (productDetail == null)
            {
                return RedirectToAction("Index", "MyUpload", new { Msg = "drop" });
            }

            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName))))
                {
                    ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                    BindAllDropDown(userProductModel);
                    return View(userProductModel);
                }

                if (attachments.Any(x => ((x.Length / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize))
                {
                    ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                    return View(userProductModel);
                }
            }
            _userproduct.EditProduct(userProductModel, attachmentModel);
            TempData["Msg"] = "updated";
            return RedirectToAction("Index", "MyUpload");
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<UserProductModel>> _AjaxBinding()
        {
            var productList = _userproduct.BindProductDetail(CurrentPublicSession.PublicUserID).ToList();
            if (productList == null)
            {
                TempData["Msg"] = "nullvalue";
            }
            return Json(productList);
        }
        #endregion

        #region Bind All Drop Down
        private UserProductModel BindAllDropDown(UserProductModel userproductModel)
        {
            userproductModel.SelectCategories = new SelectList(_userproduct.GetCategoriesList(0, string.Empty), "CategoriesID", "Name", "ProductID");
            return userproductModel;
        }
        #endregion

    }
}