﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Areas.User.UserHelpers;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class LibraryController : Controller
    {
        #region Member Declaration
        private readonly IUserProduct _userproduct;
        #endregion
        #region Constructor
        public LibraryController(IUserProduct userproduct)
        {
            _userproduct = userproduct;
        }
        #endregion
        public IActionResult Index()
        {
            return View();
        }
        #region Get All Products
        [HttpGet]
        public JsonResult GetAll() {
            List<CartModel> cartlist = HttpContext.Session.GetObject<List<CartModel>>("Cart");
            var result=_userproduct.GetAll();
            if (cartlist != null)
            {
                foreach (var cartitem in cartlist)
                {
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (cartitem.ProductID == result[i].ProductID)
                        {
                            result[i].addedstatus = true;
                        }
                    }

                }
            }

            return Json(new { data = result });
        }
        #endregion
        public IActionResult LibrarySelection(int id)
        {
            if (id == 1)
            {
                ViewData["Librarytoggle"] = "mp3";
            }
            else if (id == 2)
            {
                ViewData["Librarytoggle"] = "notes";
            }
            else {
                ViewData["Librarytoggle"] = "lyrics";
            }
            return View("Index");
        }
    }
}