﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using Nota.Areas.User.UserHelpers;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class MyPurchaseController : BaseController
    {
        #region Member Declaration
        private readonly IUserPurchase _userpurchase;
        private readonly ApplicationDbContext _context;
        [System.Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region Constractor
        [System.Obsolete]
        public MyPurchaseController(IUserPurchase userpurchase, ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _userpurchase = userpurchase;
            _context = context;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            var viewModel = new UserPurchaseModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Purchase" }
            };
            return View(viewModel);
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<UserPurchaseModel>> _AjaxBinding()
        {
            var purchaseList = _userpurchase.BindPurchaseDetail(CurrentPublicSession.PublicUserID).ToList();
            return Json(purchaseList);
        }
        #endregion

        #region Download
        public IActionResult Download(int publicUserID)
        {
            return View();
        }
        #endregion

        //#region Download Attechment
        //[Obsolete]
        //[HttpGet]
        //public ActionResult DownloadAttachment(long attachmentID)
        //{
        //    try
        //    {
        //        var attachmentFilePath = GetAttahchmentFilePath(_context, attachmentID);
        //        if (string.IsNullOrEmpty(attachmentFilePath))
        //        {
        //            return RedirectToAction("Index", "MyPurchase", new { Msg = "filenotfound" });
        //        }

        //        var fileName = Path.GetFileName(attachmentFilePath);
        //        FileInfo file = new FileInfo(attachmentFilePath);
        //        if (file.Exists)
        //        {
        //            byte[] fileBytes = System.IO.File.ReadAllBytes(attachmentFilePath);
        //            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Zip, fileName);
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "MyPurchase", new { Msg = "filenotfound" });
        //        }
        //    }
        //    catch
        //    {
        //        return RedirectToAction("Index", "MyPurchase", new { Msg = "error" });
        //    }
        //}

        //[Obsolete]
        //public string GetAttahchmentFilePath(ApplicationDbContext context, long attachmentID)
        //{
        //    var attachment = context.tblProducts.Where(x => x.PublicUserID == attachmentID).FirstOrDefault();
        //    var LeadKey = attachment.PublicUserID;
        //    if (attachment != null)
        //    {
        //        string folderName = "ApplicationDocuments/Images";
        //        string webRootPath = _hostingEnvironment.WebRootPath;
        //        string filePath = Path.Combine(webRootPath, folderName, attachment.CoverImage);

        //        return filePath;
        //    }
        //    return string.Empty;
        //}
        //#endregion
    }
}