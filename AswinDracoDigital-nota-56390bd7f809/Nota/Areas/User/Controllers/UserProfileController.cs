﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Common;
using Nota.Context;
using Nota.Controllers;
using Nota.Areas.User.UserHelpers;
using System;
using System.IO;
using System.Linq;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class UserProfileController : BaseController
    {
        #region Member Declaration
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserResetPassword _resetPassword;
        private readonly IUsers _Users;
        private readonly ApplicationDbContext _applicationDbContext;
        #endregion

        #region Constractor
        [Obsolete]
        public UserProfileController(IHostingEnvironment hostingEnvironment, IUserResetPassword resetPassword, IHttpContextAccessor httpContextAccessor, IUsers users, ApplicationDbContext applicationDbContext)
        {
            _Users = users;
            _resetPassword = resetPassword;
            _httpContextAccessor = httpContextAccessor;
            _applicationDbContext = applicationDbContext;
            _hostingEnvironment = hostingEnvironment;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Change Password
        public IActionResult ChangePassword()
        {
            UserChangePasswordModel objChangePasswordModel = new UserChangePasswordModel();
            objChangePasswordModel.PublicUserID = CurrentPublicSession.PublicUser.PublicUserID;
            objChangePasswordModel.Email = CurrentPublicSession.PublicUser.Email;
            objChangePasswordModel.ModuleName = new CommonMessagesModel { ModuleName = "Change password" };
            return PartialView(objChangePasswordModel);
        }

        [HttpPost]
        public IActionResult ChangePassword(UserChangePasswordModel objChangePasswordModel)
        {
            objChangePasswordModel.PublicUserID = CurrentPublicSession.PublicUser.PublicUserID;
            objChangePasswordModel.Email = CurrentPublicSession.PublicUser.Email;

            if (objChangePasswordModel.OldPassword == objChangePasswordModel.NewPassword)
            {
                ModelState.AddModelError("", string.Format(Messages.NewPasswordCanNotBeSame));
                TempData["Msg"] = "newpasswordcannotbesame";
                return RedirectToAction("EditDetail","UserProfile", new { id = CurrentPublicSession.PublicUserID });
            }

            if (objChangePasswordModel.NewPassword != objChangePasswordModel.ConfirmPassword)
            {
                ModelState.AddModelError("", string.Format(Messages.PasswordDoNotMatch));
                return View(objChangePasswordModel);
            }
            var userDetail = _Users.GetUserByID(objChangePasswordModel.PublicUserID);
            if (userDetail != null)
            {
                string currentPassword = GlobalCode.Encryption.Decrypt(userDetail.Password);

                if (currentPassword != objChangePasswordModel.OldPassword)
                {
                    ModelState.AddModelError("", string.Format(Messages.OldPasswordDoNotMatch));
                    TempData["Msg"] = "oldpassworddonotmatch";
                    return RedirectToAction("EditDetail", "UserProfile", new { id = CurrentPublicSession.PublicUserID });
                }
            }
            objChangePasswordModel.NewPassword = GlobalCode.Encryption.Encrypt(objChangePasswordModel.NewPassword);

            _resetPassword.EditUserPassword(objChangePasswordModel.NewPassword, objChangePasswordModel.PublicUserID);
            //HttpContext.Session.Clear();
            //HttpContext.Session.Remove("CurrentPublicUser");
            TempData["Msg"] = "changed";
            return RedirectToAction("Index", "UserDashboard");
        }
        #endregion

        #region Edit Detail
        [HttpGet]
        public IActionResult EditDetail(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("EditDetail", "UserProfile");
                }
                else
                {
                    var UserID = int.Parse(id);
                    UserChangePasswordModel changePasswordModel = _Users.GetUserDetailByUserID(UserID);
                    if (changePasswordModel == null)
                    {
                        TempData["Msg"] = "drop";
                        return RedirectToAction("EditDetail", "UserProfile");
                    }
                    return View(changePasswordModel);
                }
            }
            catch
            {
                TempData["Msg"] = "error";
                return RedirectToAction("EditDetail", "UserProfilep");
            }
        }

        [HttpPost]
        public IActionResult EditDetail(UserChangePasswordModel changePasswordModel, string publicUserID, AttachmentModel attachmentModel)
        {
            var publicUserDetail = _Users.GetUserDetailByUserID(changePasswordModel.PublicUserID);
            if (publicUserDetail == null)
            {
                return RedirectToAction("EditDetail", "UserProfile");
            }
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
                if (attachments.Any(x => !GlobalCode.AllowedDocumentTypes.Contains(Path.GetExtension(x.FileName))))
                {
                    ModelState.AddModelError("", string.Format(Messages.InvalidFile, string.Join(",", GlobalCode.AllowedDocumentTypes)));
                    return View(changePasswordModel);
                }

                if (attachments.Any(x => ((x.Length / 1024) / 1024) > GlobalCode.DocumentMaxUploadSize))
                {
                    ModelState.AddModelError("", string.Format(Messages.MaxFileSizeExceeded, GlobalCode.DocumentMaxUploadSize));
                    return View(changePasswordModel);
                }
            }
            TempData["Msg"] = "updated";
            _resetPassword.EditDetail(changePasswordModel, attachmentModel);
            return RedirectToAction("EditDetail", "UserProfile");
        }
        #endregion

        #region Download Attechment
        [Obsolete]
        public ActionResult DownloadAttachment(long attachmentID)
        {
            try
            {
                var attachmentFilePath = GetAttahchmentFilePath(_applicationDbContext, attachmentID);
                if (string.IsNullOrEmpty(attachmentFilePath))
                {
                    return RedirectToAction("EditDetail", "UserProfile", new { Msg = "filenotfound" });
                }

                var fileName = Path.GetFileName(attachmentFilePath);
                FileInfo file = new FileInfo(attachmentFilePath);
                if (file.Exists)
                {
                    byte[] fileBytes = System.IO.File.ReadAllBytes(attachmentFilePath);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
                }
                else
                {
                    return RedirectToAction("EditDetail", "UserProfile", new { Msg = "filenotfound" });
                }
            }
            catch
            {
                return RedirectToAction("EditDetail", "UserProfile", new { Msg = "error" });
            }
        }

        [Obsolete]
        public string GetAttahchmentFilePath(ApplicationDbContext context, long attachmentID)
        {
            var attachment = context.tblPublicUser.Where(x => x.PublicUserID == attachmentID).FirstOrDefault();
            var LeadKey = attachment.PublicUserID;
            if (attachment != null)
            {
                string folderName = "ApplicationDocuments/Images";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string filePath = Path.Combine(webRootPath, folderName, attachment.ProfilePicture);

                return filePath;
            }
            return string.Empty;
        }
        #endregion

        #region Logout
        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            HttpContext.Session.Remove("CurrentPublicUser");
            TempData["Msg"] = "logout";
            return RedirectToAction("Index", "Home", new { area = "" });
        }
        #endregion
    }
}