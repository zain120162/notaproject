﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.User.Interface;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class ReviewController : Controller
    {
        #region Member Declaration
        private readonly IReview _review;
        #endregion
        public ReviewController(IReview review) {
            _review = review;
         }
        public IActionResult Index()
        {
            return View();
        }
    }
}