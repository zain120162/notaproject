﻿using CCA.Util;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class MembershipController : Controller
    {
        #region Member Declaration
        private readonly IMembership _membership;
        private readonly ApplicationDbContext _context;
        private readonly IParameter _param;
        private readonly IPaymentDetail _paymentdetail;
        private readonly IUsers _user;
        #endregion

        #region Constructor
        public MembershipController(IMembership membership, ApplicationDbContext context, IParameter param, IPaymentDetail paymentdetail, IUsers user)
        {
            _membership = membership;
            _context = context;
            _param = param;
            _paymentdetail = paymentdetail;
            _user = user;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            return View();
        }
        #endregion

        #region Create
        public IActionResult Create()
        {
            MembershipModel membershipmodel = _membership.Get(UserHelpers.CurrentPublicSession.PublicUserID);
            if (membershipmodel != null)
            {
                PaymentDetailModel paymentdetailmodel = _paymentdetail.Get(membershipmodel.PaymentDetailID);
                UserMembershipModel usermembershipmodel = new UserMembershipModel();
                usermembershipmodel.StartDate = membershipmodel.StartDate;
                usermembershipmodel.ExpiryDate = membershipmodel.ExpiryDate;
                usermembershipmodel.DownloadAllowed = membershipmodel.DownloadAllowed;
                usermembershipmodel.UploadAllowed=membershipmodel.UploadAllowed;
                usermembershipmodel.ParticipantCreate = membershipmodel.ParticipantCreate;
                usermembershipmodel.ParticipantJoin = membershipmodel.ParticipantJoin;
                if (membershipmodel.MembershipName == 3)
                {
                    usermembershipmodel.Name = "Artist";
                }
                else if (membershipmodel.MembershipName == 2)
                {
                    usermembershipmodel.Name = "Silver";
                }
                else
                {
                    usermembershipmodel.Name = "Gold";
                }
                usermembershipmodel.MembershipName = membershipmodel.MembershipName;
                usermembershipmodel.Amount = paymentdetailmodel.Amount;

                return View(usermembershipmodel);
            }
            return View(null);
        }
        #endregion

        #region Payment
        [HttpGet]
        public IActionResult Payment(int id)
        {
            decimal membershipamount = 0;
          // int a= UserHelpers.CurrentPublicSession.PublicUserID;
            if (id == 1)
            {
                membershipamount = 100;
            }
            else if (id == 2)
            {
                membershipamount = 50;
            }
            else if (id == 3)
            {
                membershipamount = 200;
            }
            else {
                membershipamount = 0;
            }
            var lastpaymentrecord=_paymentdetail.GetTopRecord();
            string[] values;
            string orderid = "";
            string firstoperand = "";
            if (lastpaymentrecord != null)
            {
                values = lastpaymentrecord.ReferenceNO.Split("_");
                firstoperand = (int.Parse(values[0]) + 1).ToString();
                orderid = firstoperand + "_" + DateTime.Now.Date.Day.ToString() + DateTime.Now.Date.Month.ToString() + DateTime.Now.Date.Year.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
            }
            else {
                firstoperand = "1";
                orderid = firstoperand + "_"+ DateTime.Now.Date.Day.ToString()+ DateTime.Now.Date.Month.ToString()+ DateTime.Now.Date.Year.ToString() + DateTime.Now.Hour.ToString()+ DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
            }
            var guid = Guid.NewGuid();
            firstoperand= guid.ToString()+ DateTime.Now.Date.Hour+"/"+DateTime.Now.Date.Minute+"/"+ DateTime.Now.Date.Millisecond;
            var queryParameter = new CCACrypto();
          //  lastpaymentrecord.ReferenceNO++;
            return View("CcAvenue", new CcAvenueViewModel(queryParameter.Encrypt(BuildCcAvenueRequestParameters(orderid, orderid, id.ToString(), membershipamount.ToString(), _param.Get("TestMerchantid").Value), _param.Get("TestEncryptionkey").Value), _param.Get("TestAccesscode").Value, _param.Get("TestUrl").Value));
            //      return View();
        }
        #endregion
        #region ccavenue request parameter
        private string BuildCcAvenueRequestParameters(string orderId,string tid,string membershipid, string amount,string merchentid)
        {

            var queryParameters = new Dictionary<string, string>
            {
            {"order_id", orderId},
            {"merchant_id", merchentid},
            {"amount", amount},
            {"merchant_param5", UserHelpers.CurrentPublicSession.PublicUserID.ToString()},
            {"merchant_param4", membershipid},
            {"currency","AED" },
            {"tid",tid },
            {"redirect_url","https://nota20200819034315.azurewebsites.net/User/Membership/payment_successful" },
            {"cancel_url","https://nota20200819034315.azurewebsites.net/User/Membership/payment_cancelled"},
            {"request_type","JSON" },
            {"response_type","JSON" },
            {"version","1.1" }
            //add other key value pairs here.
            }.Select(item => string.Format("{0}={1}", item.Key, item.Value));
            return string.Join("&", queryParameters);

        }
        #endregion

        #region payment successfull
        [HttpPost]
        public IActionResult payment_successful(string encResp)
        {
            /*Do not change the encResp.If you change it you will not get any response.
            Decode the encResp*/
            var decryption = new CCACrypto();
            var decryptedParameters = decryption.Decrypt(encResp,
            _param.Get("TestEncryptionkey").Value);
            ViewData["decryptedParameters"] = decryptedParameters;
          //  dynamic json = JsonConvert.DeserializeObject(decryptedParameters);
            var myDetails = JsonConvert.DeserializeObject<CCAvenueResponseModel>(decryptedParameters);
            PaymentDetailModel paymentdetailmodel = new PaymentDetailModel();
            if(myDetails.order_status== "Success")
            {
                ViewData["Msg"] = "Thank you payment has been made";
                paymentdetailmodel.PaymentStatus = 1;
                paymentdetailmodel.ReferenceNO =myDetails.order_id;
                paymentdetailmodel.Amount =decimal.Parse( myDetails.amount);
                paymentdetailmodel.PublicUserID = int.Parse(myDetails.merchant_param5);
                paymentdetailmodel.Date = DateTime.Now;
                int result = _paymentdetail.Add(paymentdetailmodel);
                var membershipobj=_membership.Get(int.Parse(myDetails.merchant_param5));
                if (membershipobj == null)
                {
                    MembershipModel membershipmodel = new MembershipModel();
                    membershipmodel.StartDate = DateTime.Now;
                    membershipmodel.ExpiryDate = DateTime.Now;
                    membershipmodel.ExpiryDate = membershipmodel.ExpiryDate.AddYears(1);
                    membershipmodel.MembershipName =short.Parse(myDetails.merchant_param4);
                    if (myDetails.merchant_param4 == "1")
                    {
                        membershipmodel.ParticipantJoin = true;
                        membershipmodel.ParticipantCreate = false;
                        membershipmodel.DownloadAllowed = 100;
                        membershipmodel.UploadAllowed = 5;
                    }
                    else if (myDetails.merchant_param4 == "2")
                    {
                        membershipmodel.ParticipantJoin = true;
                        membershipmodel.ParticipantCreate = false;
                        membershipmodel.DownloadAllowed = 50;
                        membershipmodel.UploadAllowed = 5;
                    }
                    else {
                        membershipmodel.ParticipantJoin = true;
                        membershipmodel.ParticipantCreate = true;
                        membershipmodel.DownloadAllowed = int.MaxValue;
                        membershipmodel.UploadAllowed = int.MaxValue;

                    }
                    membershipmodel.PublicUserID = int.Parse(myDetails.merchant_param5);
                    membershipmodel.PaymentDetailID = result;    
                    int membershipid=_membership.Add(membershipmodel);
                    var publicuserobj=_user.GetPublicUserDetailByUserID(int.Parse(myDetails.merchant_param5));
                    publicuserobj.MembershipID = membershipid;
                    _user.EditUser(publicuserobj);
                }
                else {
                    MembershipModel membershipmodel = new MembershipModel();
                    membershipmodel.StartDate = DateTime.Now;
                    membershipmodel.ExpiryDate = DateTime.Now;
                    membershipmodel.ExpiryDate=membershipmodel.ExpiryDate.AddYears(1);
                    membershipmodel.MembershipName = short.Parse(myDetails.merchant_param4);
                    if (myDetails.merchant_param4 == "1")
                    {
                        membershipmodel.ParticipantJoin = true;
                        membershipmodel.ParticipantCreate = false;
                        membershipmodel.DownloadAllowed = 100;
                        membershipmodel.UploadAllowed = 5;
                    }
                    else if (myDetails.merchant_param4 == "2")
                    {
                        membershipmodel.ParticipantJoin = true;
                        membershipmodel.ParticipantCreate = false;
                        membershipmodel.DownloadAllowed = 50;
                        membershipmodel.UploadAllowed = 5;
                    }
                    else
                    {
                        membershipmodel.ParticipantJoin = true;
                        membershipmodel.ParticipantCreate = true;
                        membershipmodel.DownloadAllowed = int.MaxValue;
                        membershipmodel.UploadAllowed = int.MaxValue;

                    }
                    membershipmodel.PublicUserID = int.Parse(myDetails.merchant_param5);
                    membershipmodel.PaymentDetailID = result;
                    int membershipid = _membership.Edit(membershipmodel);
                    var publicuserobj = _user.GetPublicUserDetailByUserID(int.Parse(myDetails.merchant_param5));
                    publicuserobj.MembershipID = membershipid;
                    _user.EditUser(publicuserobj);
                }
            }
            return View();
        }
        #endregion
        #region payment Cancelled
        [HttpPost]
        public IActionResult payment_cancelled(string encResp)
        {
            /*Do not change the encResp.If you change it you will not get any response.
            Decode the encResp*/
            var decryption = new CCACrypto();
            var decryptedParameters = decryption.Decrypt(encResp,
            _param.Get("TestEncryptionkey").Value);
            ViewData["decryptedParameters"] = decryptedParameters;
            //  dynamic json = JsonConvert.DeserializeObject(decryptedParameters);
            var myDetails = JsonConvert.DeserializeObject<CCAvenueResponseModel>(decryptedParameters);
            PaymentDetailModel paymentdetailmodel = new PaymentDetailModel();
                ViewData["Msg"] = "Payment was cancelled";
                paymentdetailmodel.PaymentStatus = 1;
                paymentdetailmodel.ReferenceNO =myDetails.order_id;
                paymentdetailmodel.Amount = decimal.Parse(myDetails.amount);
                paymentdetailmodel.PublicUserID = UserHelpers.CurrentPublicSession.PublicUserID;
                _paymentdetail.Add(paymentdetailmodel);

            return View();
        }
        #endregion
    }
}