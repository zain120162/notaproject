﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nota.Areas.User.Interface;
using Nota.Areas.User.UserHelpers;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    [ValidatePublicUserLogin]
   
    public class UserDashboardController : BaseController
    {
        public ICart _cart;
        public UserDashboardController(ICart cart) {
            _cart = cart;
        }
        public IActionResult Index()
        {
            var result=_cart.GetAll(UserHelpers.CurrentPublicSession.PublicUserID);
            HttpContext.Session.SetObject("Cart", result);
            HttpContext.Session.SetString("cartcount", result.Count.ToString());
            ViewData["Title"] = "The Nota Music Player";
            return View();
        }
    }
}