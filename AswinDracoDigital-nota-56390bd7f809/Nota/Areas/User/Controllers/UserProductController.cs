﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Areas.Admin.Models;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Models;
using Nota.Common;
using Nota.Context;
using System;
using System.IO;
using System.Linq;

namespace Nota.Areas.User.Controllers
{
    [Area("User")]
    public class UserProductController : BaseController
    {
        #region Member Declaration
        private readonly IUserProduct _product;
        private readonly ApplicationDbContext _context;
        [Obsolete]
        private readonly IHostingEnvironment _hostingEnvironment;
        #endregion

        #region Constractor
        [Obsolete]
        public UserProductController(IUserProduct product, ApplicationDbContext context, IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _product = product;
            _context = context;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            var viewModel = new UserProductModel
            {
                ModuleName = new CommonMessagesModel { ModuleName = "Product" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [HttpGet]
        public IActionResult Create()
        {
            UserProductModel productModel = new UserProductModel();
            BindAllDropDown(productModel);
            return View(productModel);
        }

        [HttpPost]
        public IActionResult Create(UserProductModel productModel, AttachmentModel attachmentModel)
        {
            if (_product.IsProductExists(productModel.UserProductID, productModel.Title.Trim()))
            {
                TempData["Msg"] = "alreadyexists";
                BindAllDropDown(productModel);
                return View(productModel);
            }
            //IFormFile file;
            //if (productModel.CategoriesID == 1)
            //{
            //    file =
            //}
            if (attachmentModel.Attachments != null && attachmentModel.Attachments.Count() > 0)
            {
                var attachments = attachmentModel.Attachments.Where(x => x != null).ToList();
            }
            _product.AddProduct(productModel, attachmentModel);
            TempData["Msg"] = "added";
            return RedirectToAction("Index", "UserDashboard");
        }
        #endregion

        #region Bind All Drop Down
        private UserProductModel BindAllDropDown(UserProductModel productModel)
        {
            productModel.SelectCategories = new SelectList(_product.GetCategoriesList(0, string.Empty), "CategoriesID", "Name");
            return productModel;
        }
        #endregion
    }
}