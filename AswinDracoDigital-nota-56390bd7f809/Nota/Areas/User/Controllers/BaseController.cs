﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Nota.Context;
using Nota.Areas.User.UserHelpers;
using Nota.Interface;
using Nota.Models;
using Nota.Repository;

namespace Nota.Areas.User.Controllers
{
    public class BaseController : Controller
    {
        protected IConfiguration Config { get; }
        private readonly ICacheManager _cacheManager = null;
        private readonly ApplicationDbContext _context;
        
        public BaseController()
        {
            _cacheManager = new MemoryCacheManager();
        }
        public BaseController(ICacheManager cacheManager,ApplicationDbContext context)
        {
            _cacheManager = cacheManager;
            _context = context;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerName = ((ControllerActionDescriptor)filterContext.ActionDescriptor).ControllerName;

            ValidateLogin(filterContext);
            base.OnActionExecuting(filterContext);
        }

        private void ValidateLogin(ActionExecutingContext filterContext)
        {
            string controllerName = ((ControllerActionDescriptor)filterContext.ActionDescriptor).ControllerName;
            string methodName = ((ControllerActionDescriptor)filterContext.ActionDescriptor).ActionName;

            if (CurrentPublicSession.PublicUserID < 0)
            {
                if (!(string.Compare(controllerName.ToLower(),"home", true) == 0 && string.Compare(methodName.ToLower(), "index", true) == 0))
                {
                    filterContext.HttpContext.Session.Clear();
                    TempData["Msg"] = "unauthorize";
                    filterContext.Result = new RedirectResult("~/Home/Index");
                    return;
                }
            }           
        }
    }
}