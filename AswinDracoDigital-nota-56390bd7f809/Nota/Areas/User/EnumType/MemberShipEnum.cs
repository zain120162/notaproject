﻿using System;

namespace Nota.Areas.User.EnumType
{
    public enum MemberShipEnum : Int16
    {
        Gold = 1,
        Silver = 2,
        Artist = 3
    }
}