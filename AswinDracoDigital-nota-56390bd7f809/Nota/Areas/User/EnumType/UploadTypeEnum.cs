﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.EnumType
{
    public enum UploadTypeEnum : Int64
    {
        Notes = 1,
        Music = 2,
        Lyrics = 3,
    }
}
