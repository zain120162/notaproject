﻿using Nota.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Interface
{
    public interface IReview
    {
         List<ReviewModel> Get(int productid);
         bool Add(ReviewModel reviewmodel);
        bool CheckUserReviewExist(int productid, int publicuserid);
    }
}
