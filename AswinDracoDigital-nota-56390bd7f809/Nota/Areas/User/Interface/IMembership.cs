﻿using Nota.Areas.User.Models;

namespace Nota.Areas.User.Interface
{
    public interface IMembership
    {
        MembershipModel Get(int publicuserid);
        int Add(MembershipModel membershipmodel);
        int Edit(MembershipModel membershipmodel);
    }
}