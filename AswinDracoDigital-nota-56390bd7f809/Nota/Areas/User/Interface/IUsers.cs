﻿using Nota.Areas.Admin.Models;
using Nota.Areas.User.Models;

namespace Nota.Areas.User.Interface
{
    public interface IUsers
    {
         PublicUserModel GetUserByID(int userID);
         UserChangePasswordModel GetUserDetailByUserID(int userID);
         bool IsUserExistsByEmail(string fieldList, string valueList, int strAddOrEditID);
        bool EditUser(PublicUserModel objPublicUserModel);
        PublicUserModel GetPublicUserDetailByUserID(int userID);
    }
}
