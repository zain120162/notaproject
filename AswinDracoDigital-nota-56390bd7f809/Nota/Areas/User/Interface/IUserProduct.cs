﻿using Nota.Areas.Admin.Models;
using Nota.Areas.User.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Interface
{
    public interface IUserProduct
    {
        Task<bool> EditProduct(UserProductModel productModel, AttachmentModel attachmentModel);
        void DeleteProduct(string[] ids);
        Task<bool> AddProduct(UserProductModel productModel, AttachmentModel attachmentModel);
        IQueryable<UserProductModel> BindProductDetail(int publicUserID);
        bool IsProductExists(int productId, string Title);
         List<UserProductModel> GetCategoriesList(int CategoriesId, string name);
         UserProductModel GetProductDetailByProductId(int productID);
         List<UploadedFiles> GetProductList(int productID);
         List<UserProductModel> GetAll();
    }
}