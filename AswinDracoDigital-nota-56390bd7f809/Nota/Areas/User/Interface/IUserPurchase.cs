﻿using Nota.Areas.User.Models;
using System.Linq;

namespace Nota.Areas.User.Interface
{
    public interface IUserPurchase
    {
         IQueryable<UserPurchaseModel> BindPurchaseDetail(int publicUserID);
        bool Add(UserPurchaseModel userpurchasemodel);
    }
}