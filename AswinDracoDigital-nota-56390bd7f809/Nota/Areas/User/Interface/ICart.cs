﻿using Nota.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Interface
{
    public interface ICart
    {
        bool Add(CartModel cartmodel);
        CartModel Get(int CartID);
        List<CartModel> GetAll(int publicuserid);
        void Delete(int productid, int publicuserid);
    }
}
