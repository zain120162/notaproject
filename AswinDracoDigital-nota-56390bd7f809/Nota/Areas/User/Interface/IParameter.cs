﻿using Nota.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Interface
{
    public interface IParameter
    {
        ParameterModel Get(string paramame);
    }
}
