﻿using Nota.Areas.Admin.Models;
using Nota.Areas.User.Models;

namespace Nota.Areas.User.Interface
{
    public interface IUserResetPassword
    {
        void EditUserPassword(string password, int userID);
         void EditDetail(UserChangePasswordModel userChangePasswordModel, AttachmentModel attachmentModel);
    }
}