﻿using Nota.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Areas.User.Interface
{
    public interface IPaymentDetail
    {
        List<PaymentDetailModel> GetAll();
        PaymentDetailModel Get(int PaymentDetailID);
        int Add(PaymentDetailModel paymentdetailmodel);
        PaymentDetailModel GetTopRecord();
    }
}
