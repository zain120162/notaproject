﻿using Nota.Context;
using Nota.Interface;
using Nota.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Repository
{
    public class ProductDetailRepository : IProductDetail
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        #endregion

        #region Constructor
        public ProductDetailRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Get Music List
        public IQueryable<ProductViewModel> GetMusicList()
        {
            var  MusicList = from c in _context.tblProducts
                             where c.UploadType == 2
                             select new ProductViewModel
                                        {
                                            ProductID = c.ProductID,
                                            CoverImage = c.CoverImage,
                                            Title = c.Title,
                                            Description = c.Description,
                                            Amount = c.Amount,
                                            DiscountPercentage = c.DiscountPercentage,
                                            CategoriesID = c.CategoriesID,
                                            IsApproved = c.IsApproved,
                                            PriceType = c.PriceType,
                                        };
            return MusicList;
        }
        #endregion

        #region Get Notes List
        public IQueryable<ProductViewModel> GetNotesList()
        {
            var NotesList = from c in _context.tblProducts
                            where c.UploadType == 1
                            select new ProductViewModel
                            {
                                ProductID = c.ProductID,
                                CoverImage = c.CoverImage,
                                Title = c.Title,
                                Description = c.Description,
                                Amount = c.Amount,
                                DiscountPercentage = c.DiscountPercentage,
                                CategoriesID = c.CategoriesID,
                                IsApproved = c.IsApproved,
                                PriceType = c.PriceType,
                            };
            return NotesList;
        }
        #endregion

        #region Bind Product Detail
        public List<ProductViewModel> GetNoteList()
        {
            List<ProductViewModel> NoteList = (from c in _context.tblProducts
                                                where c.UploadType == 2
                                                select new ProductViewModel
                                                {
                                                    ProductID = c.ProductID,
                                                    CoverImage = c.CoverImage,
                                                    Title = c.Title,
                                                    Description = c.Description,
                                                    Amount = c.Amount,
                                                    DiscountPercentage = c.DiscountPercentage,
                                                    CategoriesID = c.CategoriesID,
                                                    IsApproved = c.IsApproved,
                                                    PriceType = c.PriceType,
                                                }).ToList();
            return NoteList;
        }
        #endregion
    }
}
