﻿using Nota.Areas.Admin.Interface;
using Nota.Context;
using Nota.Interface;
using Nota.Models;
using System.Linq;

namespace Nota.Repository
{
    public class UserCommonSettingRepository : IUserCommonSetting
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public UserCommonSettingRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        #endregion

        #region Common Setting
        public tblCommonSetting GetCommonSetting()
        {
            return _context.tblCommonSetting.FirstOrDefault();
        }
        #endregion
    }
}