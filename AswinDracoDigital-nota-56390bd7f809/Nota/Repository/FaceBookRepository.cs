﻿using Newtonsoft.Json;
using Nota.Common;
using Nota.Context;
using Nota.Interface;
using Nota.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static Nota.ViewModel.GoogleViewModel;
using static Nota.ViewModel.FaceBookViewModel;

namespace Nota.Repository
{
    public class FaceBookRepository : IFaceBook
    {
        #region Member Declaration
        private readonly IPublicUser _publicUser;
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constructor
        public FaceBookRepository(IPublicUser publicUser,ApplicationDbContext context)
        {
            _context = context;
            _publicUser = publicUser;
        }
        #endregion

        #region Get FaceBook User
        public async Task<FacebookUser> GetFacebookUser(string code)
        {
            // Create variables
            FacebookAuthorization facebook_authorization = null;
            FacebookUser facebook_user = new FacebookUser();
            // Get a http client
            HttpClient client = new HttpClient();

            // Create the url
            string url = "https://graph.facebook.com/oauth/access_token?client_id=" + "1605867736260802" +
            "&redirect_uri=" + "https://localhost:44322" + "/UserLogin/callback" + "&client_secret="
            + "6a551835d1d0015e8bf5d1c23ab48a88" + "&code=" + code;

            // Get the response
            HttpResponseMessage response = await client.GetAsync(url);

            // Make sure that the response is successful
            if (response.IsSuccessStatusCode)
            {
                // Get facebook authorization
                facebook_authorization = System.Text.Json.JsonSerializer.Deserialize<FacebookAuthorization>(await
                response.Content.ReadAsStringAsync());
            }
            else
            {
                // Get an error
                FacebookErrorRoot root = System.Text.Json.JsonSerializer.Deserialize<FacebookErrorRoot>(await
                response.Content.ReadAsStringAsync());
            }

            // Make sure that facebook authorization not is null
            if (facebook_authorization == null)
            {
                return null;
            }
            // Modify the url
            url = "https://graph.facebook.com/me?fields=id,name&access_token=" + facebook_authorization.access_token;
            // Get the response
            response = await client.GetAsync(url);
            // Make sure that the response is successful
            if (response.IsSuccessStatusCode)
            {
                // Get a facebook user
                facebook_user = System.Text.Json.JsonSerializer.Deserialize<FacebookUser>(await response.Content.ReadAsStringAsync());
            }
            else
            {
                // Get an error
                FacebookErrorRoot root = System.Text.Json.JsonSerializer.Deserialize<FacebookErrorRoot>(await response.Content.ReadAsStringAsync());
            }
            // Return the facebook user
            return facebook_user;
        }
        #endregion

        #region Get Google User
        public async Task<FacebookUser> GetGoogleUser(string code)
        {
            FacebookUser facebookUser = new FacebookUser();
            GoogleUser googleUser = new GoogleUser();
            GoogleUserOutputData googleUserOutputData = new GoogleUserOutputData();
            string client_id = "505669636385-0ad3rmofgmhielkbknqim5k1s8d29a4d.apps.googleusercontent.com";
            string client_secret = "An1ZPAbmCt8iahIld57-5yWA";
            string redirect_uri = "https://localhost:44322/UserLogin/callbackg";
            //string grant_type = "authorization_code";
            // Create variables
            GoogleAuthorization googleAuthorization = null;
            // Get a http clientx

            var apiClient = new HttpClient();
            var values = new Dictionary<object, object>
            {
                {"client_id", "505669636385-0ad3rmofgmhielkbknqim5k1s8d29a4d.apps.googleusercontent.com" },
                {"client_secret", "An1ZPAbmCt8iahIld57-5yWA" },
                {"grant_type", "authorization_code" },
                {"code", code },
                {"redirect_uri", "https://localhost:44322/UserLogin/callbackg" }
            };

            var content = new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/x-www-form-urlencoded");

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            webRequest.Method = "POST";
            string Parameters = "code=" + code + "&client_id=" + client_id + "&client_secret=" + client_secret + "&redirect_uri=" + redirect_uri + "&grant_type=authorization_code";
            byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ContentLength = byteArray.Length;
            Stream postStream = webRequest.GetRequestStream();
            // Add the post data to the web request
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            WebResponse response = webRequest.GetResponse();
            postStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(postStream);
            string responseFromServer = reader.ReadToEnd();

            GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);
            //// Create the url
            string url = "https://accounts.google.com/o/oauth2/token";

            //// Get the response
            HttpResponseMessage response2 = await apiClient.PostAsync(url, content);

            // Make sure that the response is successful
            if (response2.IsSuccessStatusCode)
            {
                // Get facebook authorization
                googleAuthorization = System.Text.Json.JsonSerializer.Deserialize<GoogleAuthorization>(await
                response2.Content.ReadAsStringAsync());
            }
            else
            {
                // Get an error
            }
            HttpClient client = new HttpClient();
            //// Modify the url
            client.CancelPendingRequests();

            string newurl = "https://www.googleapis.com/oauth2/v1/userinfo?access_token=" + serStatus.access_token;
            //// Get the response
            var response1 = await client.GetAsync(newurl);
            // Make sure that the response is successful
            if (response1.IsSuccessStatusCode)
            {
                // Get a facebook user
                facebookUser = System.Text.Json.JsonSerializer.Deserialize<FacebookUser>(await response1.Content.ReadAsStringAsync());
                //if (_publicUser.IsPublicUserEmailExists(googlePlusAccessToken.email) == false)
                //{
                //    var data = FaceBookRepository.AddUser(googlePlusAccessToken);
                //}
            }
            return  facebookUser;
        } // End of the GetFacebookUser method
        #endregion

        #region Add User
        public void AddUser(FacebookUser facebookUser)
        {
            tblPublicUser publicUser = new tblPublicUser();
            publicUser.FirstName = facebookUser.name;
            publicUser.LastName = facebookUser.given_name;
            publicUser.Email = facebookUser.email;
            publicUser.Password = GlobalCode.Encryption.Encrypt(FaceBookRepository.CreatePassword(8));
            publicUser.IsSocialLogin = true;
            publicUser.IsEmailVerified = true;
            _context.tblPublicUser.Add(publicUser);
            _context.SaveChanges();
        }
        #endregion

        #region Random Password Generator
        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }
        #endregion
    }

    public class GoogleUserOutputData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string picture { get; set; }
    }
    public class GooglePlusAccessToken
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string id_token { get; set; }
        public string refresh_token { get; set; }
    }
}