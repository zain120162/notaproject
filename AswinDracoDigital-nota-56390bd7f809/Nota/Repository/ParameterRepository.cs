﻿using Nota.Context;
using Nota.Interface;
using Nota.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Repository
{
    public class ParameterRepository : IParamter
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        #endregion

        #region Constractor
        public ParameterRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public ParameterViewModel Get(string paraname)
        {
            var result = _context.tblParameter.Where(x => x.Name == paraname).Select(x => new ParameterViewModel { Name = x.Name, ParameterID = x.ParameterID, isActive = x.isActive, Value = x.Value }).SingleOrDefault();
            return result;
        }
        #endregion

    }
}
