﻿using Nota.Common;
using Nota.Context;
using Nota.Helpers;
using Nota.Interface;
using Nota.Models;
using Nota.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Repository
{
    public class PublicUserRepository : IPublicUser
    {
        #region Member Declaration
        private static ApplicationDbContext _context;
        private static IUserCommonSetting _commonSetting;
        #endregion

        #region Constractor
        public PublicUserRepository(ApplicationDbContext context, IUserCommonSetting commonSetting)
        {
            _context = context;
            _commonSetting = commonSetting;
        }
        #endregion

        #region Is User Exists
        public bool IsUserExists(int publicUserID, string emailID)
        {
            bool result = (from r in _context.tblPublicUser
                           where (publicUserID == 0 || r.PublicUserID != publicUserID)
                           && (string.Equals(r.Email.Trim(), emailID, StringComparison.OrdinalIgnoreCase))
                           select r.PublicUserID).Any();
            return result;
        }
        #endregion

        #region Get Role List
        public List<PublicUserViewModel> GetRoleList(int roleID, string roleName)
        {
            List<PublicUserViewModel> RoleList = (from c in _context.tblRole
                                                  where (string.IsNullOrEmpty(roleName) || c.Role == roleName)
                                                  && (roleID == 0 || c.RoleID != roleID)
                                                  && c.IsActive == true
                                                  orderby c.Role
                                                  select new PublicUserViewModel
                                                  {
                                                      RoleID = c.RoleID,
                                                      Role = c.Role,
                                                  }).ToList();
            return RoleList;
        }
        #endregion

        #region Bind User Detail
        public IQueryable<PublicUserViewModel> BindUserDetail()
        {
            var UserDetail = from u in _context.tblPublicUser
                                 //where u.Email != GlobalCode.AdminEmailAddress
                             orderby u.PublicUserID
                             select new PublicUserViewModel
                             {
                                 PublicUserID = u.PublicUserID,
                                 FirstName = u.FirstName,
                                 LastName = u.LastName,
                                 Email = u.Email,
                                 Password = u.Password,
                                 Description = u.Description,
                                 DateJoined = u.DateJoined,
                                 IsSocialLogin = u.IsSocialLogin,
                                 ProfilePicture = u.ProfilePicture,
                                 IsCurrentAdminUser = CurrentAdminSession.UserID == u.PublicUserID ? true : false,
                                 Active = u.Status == true ? GlobalCode.activeText : GlobalCode.inActiveText,
                             };
            return UserDetail;
        }
        #endregion

        #region Add User
        public void AddUser(PublicUserViewModel publicUserViewModel)
        {
            tblPublicUser publicUser = new tblPublicUser();
            publicUser.FirstName = publicUserViewModel.FirstName.Trim();
            publicUser.LastName = publicUserViewModel.LastName.Trim();
            publicUser.Email = publicUserViewModel.Email;
            publicUser.Phone = publicUserViewModel.Phone;
            publicUser.MembershipID = publicUserViewModel.MembershipID;
            publicUser.Description = publicUserViewModel.Description;
            publicUser.DateJoined = DateTime.Now;
            publicUser.Password = GlobalCode.Encryption.Encrypt(publicUserViewModel.Password);
            publicUser.Status = publicUserViewModel.Status;
            _context.tblPublicUser.Add(publicUser);
            publicUser.IsEmailVerified = publicUserViewModel.IsEmailVerified;
            _context.SaveChanges();
        }
        #endregion

        #region Verified User
        public void VerifiedUser(string EmailID)
        {
            tblPublicUser objTblUser = _context.tblPublicUser.Where(u => u.Email == EmailID).FirstOrDefault();
            if (objTblUser != null)
            {
                objTblUser.IsEmailVerified = true;
                _context.SaveChanges();
            }
        }
        #endregion

        #region Edit User
        public void EditUser(PublicUserViewModel publicUserViewModel)
        {
            tblPublicUser objTblUser = _context.tblPublicUser.Where(u => u.PublicUserID == publicUserViewModel.PublicUserID).FirstOrDefault();
            if (objTblUser != null)
            {
                objTblUser.FirstName = publicUserViewModel.FirstName.Trim();
                objTblUser.LastName = publicUserViewModel.LastName.Trim();
                objTblUser.Phone = publicUserViewModel.Phone;
                objTblUser.Email = publicUserViewModel.Email;
                objTblUser.Password = GlobalCode.Encryption.Decrypt(publicUserViewModel.Password);
                objTblUser.Status = publicUserViewModel.Status;
                _context.SaveChanges();
            }
        }
        #endregion

        #region Get User Detail By User ID
        public PublicUserViewModel GetUserDetailByUserID(int PublicUserID)
        {
            PublicUserViewModel publicUserViewModel = (from u in _context.tblPublicUser
                                              where u.PublicUserID == PublicUserID
                                              select new PublicUserViewModel
                                              {
                                                  PublicUserID = u.PublicUserID,
                                                  Email = u.Email,
                                                  Password = GlobalCode.Encryption.Decrypt(u.Password),
                                                  FirstName = u.FirstName,
                                                  LastName = u.LastName,
                                                  Phone = u.Phone,
                                                  Description = u.Description,
                                                  ProfilePicture = u.ProfilePicture,
                                                  MembershipID = u.MembershipID,
                                                  IsSocialLogin = u.IsSocialLogin,
                                                  //RoleID = r.RoleID,
                                                  //Role = r.Role,
                                                  //AdminUser = u.IsAdminUser ? "Yes" : "No",
                                                  Status = u.Status,
                                                  Active = u.Status ? GlobalCode.activeText : GlobalCode.inActiveText,
                                              }).FirstOrDefault();
            return publicUserViewModel;
        }
        #endregion

        #region Get User Count
        public static int GetUserCount(string userName, string email, int userID)
        {
            return (from p in _context.tblPublicUser
                    where (string.IsNullOrEmpty(email) || p.Email == email) && (userID == 0 || p.PublicUserID != userID)
                    select p.PublicUserID).Count();
        }
        #endregion

        #region Is User Exists By Email
        public bool IsUserExistsByEmail(string fieldList, string valueList, int strAddOrEditID)
        {
            bool IsExists = false;
            string strUserName = string.Empty;
            if (!string.IsNullOrEmpty(fieldList))
            {
                if (strAddOrEditID == -1 && strAddOrEditID != 0)
                {
                    strUserName = PublicUserRepository.GetUserCount(string.Empty, valueList.Trim(), 0) == 0 ? null : "1";
                }
                else if (strAddOrEditID != 0)   
                {
                    strUserName = PublicUserRepository.GetUserCount(string.Empty, valueList.Trim(), strAddOrEditID) == 0 ? null : "1";
                }

                if (!string.IsNullOrEmpty(strUserName))
                {
                    IsExists = true;
                }
            }
            return IsExists;
        }
        #endregion

        #region Is User Email Exists
        public bool IsUserEmailExists(int UserID, string Email)
        {
            bool result = (from p in _context.tblPublicUser
                           where (UserID == 0 || p.PublicUserID != UserID) && (string.Equals(p.Email.Trim(), Email, StringComparison.OrdinalIgnoreCase))
                           select p.PublicUserID).Any();
            return result;
        }
        #endregion

        #region Is PublicUser Email Exists
        public bool IsPublicUserEmailExists(string Email)
        {
            bool result = (from p in _context.tblPublicUser
                           where (string.Equals(p.Email.Trim(), Email, StringComparison.OrdinalIgnoreCase))
                           select p.PublicUserID).Any();
            return result;
        }
        #endregion

        #region Is PublicUser Exists
        public bool IsPublicUserExists(string name)
        {
            bool result = (from p in _context.tblPublicUser
                           where (string.Equals(p.FirstName.Trim(), name, StringComparison.OrdinalIgnoreCase))
                           select p.PublicUserID).Any();
            return result;
        }
        #endregion

        #region Delete User
        public void DeleteUser(string[] ids)
        {
            var IdsToDelete = ids.Select(int.Parse).ToList();
            _context.tblPublicUser.RemoveRange(_context.tblPublicUser.Where(r => IdsToDelete.Contains(r.PublicUserID)).AsEnumerable());
            _context.SaveChanges();
        }
        #endregion

        #region Get Active User By Email
        public PublicUserViewModel GetActiveUserByEmail(string email)
        {
            var userDetail = (from u in _context.tblPublicUser
                              where u.Email == email 
                              select new PublicUserViewModel
                              {
                                  IsEmailVerified = u.IsEmailVerified,
                                  IsSocialLogin = u.IsSocialLogin,
                                  PublicUserID = u.PublicUserID,
                                  Password = u.Password,
                                  FirstName = u.FirstName,
                                  LastName = u.LastName,
                                  Phone = u.Phone,
                                  Email = u.Email,
                                  IsActive = u.Status,
                                  Active = u.Status ? GlobalCode.activeText : GlobalCode.inActiveText,
                              }).FirstOrDefault();
            return userDetail;
        }
        #endregion

        #region Get User By ID
        public PublicUserViewModel GetUserByID(int userID)
        {
            PublicUserViewModel objUserViewModel = (from u in _context.tblPublicUser
                                              where u.PublicUserID == userID 
                                              select new PublicUserViewModel
                                              {
                                                  PublicUserID = u.PublicUserID,
                                                  Email = u.Email,
                                                  Password = u.Password,
                                                  FirstName = u.FirstName,
                                                  LastName = u.LastName,
                                                  Phone = u.Phone,
                                                  Active = u.Status ? GlobalCode.activeText : GlobalCode.inActiveText,
                                              }).FirstOrDefault();
            return objUserViewModel;
        }
        #endregion

        #region Common Setting
        public tblCommonSetting GetCommonSetting()
        {
            return _context.tblCommonSetting.FirstOrDefault();
        }
        #endregion

        #region Get Email Template Content
        public TemplateViewModel GetEmailTemplateContent(string templateName)
        {
            TemplateViewModel obj = new TemplateViewModel();
            var Templates = (from p in _context.tblTemplate
                             where p.TemplateName.ToUpper() == templateName.ToUpper()
                             select p).FirstOrDefault();

            obj.TemplateID = Templates.TemplateID;
            obj.TemplateName = Templates.TemplateName;
            obj.Subject = Templates.Subject;
            obj.Body = Templates.Body;
            obj.TemplateContentForEmail = Templates.Body;

            return obj;
        }
        #endregion

        #region Get Public User Detail By Name
        public PublicUserViewModel GetPublicUserDetailByName(string Name)
        {
            PublicUserViewModel publicUserViewModel = (from u in _context.tblPublicUser
                                                       where u.FirstName == Name
                                                       select new PublicUserViewModel
                                                       {
                                                           PublicUserID = u.PublicUserID,
                                                           Email = u.Email,
                                                           Password = GlobalCode.Encryption.Decrypt(u.Password),
                                                           FirstName = u.FirstName,
                                                           LastName = u.LastName,
                                                           Phone = u.Phone,
                                                           Description = u.Description,
                                                           ProfilePicture = u.ProfilePicture,
                                                           MembershipID = u.MembershipID,
                                                           IsSocialLogin = u.IsSocialLogin,
                                                           IsEmailVerified = u.IsEmailVerified,
                                                           Status = u.Status,
                                                           Active = u.Status ? GlobalCode.activeText : GlobalCode.inActiveText,
                                                       }).FirstOrDefault();
            return publicUserViewModel;
        }
        #endregion
    }
}