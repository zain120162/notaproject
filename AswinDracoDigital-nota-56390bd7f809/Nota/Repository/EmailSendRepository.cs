﻿using Nota.Areas.Admin.Interface;
using Nota.Context;
using Nota.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Nota.Repository
{
    public class EmailSendRepository : IEmailSend
    {
        #region Member Declaration        
        private readonly ApplicationDbContext _context;
        private readonly IUserCommonSetting _commonSettings;
        #endregion

        #region Constractor
        public EmailSendRepository(ApplicationDbContext context, IUserCommonSetting commonSettings)
        {
            _context = context;
            _commonSettings = commonSettings;
        }
        #endregion

        #region Send Email Module
        public bool SendModuleEmail(string recipients, string subject, string emailBody, string CC = "")
        {
            SmtpClient _SmtpClient = new SmtpClient();
            MailMessage _msgCsutomer = new MailMessage();
            bool isSend = false;
            var Commonsetting = _commonSettings.GetCommonSetting();
            string strFrom = Convert.ToString(Commonsetting.Email);
            _msgCsutomer.From = new MailAddress(strFrom);
            var arrrecipients = recipients.Split(',').ToList();
            foreach (var recipient in arrrecipients)
            {
                _msgCsutomer.To.Add(recipient);
            }

            if (!string.IsNullOrEmpty(CC))
            {
                var arrCC = CC.Split(',').ToList();
                foreach (var cc in arrCC)
                {
                    _msgCsutomer.CC.Add(cc);
                }
            }
            _msgCsutomer.Subject = subject;
            _msgCsutomer.IsBodyHtml = true;
            _msgCsutomer.Body = emailBody;
            _SmtpClient.Host = Convert.ToString(Commonsetting.SMTPServer);
            _SmtpClient.Port = Convert.ToInt32(Commonsetting.Port);
            _SmtpClient.Credentials = new NetworkCredential(Commonsetting.Email, Commonsetting.Password);
            _SmtpClient.EnableSsl = Convert.ToBoolean(Commonsetting.IsSSL);
            _SmtpClient.Send(_msgCsutomer);
            isSend = true;
            return isSend;
        }
        #endregion
    }
}