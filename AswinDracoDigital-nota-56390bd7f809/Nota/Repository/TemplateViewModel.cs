﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nota.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace Nota.Repository
{
    public class TemplateViewModel
    {
        public int TemplateID { get; set; }

        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "RequiredField")]
        [StringLength(50, ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "MaxAllowedLength")]
        [Display(Name = "Template name")]
        public string TemplateName { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        public SelectList SelectTemplate { get; set; }
        public string TemplateContentForEmail { get; set; }
    }
}