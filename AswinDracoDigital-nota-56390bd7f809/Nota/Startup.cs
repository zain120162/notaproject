using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nota.Areas.Admin.Interface;
using Nota.Areas.User.Interface;
using Nota.Areas.User.Repository;
using Nota.Areas.Admin.Repository;
using Nota.Context;
using Nota.Interface;
using Nota.Repository;
using Microsoft.Extensions.Options;
using System.Net;
using Nota.Models;

namespace Nota
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddDbContext<ApplicationDbContext>(options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddRazorPages().AddRazorRuntimeCompilation();
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.Name = "Admin";
                options.IdleTimeout = TimeSpan.FromMinutes(15);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            services.AddSession(options =>
            {
                options.Cookie.Name = "User";
                options.IdleTimeout = TimeSpan.FromMinutes(15);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });
            // cookie settings
            services.ConfigureApplicationCookie(options =>
            {
                options.Events = new CookieAuthenticationEvents();
                options.SlidingExpiration = false;
            });

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IRoles, RoleRepository>();
            services.AddTransient<IUser, UserRepository>();
            services.AddTransient<IPublicUser, PublicUserRepository>();
            services.AddTransient<IUserResetPassword, UserResetPasswordRepository>();
            services.AddTransient<IPublicUser, PublicUserRepository>();
            services.AddTransient<IRolePrivileges, RolePrivilegesRepository>();
            services.AddTransient<IEmailSend, EmailSendRepository>();
            services.AddTransient<IUserPurchase, UserPurchaseRepository>();
            services.AddTransient<IForgotPassword, ForgotPasswordRepository>();
            services.AddTransient<ITemplate, TemplateRepository>();
            services.AddTransient<IUserCommonSetting, UserCommonSettingRepository>();
            services.AddTransient<ICommonSetting, CommonSettingRepository>();
            services.AddTransient<IUsers, UsersRepository>();
            services.AddTransient<IMembership, MembershipRepository>();
            services.AddTransient<IManageUser, ManageUserRepository>();
            services.AddTransient<IResetPassword, ResetPasswordRepository>();
            services.AddTransient<ICategories, CategoriesRepository>();
            services.AddTransient<IProduct, ProductRepository>();
            services.AddTransient<IUserProduct, UserProductRepository>();
            services.AddTransient<IEvents, EventsRepository>();
            services.AddTransient<IProductDetail, ProductDetailRepository>();
            services.AddTransient<IFaceBook, FaceBookRepository>();
            services.AddTransient<IChat, ChatRepository>();
            services.AddTransient<IEventParticipant, EventParticipantRepository>();
            services.AddTransient<IEventSubmission, EventSubmissionRepository>();
            services.AddTransient<IParameter, Nota.Areas.User.Repository.ParameterRepository>();
            services.AddTransient<IPaymentDetail, PaymentDetailRepository>();
            services.AddTransient<IMembership, MembershipRepository>();
            services.AddTransient<ICart, CartRepository>();
            services.AddTransient<IReview, ReviewRepository>();
            services.AddDistributedMemoryCache();
            services.AddSession();
            services.AddAuthentication();
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.AreaViewLocationFormats.Clear();
                options.AreaViewLocationFormats.Add("/Areas/{2}/Views/{1}/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Areas/{2}/Views/Shared/{0}.cshtml");
                options.AreaViewLocationFormats.Add("/Views/Shared/{0}.cshtml");
            });
            services.AddControllersWithViews();

            services.AddAuthentication().AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            });

            services.AddAuthentication().AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = Configuration["Authentication:Google:ClientId"];
                googleOptions.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseCookiePolicy();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAreaControllerRoute(
                  name: "Area" ,
                  areaName: "Admin",
                  pattern: "Admin/{controller=Login}/{action=Index}/{id?}");

                endpoints.MapAreaControllerRoute(
                 name: "Area",
                 areaName: "User",
                 pattern: "User/{controller=UserDashboard}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}