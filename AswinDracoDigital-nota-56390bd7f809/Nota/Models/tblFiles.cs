﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblFiles
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int FilesID { get; set; }

        [ForeignKey("tblProduct")]
        public int ProductID { get; set; }
    }
}
