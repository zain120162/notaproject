﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblEventParticipants
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int EventParticipantsID { get; set; }

        [ForeignKey("tblPublicUser")]
        public int PublicUserID { get; set; }

        [ForeignKey("tblEvents")]
        public int EventsID { get; set; }

        public DateTime DateApplied { get; set; }

        public bool Status { get; set; }
    }
}