﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblRolePrivileges : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int PrivilegeID { get; set; }

        [ForeignKey("tblRole")]
        public int? RoleID { get; set; }

        public int? MenuItemID { get; set; }
        public bool? View { get; set; }
        public bool? Add { get; set; }
        public bool? Edit { get; set; }
        public bool? Delete { get; set; }
        public bool? Detail { get; set; }
        public bool? IsAdminUser { get; set; }

        public virtual tblRole tblRole { get; set; }
    }
}
