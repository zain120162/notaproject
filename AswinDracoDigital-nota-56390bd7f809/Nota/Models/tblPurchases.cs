﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblPurchases
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int PurchaseID { get; set; }

        [ForeignKey("tblPublicUser")]
        public int PublicUserID { get; set; }

        [ForeignKey("tblProduct")]
        public int ProductID { get; set; }

        [ForeignKey("tblPaymentDetail")]
        public int PaymentDetailID { get; set; }

        public DateTime Date { get; set; }
    }
}