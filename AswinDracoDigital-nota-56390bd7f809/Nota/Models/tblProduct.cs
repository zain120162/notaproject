﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Models
{
    public class tblProduct : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ProductID { get; set; }

        [ForeignKey("tblCategories")]
        public int CategoriesID { get; set; }

        [ForeignKey("tblUser")]
        public int UserID { get; set; }

        [ForeignKey("tblPulicUser")]
        public int PublicUserID { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(250)]
        public string CoverImage { get; set; }

        [StringLength(250)]
        public string FileName { get; set; }

        public bool IsApproved { get; set; }

        public Int64 UploadType { get; set; }
            
        public int PriceType { get; set; }

        public decimal? DiscountPercentage { get; set; }

        public decimal Amount { get; set; }
    }
}
