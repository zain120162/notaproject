﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblPublicUser
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int PublicUserID { get; set; }

        [ForeignKey("tblMembership")]
        public int MembershipID { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        [StringLength(250)]
        public string Password { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(250)]
        public string ProfilePicture { get; set; }

        public DateTime DateJoined { get; set; }

        public bool Status { get; set; }

        public bool IsSocialLogin { get; set;}

        public bool IsEmailVerified { get; set; }
    }
}