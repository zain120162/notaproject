﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblMenuItem : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int MenuItemID { get; set; }

        [StringLength(50)]
        public string MenuItem { get; set; }

        [StringLength(50)]
        public string MenuItemController { get; set; }

        [StringLength(50)]
        public string MenuItemView { get; set; }

        public int? SortOrder { get; set; }
        public int? ParentID { get; set; }
    }
}