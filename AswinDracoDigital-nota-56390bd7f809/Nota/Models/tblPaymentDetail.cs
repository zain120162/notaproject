﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblPaymentDetail : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int PaymentDetailID { get; set; }

        [ForeignKey("tblPublicUser")]
        public int PublicUserID { get; set; }
        public string ReferenceNO { get; set; }

        public DateTime Date { get; set; }

        public Decimal Amount { get; set; }

        public Int16 PaymentStatus { get; set; } 
    }
}