﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models 
{
    public class tblMembership : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int MembershipID { get; set; }

        [ForeignKey("tblPaymentDetail")]
        public int PaymentDetailID { get; set; }

        public Int16 MembershipName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public int DownloadAllowed { get; set; }

        public int UploadAllowed { get; set; }

        public bool ParticipantJoin { get; set; }

        public bool ParticipantCreate { get; set; }


        public int PublicUserID { get; set; }
    }
}