﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Models
{
    public class tblParameter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ParameterID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool isActive { get; set; }
    }
}
