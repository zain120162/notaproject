﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Models
{
    public class tblEventSubmission
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int EventsubID { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string Path { get; set; }
        public bool isActive { get; set; }
        public int EventID { get; set; }
        public int UserID { get; set; }
    }
}
