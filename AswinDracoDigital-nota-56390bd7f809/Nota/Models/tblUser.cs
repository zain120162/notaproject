﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblUser  
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UserID { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(250)]
        public string Email { get; set; }

        [StringLength(250)]
        public string Password { get; set; }

        public bool IsActive { get; set; }
        public bool IsAdminUser { get; set; }

        [ForeignKey("tblRole")]
        public Nullable<int> RoleID { get; set; }

        public virtual tblRole tblRole { get; set; }
    }
}