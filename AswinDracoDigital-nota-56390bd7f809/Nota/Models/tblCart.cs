﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Models
{
    public class tblCart
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CartID { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public decimal Amount { get; set; }

        public int Qty { get; set; }

        public int PublicUserID { get; set; }
    }
}
