﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Models
{
    public class tblReview
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ReviewID { get; set; }

        public string Review { get; set; }

        public DateTime ReviewDate { get; set; }

        public int PublicUserID { get; set; }

        public int ReviewStar { get; set; }

        public int ProductID { get; set; }
    }
}
