﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblEvents : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int EventsID { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(70)]
        public string Brief { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        [ForeignKey("tblEventParticipants")]
        public int WinnerID { get; set; }

        public DateTime EndDate { get; set; }


        public int MaxParticipants { get; set; }

        public int RegParticipants { get; set; }
        public DateTime PostedDate { get; set; }
    }
}