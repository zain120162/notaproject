﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblCategories : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int CategoriesID{ get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(250)]
        public string Thumbnail { get; set; }

    }
}
