﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblTemplate : BaseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TemplateID { get; set; }

        public string TemplateName { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}