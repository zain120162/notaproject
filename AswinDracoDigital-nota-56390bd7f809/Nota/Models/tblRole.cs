﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nota.Models
{
    public class tblRole
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int RoleID { get; set; }

        [StringLength(50)]
        public string Role { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool? IsActive { get; set; }
        public bool IsAdminRole { get; set; }
    }
}
