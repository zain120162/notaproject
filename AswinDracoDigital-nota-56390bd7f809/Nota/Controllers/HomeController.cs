﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nota.Common;
using Nota.Interface;
using Nota.Models;
using Nota.ViewModel;

namespace Nota.Controllers
{
    public class HomeController : Controller
    {
        #region Member Declaration        
        private readonly IPublicUser _user;
        private readonly IProductDetail _productDetail;
        private readonly ILogger<HomeController> _logger;
        #endregion

        #region Constractor      
        public HomeController(ILogger<HomeController> logger, IPublicUser user,IProductDetail productDetail)
        {
            _productDetail = productDetail;
            _user = user;
            _logger = logger;
        }
        #endregion

        [HttpGet]
        public IActionResult Index()
        {
            //ProductImageViewModel productImageViewModel = new ProductImageViewModel();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult EmailVerified()
        {
            return RedirectToAction("Index", "Home", new { Msg = "EmailVerified" });
        }

        public IActionResult Verified(string EmailID)
        {
            var emailID = GlobalCode.UrlDecrypt(EmailID);
            _user.VerifiedUser(emailID);
            return View();        
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //#region Bind Music Notes
        //private ProductViewModel BindMusicNote(ProductViewModel productViewModel)
        //{
        //    productViewModel.MusicList = new SelectList(_productDetail.GetMusicList());
        //    productViewModel.NotesLIst = new SelectList(_productDetail.GetNoteList());
        //    return productViewModel;
        //}
        //#endregion

        #region Ajax Binding
        public ActionResult<IList<ProductViewModel>> _GetMusic()
        {
            var productList = _productDetail.GetMusicList().ToList();
            return Json(productList);
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<ProductViewModel>> _GetNotes()
        {
            var noteList = _productDetail.GetNotesList().ToList();
            return Json(noteList);
        }
        #endregion
    }
}