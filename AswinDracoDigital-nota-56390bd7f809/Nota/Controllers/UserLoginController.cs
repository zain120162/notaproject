﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nota.Common;
using Nota.Areas.User.UserHelpers;
using Nota.Interface;
using Nota.Models;
using System.Threading.Tasks;
using static Nota.ViewModel.FaceBookViewModel;
using static Nota.ViewModel.GoogleViewModel;
using Nota.Context;

namespace Nota.Controllers
{
    public class UserLoginController : Controller
    {
        #region Member Declaration        
        private readonly IFaceBook _faceBook;
        private readonly IPublicUser _user;
        #endregion

        #region Constractor      
        public UserLoginController(IPublicUser user,IFaceBook faceBook)
        {
            _faceBook = faceBook;
            _user = user;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            if (CurrentPublicSession.PublicUser != null && CurrentPublicSession.PublicUserID> 0)
            {
                return RedirectToAction("Index", "Dashboard",new { Msg = "AlreadyLoggedIn" });
            }
            return View();
        }

        [HttpPost]
        public IActionResult Index(int? id)
        {
            if (CurrentPublicSession.PublicUser != null && CurrentPublicSession.PublicUserID > 0)
            {
                return RedirectToAction("Index", "Home");
            }
            if (!string.IsNullOrEmpty(Request.Form["Email"]) && !string.IsNullOrEmpty(Request.Form["Password"]))
            {
                string strUserName = Convert.ToString(Request.Form["Email"]);
                string strUserPassword = Convert.ToString(Request.Form["Password"]);
                var userDetail = _user.GetActiveUserByEmail(strUserName);
                if (userDetail != null)
                {
                    if (userDetail.IsSocialLogin == true)
                    {                        
                        CurrentPublicUser User = new CurrentPublicUser();
                        User.PublicUserID = userDetail.PublicUserID;
                        User.FirstName = userDetail.FirstName;
                        User.LastName = userDetail.LastName;
                        User.Email = userDetail.Email;
                        CurrentPublicSession.PublicUser = User;
                        TempData["msg"] = "welcome";
                        return RedirectToAction("Index", "UserDashboard", new { area = "User" });
                    }
                    else
                    {
                        if (userDetail.IsEmailVerified == true)
                        {
                            if (strUserPassword == GlobalCode.Encryption.Decrypt(userDetail.Password))
                            {
                                CurrentPublicUser User = new CurrentPublicUser();
                                User.PublicUserID = userDetail.PublicUserID;
                                User.FirstName = userDetail.FirstName;
                                User.LastName = userDetail.LastName;
                                User.Email = userDetail.Email;
                                CurrentPublicSession.PublicUser = User;


                                if (TempData["ReturnPath"] != null)
                                {
                                    return Redirect(Url.Content(TempData["ReturnPath"].ToString()));
                                }
                                TempData["msg"] = "welcome";
                                return RedirectToAction("Index", "UserDashboard", new { area = "User" });
                            }
                            else
                            {
                                TempData["Msg"] = "wrongemailpassword";
                                ModelState.AddModelError(string.Empty, Messages.WrongEmailPassword);
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            TempData["Msg"] = "unverified";
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
                else
                {
                    TempData["Msg"] = "usernotexists";
                    ModelState.AddModelError(string.Empty, Messages.NotExistsUser);
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, Messages.RequiredField);
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion

        #region Logout
        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            HttpContext.Session.Remove("CurrentPublicUser");
            return RedirectToAction("Index", "Home", new { Msg = "Logout" });
        }
        #endregion

        [HttpGet]
        public async Task<IActionResult> CallBack(string code, string state)
        {
            FacebookUser facebook_user = await this._faceBook.GetFacebookUser(code);
            if (_user.IsPublicUserExists(facebook_user.name) == false)
            {
                _faceBook.AddUser(facebook_user);
            }
            var userDetail = _user.GetPublicUserDetailByName(facebook_user.name);
            CurrentPublicUser User = new CurrentPublicUser();
            User.PublicUserID = userDetail.PublicUserID;
            User.FirstName = userDetail.FirstName;
            CurrentPublicSession.PublicUser = User;

            TempData["msg"] = "welcome";
            return RedirectToAction("Index", "UserDashboard", new { area = "User" });
        } 

        public IActionResult AddUser(FacebookUser facebookUser)
        {
            return View();
        }

        public IActionResult Privacy()
        {
            // Create a random state
            string state = Guid.NewGuid().ToString();
            // Create the url
            string url = "https://www.facebook.com/dialog/oauth?client_id=" + "228721164991378" + "&state=" +
            state + "&response_type=code&redirect_uri=" + "https://localhost:44355" + "/home/callback";
            // Redirect the user
            return Redirect(url);
        }

        public IActionResult Google()
        {
            //     var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + googleplus_redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;

            //https://accounts.google.com/o/oauth2/auth/oauthchooseaccount?response_type=code&client_id=505669636385-0ad3rmofgmhielkbknqim5k1s8d29a4d.apps.googleusercontent.com&redirect_uri=https%3A%2F%2Fauth.web.dev%2F__%2Fauth%2Fhandler&state=AMbdmDmKMJkyVEdZq33YB-UFktf-6xWTOC6gOqEJsx_kovXdqZztvjUf54WlJYb9HrspLze5PFyOYCQeiIUQkQwCdXlQ9GVl9GlKme-XRGj9lLXK72cQNtf56A6-S1zKvlrAgXDfvAQNweJLwm7PY_U29RViM3eYd0NNJYfof66RHsBtq9bMChzcThpP0-PibTkxPNqojHM7E0DuVfahyZxgyK2Z-1yTieKrIrqwOr8jgOqEX8W2NQci-H_tSha0AvXme47MvWCNTMqcHTTQTxsbmOoCmq3X2mStblTEaeF4XebnXcxE&scope=openid%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email%20profile&context_uri=https%3A%2F%2Fweb.dev&flowName=GeneralOAuthFlow
            // Create a random state
            string state = Guid.NewGuid().ToString();
            string url = "https://accounts.google.com/o/oauth2/v2/auth?" +
            "scope=openid email profile&" +
                //"include_granted_scopes=true&" +
                "response_type=code&" +
                "state=" + state + "&" +
                "redirect_uri=https://localhost:44322" + "/UserLogin/callbackg&" +
                "client_id=505669636385-0ad3rmofgmhielkbknqim5k1s8d29a4d.apps.googleusercontent.com";
            return Redirect(url);
        }

        public IActionResult FaceBook() 
        {
            string state = Guid.NewGuid().ToString();
            var url = "https://www.facebook.com/v8.0/dialog/oauth?" +
                "response_type=code&" +
                "display=popup&" +
                "state=" + state + "&" +
                "redirect_uri=https://localhost:44322" + "/UserLogin/callback&" +
                "client_id=1605867736260802";
            //var facebookurl = "https://www.facebook.com/v8.0/dialog/oauth?&client_id=1605867736260802&display=popup&domain=https://localhost:44322/UserLogin/callback&e2e=%7B%7D&redirect_uri=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df3f109e2989fe0c%26domain%3Dlocalhost%26origin%3Dhttps%253A%252F%252Flocalhost%253A44322%252Ff1d1ca646c0d6c8%26relation%3Dopener.parent%26frame%3Df285d937f7b8e8&scope=public_profile%2Cemail&sdk=joey&url=https://localhost:44322/UserLogin/callback&version=v8.0";
            return Redirect(url);
        }

        [HttpGet]
        public async Task<IActionResult> CallBackg(string code, string state)
        {
            FacebookUser facebook = await this._faceBook.GetGoogleUser(code);
            if (_user.IsPublicUserEmailExists(facebook.email) == false)
            {
                _faceBook.AddUser(facebook);
            }
            var userDetail = _user.GetActiveUserByEmail(facebook.email);
            CurrentPublicUser User = new CurrentPublicUser();
            User.PublicUserID = userDetail.PublicUserID;
            User.FirstName = userDetail.FirstName;
            User.LastName = userDetail.LastName;
            User.Email = userDetail.Email;
            CurrentPublicSession.PublicUser = User;

            TempData["msg"] = "welcome";
            return RedirectToAction("Index", "UserDashboard", new { area = "User" });
        }

        #region Error
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
    }
}