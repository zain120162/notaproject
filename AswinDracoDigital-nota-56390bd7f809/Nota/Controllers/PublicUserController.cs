﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nota.Common;
using Nota.Interface;
using Nota.ViewModel;

namespace Nota.Controllers
{
    public class PublicUserController : Controller
    {
        #region Member Declaration
        private readonly IPublicUser _user;
        private readonly IEmailSend _emailsend;
        #endregion

        #region Constractor
        public PublicUserController(IPublicUser user, IEmailSend emailSend)
        {
            _user = user;
            _emailsend = emailSend;
        }
        #endregion

        #region Index
        public IActionResult Index()
        {
            var viewModel = new PublicUserViewModel
            {
                ModuleName = new CommonMessagesViewModel { ModuleName = "User" }
            };
            return View(viewModel);
        }
        #endregion

        #region Create
        [HttpGet]
        public IActionResult Create()
        {
            PublicUserViewModel objUserModel = new PublicUserViewModel();
            objUserModel.IsActive = true;
            return View(objUserModel);
        }

        [HttpPost]
        public IActionResult Create(PublicUserViewModel objUserModel)
        {
            //Validate User exists or not
            if (_user.IsUserExists(objUserModel.PublicUserID, objUserModel.Email.Trim()))
            {
                TempData["Msg"] = "alreadyexists";
                //ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Email"));
                return RedirectToAction("Index", "Home");
            }
            else
            {
                _user.AddUser(objUserModel);
                var templates = _user.GetEmailTemplateContent("Account Verification");
                string emailBody = templates.Body;
                emailBody = PopulateEmailBody(objUserModel, emailBody);
                _emailsend.SendModuleEmail(objUserModel.Email, templates.Subject, emailBody);
                TempData["Msg"] = "added";
                return RedirectToAction("Index", "Home");                
            }
        }
        #endregion

        #region Email body Html In Forgot Password        
        private string PopulateEmailBody(PublicUserViewModel result, string emailBody)
        {
            var siteUrl = _user.GetCommonSetting().SiteURL;
            emailBody = emailBody.Replace("#FirstName#", Convert.ToString(result.FirstName));
            emailBody = emailBody.Replace("#LastName#", Convert.ToString(result.LastName));
            emailBody = emailBody.Replace("#click here#", "<a href=" + string.Format(siteUrl) + "Home/Verified?EmailID=" + GlobalCode.UrlEncrypt(result.Email) + ">click here</a>");

            return emailBody;
        }
        #endregion

        #region Edit
        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    var userID = int.Parse(id);
                    PublicUserViewModel objUserModel = _user.GetUserDetailByUserID(userID);
                    if (objUserModel == null)
                    {
                        TempData["Msg"] = "drop";
                        return RedirectToAction("Index", "User");
                    }
                    //if (objUserViewModel.Email == GlobalCode.AdminEmailAddress)
                    //{
                    //    return RedirectToAction("Index", "User", new { Msg = "notauthorized" });
                    //}
                    objUserModel.Password = objUserModel.Password;
                    return View(objUserModel);
                }
            }
            catch
            {
                TempData["Msg"] = "error";
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public IActionResult Edit(PublicUserViewModel objUserModel, string userId)
        {
            var userDetail = _user.GetUserDetailByUserID(objUserModel.PublicUserID);
            if (userDetail == null)
            {
                TempData["Msg"] = "drop";
                return RedirectToAction("Index", "Home");
            }
            if (userDetail.Email == GlobalCode.AdminEmailAddress)
            {
                TempData["Msg"] = "notauthorized";
                return RedirectToAction("Index", "Home");
            }

            bool emailDuplicateStatus = _user.IsUserExistsByEmail("Email", objUserModel.Email.Trim(), objUserModel.PublicUserID);
            if (emailDuplicateStatus)
            {
                ModelState.AddModelError(string.Empty, string.Format(Messages.AlreadyExists, "Email "));
                return View(objUserModel);
            }
            _user.EditUser(objUserModel);
            TempData["Msg"] = "updated";
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Detail
        [HttpGet]
        public IActionResult Detail(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    var userID = int.Parse(id);
                    PublicUserViewModel objUserModel = _user.GetUserDetailByUserID(userID);
                    if (objUserModel == null)
                    {
                        TempData["Msg"] = "drop";
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View(objUserModel);
                    }
                }
            }
            catch
            {
                TempData["Msg"] = "error";
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion

        #region Ajax Binding
        public ActionResult<IList<PublicUserViewModel>> _AjaxBinding()
        {
            var userList = _user.BindUserDetail().ToList();
            return Json(userList);
        }
        #endregion

        #region Delete
        [HttpPost]
        public IActionResult Delete(string[] userId)
        {
            try
            {
                if (userId.Length > 0)
                {
                    _user.DeleteUser(userId);
                    TempData["Msg"] = "deleted";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Msg"] = "noselect";
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception _exception)
            {
                if (_exception.InnerException != null && (_exception.InnerException.Message.Contains(GlobalCode.foreignKeyReference) || ((_exception.InnerException).InnerException).Message.Contains(GlobalCode.foreignKeyReference)))
                {
                    TempData["Msg"] = "inuse";
                    return RedirectToAction("Index", "Home");
                }
                TempData["Msg"] = "error";
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
    }
}