﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Nota.Helpers;
using Nota.Interface;
using Nota.Models;
using Nota.Repository;

namespace Nota.Controllers
{
    public class BaseController : Controller
    {
        protected IConfiguration Config { get; }
        private readonly ICacheManager _cacheManager = null;
        
        public BaseController()
        {
            _cacheManager = new MemoryCacheManager();
        }
        public BaseController(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controllerName = ((ControllerActionDescriptor)filterContext.ActionDescriptor).ControllerName;

            ValidateLogin(filterContext);
            ModulePrivileges(controllerName);
            base.OnActionExecuting(filterContext);
        }

        private void ValidateLogin(ActionExecutingContext filterContext)
        {
            string controllerName = ((ControllerActionDescriptor)filterContext.ActionDescriptor).ControllerName;
            string methodName = ((ControllerActionDescriptor)filterContext.ActionDescriptor).ActionName;

            if (CurrentAdminSession.User == null)
            {
                if (!(string.Compare(controllerName.ToLower(), "home", true) == 0 && string.Compare(methodName.ToLower(), "index", true) == 0))
                {
                    filterContext.HttpContext.Session.Clear();
                    TempData["Msg"] = "unauthorize";
                    filterContext.Result = new RedirectResult("~/Home/Index");
                    return;
                }
            }           
        }

        private void ModulePrivileges(string controller)
        {
            try
            {
                vwRolePrivileges rolePrivileges = new vwRolePrivileges();
                var privileges = (HttpContext.Session.GetObject<IList<vwRolePrivileges>>("MenuItems"));
                bool isAdminUser = CurrentAdminSession.IsAdminRole;                
                rolePrivileges = privileges.Where(p => (isAdminUser ? p.IsAdminUser == isAdminUser : p.RoleID == CurrentAdminSession.RoleID) && p.MenuItemController == controller && p.IsActive == 1).FirstOrDefault();

                if (rolePrivileges != null)
                {
                    CurrentAdminPermission Permission = new CurrentAdminPermission();
                    Permission.HasViewPermission = rolePrivileges.View.Value;
                    Permission.HasAddPermission = rolePrivileges.Add.Value;
                    Permission.HasEditPermission = rolePrivileges.Edit.Value;
                    Permission.HasDeletePermission = rolePrivileges.Delete.Value;
                    Permission.HasDetailPermission = rolePrivileges.Detail.Value;
                    CurrentAdminSession.Permission = Permission;
                }
                else
                {
                    CurrentAdminPermission Permission = new CurrentAdminPermission();
                    Permission.HasViewPermission = false;
                    Permission.HasAddPermission = false;
                    Permission.HasEditPermission = false;
                    Permission.HasDeletePermission = false;
                    Permission.HasDetailPermission = false;
                    CurrentAdminSession.Permission = Permission;
                }
            }
            catch
            {
            }
        }
    }
}