﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.ViewModel
{
    public class ParameterViewModel
    {
        public int ParameterID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool isActive { get; set; }
    }
}
