﻿using Nota.Areas.Admin.Models;
using Nota.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.ViewModel
{
    public class ProductImageViewModel
    {
        public int ProductImageID { get; set; }

        public int ProductID { get; set; }

        public string ImageName { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public ProductModel ProductModel { get; set; }

        public List<ProductModel> productlist { get; set; }

        public CommonMessagesModel ModuleName { get; set; }

        public PublicUserViewModel PublicUserViewModel { get; set; }
    }
}