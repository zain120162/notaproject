﻿namespace Nota.ViewModel
{
    public class GoogleViewModel
    {
        public class GoogleAuthorization
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string refresh_token { get; set; }
            public string scope { get; set; }
        } // End of the class
        public class GoogleUser
        {
            #region Veriable
            public string id { get; set; }
            public string name { get; set; }
            public string given_name { get; set; }
            public string email { get; set; }
            public string password { get; set; }
            public string picture { get; set; }
            #endregion
            #region Constructor
            public GoogleUser()
            {
                this.id = null;
                this.name = null;
                this.given_name = null;
                this.email = null;
                this.password = null;
                this.picture = null;
            }
            #endregion
        }
    }
}