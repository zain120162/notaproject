﻿using System;

namespace Nota.ViewModel
{
    public class FaceBookViewModel
    {
        public class FacebookAuthorization
        {
            #region Variables
            public string access_token { get; set; }
            public string token_type { get; set; }
            #endregion
            #region Constructors
            public FacebookAuthorization()
            {
                // Set values for instance variables
                this.access_token = null;
                this.token_type = null;
            } // End of the constructor
            #endregion
        }

        public class FacebookErrorRoot
        {
            #region Variables
            public FacebookError error { get; set; }
            #endregion
            #region Constructors
            public FacebookErrorRoot()
            {
                // Set values for instance variables
                this.error = null;
            } // End of the constructor
            #endregion
        } 
       
        public class FacebookError
        {
            #region Variables
            public string message { get; set; }
            public string type { get; set; }
            public Int32? code { get; set; }
            public Int32? error_subcode { get; set; }
            public string fbtrace_id { get; set; }
            #endregion
            #region Constructors
            public FacebookError()
            {
                this.message = null;
                this.type = null;
                this.code = null;
                this.error_subcode = null;
                this.fbtrace_id = null;
            } // End of the constructor
            #endregion
        } 
        
        public class FacebookUser
        {
            #region Variables
            public string id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string given_name { get; set; }
            public string Password { get; set; }
            #endregion
            #region Constructors
            public FacebookUser()
            {
                // Set values for instance variables
                this.id = null;
                this.name = null;
                this.email = null;
                this.given_name = null;
                this.Password = null;
            } // End of the constructor
            #endregion
        } 
    }
}