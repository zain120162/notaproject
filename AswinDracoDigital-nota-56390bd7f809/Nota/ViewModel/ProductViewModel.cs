﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.ViewModel
{
    public class ProductViewModel
    {
        public int ProductID { get; set; }

        public int CategoriesID { get; set; }
        public SelectList MusicList { get; set; }
        public SelectList NotesLIst { get; set; }
        public int UserID { get; set; }

        public int PublicUserID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string FileName { get; set; }

        public string CoverImage { get; set; }

        public bool IsApproved { get; set; }

        public int UploadType { get; set; }

        public int PriceType { get; set; }

        public decimal? DiscountPercentage { get; set; }

        public decimal Amount { get; set; }
    }
}
