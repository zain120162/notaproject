/**
 * custom.js - fileuploader
 * Copyright (c) 2020 Innostudio.de
 * Website: https://innostudio.de/fileuploader/
 * Version: 2.2 (22 Jul 2020)
 * License: https://innostudio.de/fileuploader/documentation/#license
 */
$(document).ready(function() {
	
	// enable fileuploader plugin
	$('input[name="Attachments"]').fileuploader({
		limit: 20,
		maxSize: 50,
	
		addMore: true
	});
	
});