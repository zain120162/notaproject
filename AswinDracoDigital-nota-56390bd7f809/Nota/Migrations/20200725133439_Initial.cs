﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nota.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblCommonSetting",
                columns: table => new
                {
                    SettingID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    SMTPServer = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    Password = table.Column<string>(maxLength: 20, nullable: true),
                    Port = table.Column<int>(nullable: false),
                    SiteURL = table.Column<string>(maxLength: 100, nullable: true),
                    IsSSL = table.Column<byte>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCommonSetting", x => x.SettingID);
                });

            migrationBuilder.CreateTable(
                name: "tblMenuItem",
                columns: table => new
                {
                    MenuItemID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    MenuItem = table.Column<string>(maxLength: 50, nullable: true),
                    MenuItemController = table.Column<string>(maxLength: 50, nullable: true),
                    MenuItemView = table.Column<string>(maxLength: 50, nullable: true),
                    SortOrder = table.Column<int>(nullable: true),
                    ParentID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblMenuItem", x => x.MenuItemID);
                });

            migrationBuilder.CreateTable(
                name: "tblRole",
                columns: table => new
                {
                    RoleID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Role = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: true),
                    IsAdminRole = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblRole", x => x.RoleID);
                });

            migrationBuilder.CreateTable(
                name: "tblTemplate",
                columns: table => new
                {
                    TemplateID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    TemplateName = table.Column<string>(nullable: true),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblTemplate", x => x.TemplateID);
                });

            migrationBuilder.CreateTable(
                name: "vwRolePrivileges",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    MenuItemID = table.Column<int>(nullable: false),
                    MenuItem = table.Column<string>(nullable: true),
                    MenuItemController = table.Column<string>(nullable: true),
                    MenuItemView = table.Column<string>(nullable: true),
                    View = table.Column<bool>(nullable: true),
                    Add = table.Column<bool>(nullable: true),
                    Edit = table.Column<bool>(nullable: true),
                    Delete = table.Column<bool>(nullable: true),
                    Detail = table.Column<bool>(nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    ParentID = table.Column<int>(nullable: true),
                    SortOrder = table.Column<int>(nullable: true),
                    IsActive = table.Column<int>(nullable: true),
                    RoleID = table.Column<int>(nullable: true),
                    IsAdminUser = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vwRolePrivileges", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "tblRolePrivileges",
                columns: table => new
                {
                    PrivilegeID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleID = table.Column<int>(nullable: true),
                    MenuItemID = table.Column<int>(nullable: true),
                    View = table.Column<bool>(nullable: true),
                    Add = table.Column<bool>(nullable: true),
                    Edit = table.Column<bool>(nullable: true),
                    Delete = table.Column<bool>(nullable: true),
                    Detail = table.Column<bool>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsAdminUser = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblRolePrivileges", x => x.PrivilegeID);
                    table.ForeignKey(
                        name: "FK_tblRolePrivileges_tblRole_RoleID",
                        column: x => x.RoleID,
                        principalTable: "tblRole",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tblUser",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 50, nullable: true),
                    LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Phone = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(maxLength: 250, nullable: true),
                    Password = table.Column<string>(maxLength: 250, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsAdminUser = table.Column<bool>(nullable: false),
                    RoleID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblUser", x => x.UserID);
                    table.ForeignKey(
                        name: "FK_tblUser_tblRole_RoleID",
                        column: x => x.RoleID,
                        principalTable: "tblRole",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tblRolePrivileges_RoleID",
                table: "tblRolePrivileges",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_tblUser_RoleID",
                table: "tblUser",
                column: "RoleID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblCommonSetting");

            migrationBuilder.DropTable(
                name: "tblMenuItem");

            migrationBuilder.DropTable(
                name: "tblRolePrivileges");

            migrationBuilder.DropTable(
                name: "tblTemplate");

            migrationBuilder.DropTable(
                name: "tblUser");

            migrationBuilder.DropTable(
                name: "vwRolePrivileges");

            migrationBuilder.DropTable(
                name: "tblRole");
        }
    }
}
