﻿using Microsoft.Extensions.Configuration;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Nota.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base()
        {
        }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
                var connectionString = configuration.GetConnectionString("DefaultConnection");
                optionsBuilder.UseMySql(connectionString);
            }
        }
        public DbSet<Nota.Models.tblUser> tblUser { get; set; }
        public DbSet<Nota.Models.tblRole> tblRole { get; set; }
        public DbSet<Nota.Models.tblRolePrivileges> tblRolePrivileges { get; set; }
        public DbSet<Nota.Models.tblTemplate> tblTemplate { get; set; }
        public DbSet<Nota.Models.tblMenuItem> tblMenuItem { get; set; }
        public DbSet<Nota.Models.tblProductImage> tblProductImage { get; set; }
        public DbSet<Nota.Models.tblCategories> tblCategories { get; set; }
        public DbSet<Nota.Models.tblProduct> tblProducts { get; set; }
        public DbSet<Nota.Models.tblPurchases> tblPurchases { get; set; }
        public DbSet<Nota.Models.tblPublicUser> tblPublicUser { get; set; }
        public DbSet<Nota.Models.tblEvents> tblEvents { get; set; }
        public DbSet<Nota.Models.tblEventParticipants> tblEventParticipants { get; set;} 
        public DbSet<Nota.Models.tblPaymentDetail> tblPaymentDetail { get; set; }
        public DbSet<Nota.Models.tblMembership> tblMembership { get; set; }
        public DbSet<Nota.Models.tblCommonSetting> tblCommonSetting { get; set; }
        public DbSet<Nota.Models.vwRolePrivileges> vwRolePrivileges { get; set; }
        public DbSet<Nota.Models.tblProductFile> tblProductFile { get; set; }
        public DbSet<Nota.Models.tblChat> tblChat { get; set; }
        public DbSet<Nota.Models.tblEventSubmission> tblEventSubmission { get; set; }
        public DbSet<Nota.Models.tblParameter> tblParameter { get; set; }
        public DbSet<Nota.Models.tblCart> tblCart { get; set; }
        public DbSet<Nota.Models.tblReview> tblReview { get; set; }
    }
}