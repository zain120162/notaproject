﻿using Nota.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nota.Interface
{
    public interface IParamter
    {
        ParameterViewModel Get(string paramame);
    }
}
