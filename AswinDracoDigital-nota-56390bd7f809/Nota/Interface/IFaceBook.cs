﻿using System.Threading.Tasks;
using static Nota.ViewModel.FaceBookViewModel;

namespace Nota.Interface
{
    public interface IFaceBook
    {
         Task<FacebookUser> GetFacebookUser(string code);
         Task<FacebookUser> GetGoogleUser(string code);
         void AddUser(FacebookUser facebookUser);
    }
}