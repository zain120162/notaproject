﻿using Nota.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Interface
{
    public interface IProductDetail
    {
         IQueryable<ProductViewModel> GetMusicList();
         IQueryable<ProductViewModel> GetNotesList();
    }
}