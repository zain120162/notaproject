﻿using Nota.Models;

namespace Nota.Interface
{
    public interface IUserCommonSetting
    {
        tblCommonSetting GetCommonSetting();
    }
}