﻿namespace Nota.Interface
{
    public interface IEmailSend
    {
        bool SendModuleEmail(string recipients, string subject, string emailBody, string CC = "");
    }
}