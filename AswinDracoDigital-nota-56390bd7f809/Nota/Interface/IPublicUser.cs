﻿using Nota.Models;
using Nota.Repository;
using Nota.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Nota.Interface
{
    public interface IPublicUser
    {
        IQueryable<PublicUserViewModel> BindUserDetail();
        List<PublicUserViewModel> GetRoleList(int roleID, string roleName);
        bool IsUserExists(int publicUserID, string emailID);
         bool IsUserEmailExists(int UserID, string Email);
        void AddUser(PublicUserViewModel publicUserViewModel);
        PublicUserViewModel GetUserDetailByUserID(int PublicUserID);
        bool IsUserExistsByEmail(string fieldList, string valueList, int strAddOrEditID);
        void EditUser(PublicUserViewModel publicUserViewModel);
        void DeleteUser(string[] ids);
        PublicUserViewModel GetActiveUserByEmail(string email);
        PublicUserViewModel GetUserByID(int userID);
         tblCommonSetting GetCommonSetting();
         TemplateViewModel GetEmailTemplateContent(string templateName);
         void VerifiedUser(string EmailID);
         bool IsPublicUserEmailExists(string Email);
         bool IsPublicUserExists(string name);
         PublicUserViewModel GetPublicUserDetailByName(string Name);
    }
}